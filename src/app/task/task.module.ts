// ********************* Core Modules *********************//

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { Router, RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { MaterialModule, MatNativeDateModule, MatDatepickerModule } from '@angular/material';
import { MaterialModule } from '../material/material.module';
import { CdkTableModule } from '@angular/cdk/table';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DragulaModule } from 'ng2-dragula';
import { DndModule } from 'ng2-dnd';
import { MomentModule } from 'angular2-moment';
import { FileUploadModule } from 'ng2-file-upload';
import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { DragScrollModule } from 'angular2-drag-scroll';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ColorPickerModule } from 'ngx-color-picker';
import { PopoverModule } from 'ngx-popover';
import 'hammerjs';

// ********************* Core Modules *********************//


// ********************* Components *********************//

import { TaskManagerComponent } from './task-manager/task-manager.component';
import { AppHeaderComponent } from './app-header/app-header.component';
import { environment } from '../../environments/environment';
import { PhaseComponent } from './phase/phase.component';
import { TasksComponent } from './tasks/tasks.component';
import { AlertBoxComponent } from './alert-box/alert-box.component';
import { EditTaskComponent } from './edit-task/edit-task.component';
import { InviteMembersComponent } from './invite-members/invite-members.component';
import { AddNewTaskComponent } from './add-new-task/add-new-task.component';
import { EditTaskTabsComponent } from './edit-task-tabs/edit-task-tabs.component';
import { EditTabComponent, PriorityPipe } from './edit-tab/edit-tab.component';
import { CommentsTabComponent } from './comments-tab/comments-tab.component';
import { AttachmentsTabComponent } from './attachments-tab/attachments-tab.component';
import { ActivityTabComponent } from './activity-tab/activity-tab.component';
import { InlineTextComponent } from './inline-text/inline-text.component';
import { NotificationComponent } from './notification/notification.component';
import { TaskFilterComponent } from './task-filter/task-filter.component';
import { InlineTextareaComponent } from './inline-textarea/inline-textarea.component';
import { TagsComponent } from './tags/tags.component';

// ********************* Components *********************//


// ********************* <Service> *********************//

import { TaskerService } from './task-manager/tasker.service';
import { PhaseService } from './phase/phase.service';
import { MemberService } from './invite-members/member.service';
import { TaskService } from './tasks/task.service';
import { TimerService } from './edit-task/timer.service';
import { TabService } from './shared/tabs.service';
import { ToastrService } from './shared/toaster.service';
import { AttachmentsService } from './attachments-tab/attachments.service';
import { SocketService } from './shared/socket.service'
import { TagsService } from './tags/tags.service';
import { CommentService } from './comments-tab/comment.service';
import { LoginHistoryService } from '../authentication/login-history/login-history.service';
import { SpeechRecognitionService } from './speech-recognizer/speech-recognition.service';
import { TaskFilterService } from './task-filter/task-filter.service';

// ********************* </Service> *********************//

// ********************* Pipe *********************//
import { LimitPipe } from './shared/taskLimitPipe.pipe';
// ********************* Pipe *********************//


// ********************* Module *********************//
import { GoogleDrivePickerDirective } from './shared/google-drive-picker.directive';
import { SharedModule } from '../shared/shared.module';
import { NewLabelComponent } from './new-label/new-label.component';
import { SpeechRecognizerComponent } from './speech-recognizer/speech-recognizer.component'
import { DateAdapter, MAT_DATE_FORMATS, NativeDateAdapter } from '@angular/material/typings/core';
import { CummulativeChartComponent } from './cummulative-chart/cummulative-chart.component';
import { CummulativeChartService } from './cummulative-chart/cummulative-chart.service';
import { InlineDropdownComponent } from './inline-dropdown/inline-dropdown.component';
import { DropdownvaluePipe } from './shared/dropdownvalue.pipe';
import { DropdownfilterComponent } from './dropdownfilter/dropdownfilter.component';
import { TextfilterComponent } from './textfilter/textfilter.component';

// ********************* Module *********************//

// ********************** Ng2 Nvd3 *****************//
import { NvD3Module } from 'ng2-nvd3';
// d3 and nvd3 should be included somewhere
import 'd3';
import 'nvd3';
import { ChecklistComponent } from './checklist/checklist.component';
import { SelecteddropdownPipe } from './shared/selecteddropdown.pipe';
import { ReportComponent } from './report/report.component';
import { ReportService } from './report/report.service';

@NgModule({
  declarations: [
    AppHeaderComponent,
    TaskManagerComponent,
    AlertBoxComponent,
    EditTaskComponent,
    PhaseComponent,
    TasksComponent,
    AddNewTaskComponent,
    EditTaskTabsComponent,
    EditTabComponent,
    CommentsTabComponent,
    AttachmentsTabComponent,
    ActivityTabComponent,
    PriorityPipe,
    InlineTextComponent,
    TaskFilterComponent,
    InlineTextareaComponent,
    TagsComponent,
    GoogleDrivePickerDirective,
    NotificationComponent,
    InviteMembersComponent,
    NewLabelComponent,
    LimitPipe,
    SpeechRecognizerComponent,
    CummulativeChartComponent,
    // NvD3Component,
    InlineDropdownComponent,
    DropdownvaluePipe,
    DropdownfilterComponent,
    TextfilterComponent,
    ChecklistComponent,
    SelecteddropdownPipe,
    ReportComponent,
  ],

  entryComponents: [
    AlertBoxComponent,
    EditTaskComponent,
    InviteMembersComponent,
    AddNewTaskComponent,
    NewLabelComponent,
    InlineTextComponent,
    SpeechRecognizerComponent
  ],

  imports: [
    CommonModule,
    FormsModule,
    // MatNativeDateModule,
    // MatDatepickerModule,
    DragScrollModule,
    DragulaModule,
    NvD3Module,
    DndModule.forRoot(),
    InfiniteScrollModule,
    ColorPickerModule,
    PopoverModule,
    MomentModule,
    FileUploadModule,
    CdkTableModule,
    MaterialModule,
    HttpModule,
    SharedModule
  ],

  providers: [
    TaskerService,
    PhaseService,
    MemberService,
    TaskService,
    TimerService,
    CommentService,
    PriorityPipe,
    TabService,
    ToastrService,
    AttachmentsService,
    TagsService,
    LoginHistoryService,
    SocketService,
    SpeechRecognitionService,
    CummulativeChartService,
    TaskFilterService,
    ReportService
  ],

  exports: [
    AppHeaderComponent,
    SpeechRecognizerComponent,
    TagsComponent
  ]

})
export class TaskModule { }
