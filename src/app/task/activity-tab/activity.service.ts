import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class ActivityService {

  constructor(private _http: Http) { }

  getActivities(taskId) {
    return this._http.get('/history/' + taskId).map(res => res.json());
  }

}
