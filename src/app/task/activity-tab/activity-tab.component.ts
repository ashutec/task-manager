import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivityService } from './activity.service';
import { Task } from '../tasks/task';
import { TabService } from '../shared/tabs.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-activity-tab',
  templateUrl: './activity-tab.component.html',
  styleUrls: ['./activity-tab.component.css'],
  providers: [ActivityService],
})

export class ActivityTabComponent implements OnInit, OnDestroy {

  @Input() task: Task
  @Input() TabElement: any;
  activities: Array<any> = [];
  isRefreshing: Boolean = false;
  subscription: Subscription;
  loading = false;

  constructor(private _activityService: ActivityService,
    private _tabService: TabService,
  ) { }

  ngOnInit() {
    this.subscription = this._tabService.Tab.filter(tab => tab === this.TabElement.textLabel).subscribe(() => {
      if (this.activities.length === 0) {
        this.loading = true;
      }
      this.isRefreshing = true;
      this._activityService.getActivities(this.task._id).subscribe(activities => {
        this.activities = activities;
        this.isRefreshing = false;
        this.loading = false;
      });
      // this._loaderService.hide();
    })
  }

  ngOnDestroy() {
    if (!!this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
