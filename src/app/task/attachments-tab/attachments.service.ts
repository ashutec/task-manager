import { Injectable } from '@angular/core';
import { Http, ResponseContentType, RequestOptionsArgs } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AttachmentsService {

  attachmentSubject = new BehaviorSubject<any>([]);
  constructor(private _http: Http) { }

  getFiles(taskId) {
    return this._http.post('/task/getFiles', { taskId: taskId }).map(result => result.json());
  }

  NewAttachments(attachments) {
    this.attachmentSubject.next(attachments);
  }

  getAttachments() {
    return this.attachmentSubject;
  }

  deleteAttachment(taskId, fileId) {
    const body: RequestOptionsArgs = {
      body: {
        taskId: taskId,
        fileId: fileId,
      }
    }
    return this._http.delete('/task/deleteAttachment', body).map(result => result.json());
  }

  downloadAttachment(fileName, token) {
    const url = `/task/downloadFile/${fileName}?token=${token}`;
    this._http.get(url).subscribe(data => window.open(URL.createObjectURL(data.json())));
  }

  uploadNewAttachment(taskId, url, filename, source) {
    const body = {
      taskId,
      filename,
      source,
      url
    }
    return this._http.post('/task/fileUpload', body).map(res => res.json())
    // .map(task => task.attachments.filter(attachment => !attachment.isDeleted))
    // .map(attachments => this.NewAttachments(attachments))
  }
}
