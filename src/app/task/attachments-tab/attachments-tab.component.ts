import { Component, OnInit, Input, OnDestroy, NgZone } from '@angular/core';
import { TaskerService } from '../task-manager/tasker.service';
import { FileUploader, FileUploaderOptions, Headers } from 'ng2-file-upload';
import { TokenService } from '../../http-interceptor/token.service'
import { Task } from '../tasks/task';
import { AttachmentsService } from './attachments.service';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { ControlValueAccessor } from '@angular/forms';
import { environment } from '../../../environments/environment';
import { AlertBoxComponent } from '../alert-box/alert-box.component'
import { MatDialog, MatDialogRef } from '@angular/material'
import { attachmentType } from './attachments.types';
import { TabService } from '../shared/tabs.service';
import { Subject, Subscription } from 'rxjs';
import { ToastrService } from '../shared/toaster.service';
import { AuthorizationService } from "../../shared/Authorization.service";
import { Privilages } from "../../shared/privilages.enum";
import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-attachments-tab',
  templateUrl: './attachments-tab.component.html',
  styleUrls: ['./attachments-tab.component.css'],
})
export class AttachmentsTabComponent implements OnInit, OnDestroy {

  @Input() task: Task;
  @Input() TabElement: any;
  uploadToggle = true;
  tableToggle = true;
  files = [];
  token = this._tokenService.getToken();
  displayedColumns = ['userName', 'uploadBy', 'download', 'delete'];
  dataSource: AttachmentDataSource;
  apiURL = environment.apiURL;
  uploader: FileUploader;
  dialogRef: MatDialogRef<AlertBoxComponent>;
  buttonText;
  uploadOption = true;
  public hasBaseDropZoneOver = false;
  subscription: Subscription;
  public privilages = Privilages;
  workspace;

  constructor(private _taskerService: TaskerService,
    public _toastr: ToastrService,
    public _authorizationService: AuthorizationService,
    private _tokenService: TokenService,
    private _dialog: MatDialog,
    public _attachmentsService: AttachmentsService,
    private _tabService: TabService,
    private ngZone: NgZone) {
    this.workspace = this._taskerService.workspace.value;
    this.uploader = new FileUploader({ url: this.apiURL + '/task/fileUpload', headers: [{ name: 'x-wsid', value: this.workspace._id }] });
    this.uploader.onCompleteAll = () => {
      // tslint:disable-next-line:max-line-length
      // this.task.attachmentCount = this.task.attachmentCount ? (this.task.attachmentCount + this.uploader.queue.length) : this.uploader.queue.length;
      // this._taskerService.taskUpdated.next(this.task);
      this.getNewAttachments();
      this.uploader.clearQueue();
    }
  }

  uploadToggleFun() {
    this.uploadToggle = !this.uploadToggle;
    if (this.uploadToggle) { this.buttonText = 'Cancel'; } else { this.buttonText = 'Upload File'; }
  }

  ngOnInit() {
    // this.subscription = this._tabService.Tab.filter(tab => tab === this.TabElement.textLabel).subscribe(() => {
      if (this._authorizationService.userCan(Privilages.CAN_DOWNLOAD_ATTACHMENT)) {
        this.dataSource = new AttachmentDataSource(this._attachmentsService);
        this.getNewAttachments();
        let file: FileUploaderOptions;
        file = {
          additionalParameter: {
            result:
            [JSON.stringify({
              taskId: this.task._id,
              source: attachmentType.local
            })],
          },
        }
        this.uploader.setOptions(file);
        this.uploader.authTokenHeader = 'x-access-token';
        this.uploader.authToken = this.token;
      }
    // });
  }

  downloadFile(row) {
    this._attachmentsService.downloadAttachment(row.fileName, this.token);
  }

  public deleteAttachment(file) {
    if (this._authorizationService.userCan(Privilages.CAN_DELETE_ATTACHMENT)) {
      this.dialogRef = this._dialog.open(AlertBoxComponent, { width: '500px' });
      this.dialogRef.afterClosed().subscribe(res => {
        if (res) {
          this._attachmentsService.deleteAttachment(this.task._id, file._id)
            .subscribe(result => {
              this.getNewAttachments();
            });
        }
      })
    } else {
      this._toastr.showError(this._authorizationService.errorMessage);
    }
  }

  public SaveGDriveFileUrl(fileDate: any[]) {
    if (this._authorizationService.userCan(Privilages.CAN_ADD_ATTACHMENT)) {
      for (const file of fileDate) {
        this._attachmentsService.uploadNewAttachment(this.task._id, file.url, file.filename, attachmentType.GDrive).subscribe(() => {
          this.getNewAttachments();
        });
      }
    } else {
      this._toastr.showError(this._authorizationService.errorMessage);
    }
  }

  public getNewAttachments() {
    if (this._authorizationService.userCan(Privilages.CAN_ADD_ATTACHMENT)) {
      this._attachmentsService.getFiles(this.task._id).subscribe(attachments => {
        this.task.attachments = attachments.attachments;
        this.task.attachmentCount = this.task.attachments.length;
        this._taskerService.taskUpdated.next(this.task);
        this._attachmentsService.NewAttachments(attachments.attachments);
        if (attachments.attachments.length) {
          this.uploadToggle = false;
          this.tableToggle = false;
        }
        if (this.uploadToggle) { this.buttonText = 'Cancel'; } else { this.buttonText = 'Upload File'; }
      });
    } else {
      this._toastr.showError(this._authorizationService.errorMessage);
    }
  }

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}

class AttachmentDataSource extends DataSource<any> {
  constructor(private _attachmentService: AttachmentsService) {
    super();
  }

  connect(): Observable<any> {
    this._attachmentService.attachmentSubject.subscribe(result => { });
    return this._attachmentService.attachmentSubject;
  }

  disconnect() { }
}

