// let Priority = [
//     { level: 'Low', priority: '1' },
//     { level: 'Medium', priority: '2' },
//     { level: 'High', priority: '3' },
// ]
// export enum Priority {
//     High = 1,
//     Medium = 2,
//     Low = 3,
// }
export var Priority: Array<any> = [
    // High: 1,
    // Medium: 2,
    // Low: 3,
    { level: 'High', priority: 1, color: '#ef9b0f' },
    { level: 'Medium', priority: 2, color: '#a6ef0f' },
    { level: 'Low', priority: 3, color: '#0f9fef' }
];


