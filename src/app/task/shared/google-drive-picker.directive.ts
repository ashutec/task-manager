import { Directive, HostListener, Output, EventEmitter, NgZone } from '@angular/core';
import { } from 'gapi.drive';
declare var google;

@Directive({
  selector: '[GoogleDrivePicker]'
})
export class GoogleDrivePickerDirective {

  @HostListener('click') onMouseClick() {
    this.onApiLoad();
  }

  @Output('onFileSelected') onFileSelected: EventEmitter<string[]> = new EventEmitter();

  // The Browser API key obtained from the Google API Console.
  developerKey = 'AIzaSyBwPhB06a3xDncKzOgnT8uKzGEXqwB3Lzc';

  // The Client ID obtained from the Google API Console. Replace with your own Client ID.
  clientId = "211547030228-nq4hhr3nk37l4grb4khor98h425vqcc4.apps.googleusercontent.com"

  // Scope to use to access user's photos.
  scope = ['https://www.googleapis.com/auth/drive.readonly'];

  pickerApiLoaded = false;
  oauthToken;

  constructor(private _ngZone: NgZone) { }

  onApiLoad() {
    let self = this;
    if (!this.oauthToken) {
      gapi.load('auth', self.onAuthApiLoad.bind(this));
    }
    gapi.load('picker', self.onPickerApiLoad.bind(this));
  }

  onAuthApiLoad() {
    let self = this;
    gapi.auth.authorize(
      {
        'client_id': self.clientId,
        'scope': self.scope,
        'immediate': false
      },
      self.handleAuthResult.bind(self));
  }

  onPickerApiLoad() {
    this.pickerApiLoaded = true;
    this.createPicker();
  }

  handleAuthResult(authResult) {
    let self = this;
    if (authResult && !authResult.error) {
      this.oauthToken = authResult.access_token;
      this.createPicker();
    }
  }

  createPicker() {
    if (this.pickerApiLoaded && this.oauthToken) {
      let picker = new google.picker.PickerBuilder().
        addView(google.picker.ViewId.DOCS).
        enableFeature(google.picker.Feature.NAV_HIDDEN).
        enableFeature(google.picker.Feature.MULTISELECT_ENABLED).
        setOAuthToken(this.oauthToken).
        setDeveloperKey(this.developerKey).
        setCallback(this.pickerCallback.bind(this)).
        build();
      picker.setVisible(true);
    }
  }

  pickerCallback(data) {
    if (data[google.picker.Response.ACTION] == google.picker.Action.PICKED) {
      if (data.docs.length > 0) {
        // console.log(data.docs);
        this._ngZone.run(() => {
          this.onFileSelected.emit(data.docs.map((doc) => {
            return {
              url: doc.url,
              filename: doc.name,
            }
          }))
        });
      }
    }
  }

}
