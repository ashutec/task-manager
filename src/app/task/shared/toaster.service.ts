import { Injectable, ViewContainerRef } from '@angular/core';
import { ToasterService } from 'angular2-toaster';


@Injectable()
export class ToastrService {

    vcr: ViewContainerRef;
    constructor(public toastr: ToasterService) {
    }

    showSuccess(message) {
        this.toastr.pop('success', 'Success!', message)
    }

    showError(message?: string) {
        this.toastr.pop('error', 'Oops!', message || 'Something went Wrong! Please try again...')
    }

    showWarning(message) {
        this.toastr.pop('warning', 'Oh Hooo..', message)
    }

    showInfo(message) {
        this.toastr.pop('info', message);
    }

    // showCustom(message) {
    //     this.toastr.custom('<span style="color: red">Message in red.</span>', null, { enableHTML: true });
    // }
}
