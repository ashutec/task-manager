import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class TabService {

    Tab: Subject<any> = new Subject<any>();
    constructor() { }

    newActiveTab(tabId) {
        console.log(tabId);
        this.Tab.next(tabId);
    }
}
