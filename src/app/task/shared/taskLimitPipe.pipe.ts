import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'TaskLimit'
})

export class LimitPipe implements PipeTransform {
    transform(list: any[], value: any): any {
        if (value && list.length) {
            let Limit = parseInt(value);
            return list.slice(0, Limit);
        }
        return list;
    }
}