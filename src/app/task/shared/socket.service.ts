import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import * as io from 'socket.io-client';
import { environment } from '../../../environments/environment';
import { TokenService } from '../../http-interceptor/token.service'


@Injectable()
export class SocketService {
  private url = environment.socketURL;
  private socket;

  constructor(private _tokenService: TokenService) { }
  start() {
    this.socket = io(this.url);
    // console.log(this.socket);
  }

  sendMessage(status, statusData) {
    this.socket.emit(status, statusData);
  }

  getMessage(status): Observable<any> {
    const observable = new Observable((observer) => {
      this.socket.on(status, (res) => {
        observer.next(res);
      })
    })
    return observable;
  }

  stop(room) {
    this.sendMessage('leave', room);
    this.socket.disconnect();
  }
}
