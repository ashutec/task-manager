import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dropdownvalue'
})
export class DropdownvaluePipe implements PipeTransform {

 transform(value: any, options?: [any]): any {
    const seloption = options.find(option => option.id == value);
    if (seloption && seloption.value) {
      return seloption.value
    }
    return 'Not Set';
  }

}
