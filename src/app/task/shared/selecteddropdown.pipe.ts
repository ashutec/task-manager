import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'selecteddropdown'
})
export class SelecteddropdownPipe implements PipeTransform {

  transform(value: [any], options?: [any]): any {
    const selOption = value.map((selValue) => {
      const matchOption = options.find(option => option.id == selValue);
      return matchOption ? matchOption.value : 'Not Set';
    });
    if (selOption && selOption.length) {
      return selOption.join(', ')
    }
    return 'Any';
  }

}
