import { Injectable } from '@angular/core';
import { SpeechRecognitionService } from '../speech-recognizer/speech-recognition.service';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class TaskFilterService {

  users: Subject<any> = new Subject<any>();

  constructor(public _speechRecognizerService: SpeechRecognitionService) {
    this._speechRecognizerService.userFilter.subscribe((userNameArray) => {
      this.users.next(userNameArray);
    })
  }

}
