import { Component, OnInit, Output, EventEmitter, Input, ViewChild, ElementRef } from '@angular/core';
import { MemberService } from '../invite-members/member.service';
import { Router } from '@angular/router';
import * as intersectionWith from 'lodash/intersectionWith';
import * as differenceWith from 'lodash/differencewith';
import * as join from 'lodash/join';
import * as find from 'lodash/find';
import { Priority } from '../shared/Priority';
import { TaskerService } from '../task-manager/tasker.service';
import { TagsService } from '../tags/tags.service';
import { CustomFieldTypes } from '../../workspace/custom-field-settings/customFieldTypes';
import { SpeechRecognitionService } from '../speech-recognizer/speech-recognition.service';
import { TaskFilterService } from '../task-filter/task-filter.service';


@Component({
  selector: 'app-task-filter',
  templateUrl: './task-filter.component.html',
  styleUrls: ['./task-filter.component.css']
})
export class TaskFilterComponent implements OnInit {

  @ViewChild('userFilterElement') userFilterElement: ElementRef;
  @ViewChild('tagFilterElement') tagFilterElement: ElementRef;
  @Output() onClose = new EventEmitter<boolean>();

  name: string;
  Users = [];
  SelectedUsers = [];
  UserCopy = [];
  public Priority = Priority;
  userFilter = [];
  priorityFilter = [];
  dueDateFilter = [];
  selectedPriority = [];
  fromDate = null;
  toDate = null;
  collapsible = [];
  selectedString = [];
  customFields = [];
  public customFieldTypes = CustomFieldTypes;
  clearAllEvent = new EventEmitter();
  customFilterObj = {};

  // tag //
  tagName: string;
  tags = [];
  selectedTags = [];
  copyTags = [];
  // tag //

  FilterObj = {
    userFilter: this.userFilter,
    priorityFilter: this.priorityFilter,
    dueDateFilter: this.dueDateFilter,
    tagFilter: this.selectedTags.map(selectedTags => selectedTags._id),
    customFilter: this.customFilterObj,
  }

  constructor(private _memberService: MemberService,
    private _speechRecognizerService: SpeechRecognitionService,
    private _router: Router,
    private _taskerService: TaskerService,
    private _tagsService: TagsService,
    private _taskFilterService: TaskFilterService) {
  }

  ngOnInit() {
    this.Initialize();
  }

  Initialize() {
    this._taskerService.workspace.subscribe((workspace) => {
      if (workspace.tags) {
        this.tags = workspace.tags;
        this.copyTags = workspace.tags.map(function (obj) { return { ...obj } });
      }
      if (workspace.members) {
        this.Users = workspace.member.map(member => member.user);
        this.Users.push({
          firstname: 'Not Assigned',
          lastname: '',
          profile: 'assets/img/login-profile.png',
          _id: '',
        });
        this.UserCopy = JSON.parse(JSON.stringify(this.Users));
      }
      if (workspace._id) {
        this._taskerService.getCustomFields().subscribe(customfields => this.customFields = customfields);
      }
      this.clearAll();
    });
    this.selectedStrings();
    this.chatbotFilters();
  }

  titleClick(num) {
    for (let i = 0; i < this.collapsible.length; i += 1) {
      this.collapsible[i] = false;
    }
    this.collapsible[num] = true;
    this.filterTask();
  }

  addUser(user, event) {
    this.SelectedUsers.push(user);
    this.Users = differenceWith(this.UserCopy, this.SelectedUsers, this.idChecker);
    if (event.type === 'click') {
      this.userFilterElement.nativeElement.blur();
    }
    this.filterTask();
  }

  filter() {
    this.Users = this.UserCopy.filter(user => new RegExp(`^${this.name}`, 'gi').test(user.firstname));
    this.Users = differenceWith(this.Users, this.SelectedUsers, this.idChecker);
  }

  deleteUser(SelectedUser) {
    this.SelectedUsers.splice(this.SelectedUsers.indexOf(SelectedUser), 1);
    this.Users = differenceWith(this.UserCopy, this.SelectedUsers, this.idChecker);
    this.filterTask();
  }

  clearUsers(ev) {
    if (ev) {
      ev.stopPropagation();
    }
    this.SelectedUsers = [];
    this.Users = differenceWith(this.UserCopy, this.SelectedUsers, this.idChecker);
    this.filterTask();
  }

  filterTask() {
    this.dueDateFilter = [];
    this.selectedStrings();
    if (this.fromDate) { this.dueDateFilter.push(this.fromDate); }
    if (this.toDate) { this.dueDateFilter.push(this.toDate); }
    this.userFilter = [];
    this.SelectedUsers.map(selectedUser => this.userFilter.push(selectedUser._id));

    this.FilterObj.userFilter = this.userFilter;
    this.FilterObj.priorityFilter = this.priorityFilter;
    this.FilterObj.dueDateFilter = this.dueDateFilter;
    this.FilterObj.tagFilter = this.selectedTags.map(selectedTags => selectedTags._id);
    this._taskerService.filterTask(this.FilterObj);
    // this.onClose.emit(true);
  }

  idChecker(result, SelectedTag) {
    return result._id === SelectedTag._id;
  }

  clearDate(ev) {
    if (ev) {
      ev.stopPropagation();
    }
    this.toDate = null;
    this.fromDate = null;
    this.filterTask();
  }

  clearPriority(ev) {
    if (ev) {
      ev.stopPropagation();
    }
    this.priorityFilter = [];
    this.filterTask();
  }

  clearAll() {
    this.SelectedUsers = [];
    this.priorityFilter = [];
    this.toDate = null;
    this.fromDate = null;
    this.selectedTags = [];
    this.clearAllEvent.emit();
    this.filterTask();
  }

  filterResponse(res) {
    const filterObj = JSON.parse(res);
    const key = Object.keys(filterObj)[0];
    if (filterObj[key] && filterObj[key].length) {
      this.customFilterObj[key] = filterObj[key];
    } else {
      if (this.customFilterObj[key]) {
        delete this.customFilterObj[key];
      }
    }
    // this.FilterObj.customFilter = this.customFilterObj;
    this._taskerService.filterTask(this.FilterObj);
  }

  selectedStrings() {
    this.selectedString = [];
    if (this.SelectedUsers.length) {
      this.selectedString[0] = join(this.SelectedUsers.map(selectedUser => selectedUser.firstname), ', ');
    } else {
      this.selectedString[0] = 'Any';
    }
    if (this.fromDate && this.toDate) {
      this.selectedString[1] = join([this.fromDate.toString().substr(0, 15), this.toDate.toString().substr(0, 15)], ', ');
    } else if (this.fromDate) {
      this.selectedString[1] = join([this.fromDate.toString().substr(0, 15)], ', ');
    } else {
      this.selectedString[1] = 'Any';
    }
    if (this.priorityFilter.length) {
      const result = [];
      for (let i = 0; i < this.priorityFilter.length; i += 1) {
        result.push(this.Priority[this.priorityFilter[i] - 1].level);
      }
      this.selectedString[2] = join(result, ', ')
    } else {
      this.selectedString[2] = 'Any';
    }
    // this.selectedString.push('Any');
    if (this.selectedTags.length) {
      this.selectedString[3] = join(this.selectedTags.map(selectedTag => selectedTag.tagName), ', ');
    } else {
      this.selectedString[3] = 'Any';
    }
  }

  // tag //

  tagFilter() {
    this.tags = this.copyTags.filter(filteredTagOption => new RegExp(`^${this.tagName}`, 'gi')
      .test(filteredTagOption.tagName));
    this.tags = differenceWith(this.tags, this.selectedTags, this.tagIdChecker);
  }

  addTag(tag) {
    this.selectedTags.push(tag);
    this.tags = differenceWith(this.copyTags, this.selectedTags, this.tagIdChecker);
    this.tagFilterElement.nativeElement.blur();
    this.filterTask();
  }

  tagIdChecker(copyTag, SelectedTag) {
    return copyTag._id === SelectedTag._id;
  }

  deleteTag(tag) {
    this.selectedTags.splice(this.selectedTags.indexOf(tag), 1);
    this.tags = differenceWith(this.copyTags, this.selectedTags, this.tagIdChecker);
    this.filterTask();
  }

  clearTags(ev) {
    if (ev) {
      ev.stopPropagation();
    }
    this.selectedTags = [];
    this.tagFilter();
    this.filterTask();
  }

  chatbotFilters() {
    this._taskFilterService.users.subscribe((userArray) => {
      this.titleClick(0);
      for (let i = 0; i < userArray.length; i++) {
        this.name = userArray[i];
        const userObj = find(this.UserCopy, (user) => {
          if (user.firstname.toLowerCase() === this.name.toLowerCase()) {
            return user;
          }
        });
        if (userObj) {
          this.filter();
          this.addUser(userObj, '');
        }
      }
    });
    this._speechRecognizerService.clearAllFilter.subscribe((filterType) => {
      if (filterType === 'all' || filterType === 'no') {
        this.clearAll();
      }

    })

  }
}
