import { Injectable } from '@angular/core';
import { Http } from '@angular/http'

@Injectable()
export class TagsService {

  constructor(private _http: Http) { }

  getTags(wsId) {
    return this._http.post('/workspace/getTags', { wsId }).map(result => result.json());
  }

  setTagWorkspace(wsId, tag: any) {
    return this._http.post('/workspace/setTagWorkspace', { wsId, tag }).map((result) => {
      const workspace = result.json();
      return workspace.tags;
    });
  }

  updateTag(wsId, tag: any) {
    return this._http.put('/workspace/updateTag', { wsId, tag }).map((result) => {
      const workspace = result.json();
      return workspace.tags;
    });
  }

  // local filter by text box
}
