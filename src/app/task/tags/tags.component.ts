import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';
import { NewLabelComponent } from "../new-label/new-label.component";
import { Router, ActivatedRoute } from '@angular/router';
import { TagsService } from './tags.service';
import * as differenceWith from 'lodash/differencewith';
import * as intersectionWith from 'lodash/intersectionWith';
import * as join from 'lodash/join';
import { TaskerService } from '../task-manager/tasker.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.css']
})
export class TagsComponent implements OnInit {

  tagOptions = [];
  filteredTagOptions = [];
  // wsId;
  tagEdit;
  tagString;
  name: String;
  dialogref: MatDialogRef<NewLabelComponent>;
  @Input() taskTags = [];
  @Input() wsId: string;
  @Output() sendTag = new EventEmitter<String>();
  selectedTags = [];
  addTagName = false;
  isEdit = false;
  @ViewChild('tagFilterElement') tagFilterElement: ElementRef;

  constructor(private _router: Router,
    private _tagService: TagsService,
    private _taskerService: TaskerService,
    private _matDialog: MatDialog,
    private _activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.getTags();
  }

  getTags() {
    this._taskerService.workspace.subscribe(result => {
      if (result.tags) {
        this.tagOptions = result.tags.map(function (obj) { return { ...obj } });
        this.selectedTags = intersectionWith(this.tagOptions, this.taskTags, this.idCheckerSelectedTag);
        this.filteredTagOptions = differenceWith(this.tagOptions, this.selectedTags, this.idChecker);
        this.tagString = join(this.selectedTags.map(selectedTag => selectedTag.tagName), ', ');
      } else {
        this.tagOptions = [];
        this.selectedTags = [];
        this.filteredTagOptions = [];
        this.tagString = '';
      }
    });
  }

  idCheckerSelectedTag(SelectedTag, tagId) {
    return SelectedTag._id === tagId;
  }

  selectOption(filteredTagOption) {
    this.selectedTags.push(filteredTagOption);
    this.filteredTagOptions = differenceWith(this.tagOptions, this.selectedTags, this.idChecker);
    this.tagFilterElement.nativeElement.blur();
    this.addTagName = false;
  }

  idChecker(result, SelectedTag) {
    return result._id === SelectedTag._id;
  }

  filter() {
    this.filteredTagOptions = this.tagOptions.filter(filteredTagOption => new RegExp(`^${this.name}`, 'gi')
      .test(filteredTagOption.tagName));
    this.filteredTagOptions = differenceWith(this.filteredTagOptions, this.selectedTags, this.idChecker);
    this.showAddTag();
  }

  deleteTag(SelectedTag) {
    this.selectedTags.splice(this.selectedTags.indexOf(SelectedTag), 1);
    this.filteredTagOptions = differenceWith(this.tagOptions, this.selectedTags, this.idChecker);
  }

  showAddTag() {
    if (this.name.length === 0) {
      this.addTagName = false;
    } else {
      if (this.tagOptions.find((filteredTagOption) => filteredTagOption.tagName === this.name)) {
        this.addTagName = false;
      } else {
        this.addTagName = true;
      }
    }
  }

  setTagWorkspace() {
    this.isEdit = false;
    const config = new MatDialogConfig();
    config.width = '300px';
    config.data = { tagName: this.name };
    this.dialogref = this._matDialog.open(NewLabelComponent, config);
    this.addTagName = false;
    this.dialogref.afterClosed().flatMap((tagData) => {
      if (tagData) {
        return this._tagService.setTagWorkspace(this.wsId, tagData)
      }
      else {
        this.name = '';
        return Observable.never();
      }
    }).subscribe((tags) => {
      this.loadTagsAfterSave(tags);
    }, (err) => {
      console.log(err);
    });
  }

  setTagTask() {
    const arrayOfTagId = this.selectedTags.map((selectedTag) => selectedTag._id);
    this.tagString = join(this.selectedTags.map(selectedTag => selectedTag.tagName), ', ');
    this.sendTag.emit(JSON.stringify(arrayOfTagId));
  }

  editTag(ev, tag) {
    if (ev)
      ev.stopPropagation();
    this.isEdit = true;
    const config = new MatDialogConfig();
    config.width = '300px';
    config.data = tag;
    this.dialogref = this._matDialog.open(NewLabelComponent, config);
    this.dialogref.afterClosed().flatMap((tagData) => {
      if (tagData)
        return this._tagService.updateTag(this.wsId, tagData)
      else {
        this.name = '';
        return Observable.never();
      }
    })
      .subscribe(tags => {
        this.filteredTagOptions = differenceWith(tags, this.selectedTags, this.idChecker);
        this.tagFilterElement.nativeElement.blur();
        this.addTagName = false;
      });
  }

  loadTagsAfterSave(tags) {
    this.tagOptions = tags.map(function (obj) { return { ...obj } });
    this.filteredTagOptions = tags;
    const tag = this.filteredTagOptions.find((filteredTagOption) => filteredTagOption.tagName === this.name);
    this.selectOption(tag);
    this.name = '';
  }

}
