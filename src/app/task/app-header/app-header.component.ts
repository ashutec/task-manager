import { Component, OnInit, Inject } from '@angular/core';
import { InviteMembersComponent } from '../invite-members/invite-members.component';
import { MatDialog, MatSnackBar, MatDialogConfig } from '@angular/material';
import { Router, ActivatedRoute, NavigationEnd, RouterState } from '@angular/router';
import { TokenService } from '../../http-interceptor/token.service';
import { User } from '../../authentication/user';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.css']
})
export class AppHeaderComponent implements OnInit {
  userName = '';
  taskName = 'Todo List';
  profileLink: string;
  InviteMembers: Boolean = false;

  constructor(public dialog: MatDialog,
    private router: Router,
    private _tokenService: TokenService,
    private snackBar: MatSnackBar,
    private _activatedRoute: ActivatedRoute,
  ) { }

  inviteMember() {
    const config = new MatDialogConfig();
    const dialogRef = this.dialog.open(InviteMembersComponent, { height: '400px', width: '600px' });
  }

  ngOnInit() {
    this.profileLink = this._tokenService.getLoggedInUser().profile;
    this.showInvitees();
    this.router.events.filter(route => (route instanceof NavigationEnd)).subscribe((route: NavigationEnd) => {
      this.showInvitees();
    });
  }

  showInvitees() {
    this._activatedRoute.children[0].data.subscribe((data) => {
      if (data.name === 'TaskManager') {
        this.InviteMembers = true;
      } else {
        this.InviteMembers = false
      }
    });
  }

  logout() {
    this._tokenService.loggedinUser = new User();
    this._tokenService.logout();
    this.router.navigate(['/auth']);
  }

  goPrifile() {
    this.router.navigate(['/auth/profile']);
  }

  changeProfilePicture() {
    this.profileLink = this._tokenService.getLoggedInUser().profile;
  }

  goWorkspace() {
    this.router.navigate(['/workspace']);
  }

  goReports() {
    this.router.navigate(['/auth/report']);
  }
  goLoginHostory() {
    this.router.navigate(['/auth/loginHistory']);
  }
}
