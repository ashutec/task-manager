import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FieldNameAlias } from '../shared/fieldNameAlias';
import { CustomField } from '../../workspace/custom-field-settings/CustomField';

@Component({
  selector: 'app-inline-text',
  templateUrl: './inline-text.component.html',
  styleUrls: ['./inline-text.component.css']
})
export class InlineTextComponent implements OnInit {

  @Input('name') name: any;
  @Input('value') value: any;
  @Input('customFieldProperties') customFieldProperties: CustomField;
  @Output() updatedField = new EventEmitter<String>();
  inputText: boolean;
  modelValue: string;

  constructor() { }

  ngOnInit() {
    if (!this.customFieldProperties) {
      this.customFieldProperties = new CustomField();
    }
    this.modelValue = this.value;
  }

  updateField() {
    const obj = {}
    if (!this.customFieldProperties._id) {
      obj['type'] = 'taskField';
      obj['field'] = FieldNameAlias[this.name];
    } else {
      obj['type'] = 'customField';
      obj['customFieldId'] = this.customFieldProperties._id;
    }
    obj['value'] = this.value;
    const objString = JSON.stringify(obj);
    // this.value = value;
    this.updatedField.emit(objString);
  }

}
