import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Router } from '@angular/router'
import { SocketService } from '../shared/socket.service';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';
import { TokenService } from '../../http-interceptor/token.service';
import { NotificationService } from './notification.service';
import { Notification } from './notification';
import { EditTaskComponent } from '../edit-task/edit-task.component';
import { TaskerService } from '../task-manager/tasker.service';
import { AlertBoxComponent } from '../alert-box/alert-box.component';
import { SocketActionService } from '../../shared/SocketAction.service';
import { SocketAction } from '../../shared/Socket.actions';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css'],
  providers: [NotificationService],
})
export class NotificationComponent implements OnInit {
  @Input() result = '';
  @Output() notificationEvent = new EventEmitter<Notification>();
  notificationCounter: Number;
  notifications: Notification[] = new Array<Notification>();
  dialogref: MatDialogRef<AlertBoxComponent>;
  skipCount: number = 0
  unReadCount = 0;
  type;
  Limit: number = 10;
  isEnd: boolean = false;
  // notifications;

  constructor(private _socketService: SocketService,
    private _tokenService: TokenService,
    private _notificationService: NotificationService,
    private _taskerService: TaskerService,
    public dialog: MatDialog,
    private router: Router,
    private _socketActionService: SocketActionService,
  ) { }

  ngOnInit() {
    this.getNewNotificaions();
    this._socketService.start();
    this._socketService.sendMessage('newUser', this._tokenService.getLoggedInUser().id);
    this._socketActionService.RegisterNewSubscription(SocketAction.New_Notificaion, (notification) => {
      this.notifications.unshift(notification);
      this.unReadCount++;
    })
    // this._socketService.getMessage('newNotification').subscribe(notification => {
    //   this.notifications.push(notification);
    //   this.unReadCount++;
    // });
  }

  getNewNotificaions() {
    if (!this.isEnd)
      this._notificationService.getNotifications(this.skipCount).subscribe(notifications => {
        if (notifications.length) {
          this.skipCount += notifications.length;
          this.notifications = this.notifications.concat(notifications);
          this.unReadCount = notifications.filter(notification => notification.isRead === false).length;
        } else {
          this.isEnd = true;
        }
      });
  }

  openNotification(notification: Notification) {
    // this.notificationEvent.emit(notification);
    this.router.navigate([`workspace/${notification.sourceId.wsId}`]);
    const config = new MatDialogConfig();
    this._taskerService.getSingleTask(notification.sourceId._id).subscribe(task => {
      if (notification.type === 1) {
        this.type = 0;
      } else if (notification.type === 2) {
        this.type = 1;
      }
      config.data = Object.assign({}, task, { activeTabId: this.type });
      config.width = '700px';
      config.disableClose = true;
      this.dialog.open(EditTaskComponent, config);
    })
  }

  public markNotificationsRead() {
    if (this.unReadCount > 0) {
      const notificationIds = this.notifications
        .filter(notificaion => !notificaion.isRead)
        .map(notification => notification.notificationId);
      this._notificationService.markNotificationsRead(notificationIds).subscribe(() => {
        this.unReadCount = 0;
      });
    } else {
      // this._notificationService.getNotifications(this.isRead).subscribe(notifications => {
      //   this.notifications = notifications;
      // });
    }
  }

  showMore() {
    this.getNewNotificaions();
    // this.Limit += 10;
  }

}
