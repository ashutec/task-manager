import { Task } from '../tasks/task';

export class Notification {
    notificationId: string;
    message: string;
    createdOn: Date;
    profile: string;
    isRead: boolean;
    sourceId: Task;
    type: Number;
}
