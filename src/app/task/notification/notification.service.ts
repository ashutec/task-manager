import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Notification } from './notification';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import { TokenService } from '../../http-interceptor/token.service';

@Injectable()
export class NotificationService {

    constructor(private _http: Http,
        private _tokenService: TokenService,
    ) { }

    getNotifications(skipCount: number): Observable<Notification[]> {
        return this._http.get('/notification?skipcount=' + skipCount).map(res => res.json())
    }

    markNotificationsRead(notificationIds): Observable<any> {
        return this._http.post('/notification/NotificationsRead/', { notificationIds })
    }

}