export class Phase {
    _id: string;
    name: string;
    allowedActions: string[];
    isEndOfProcess: boolean;
    canAddTask: boolean;
    wsId: string;
}
