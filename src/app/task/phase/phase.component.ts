import { Component, OnInit, Input, OnChanges, HostListener, Host, NgZone } from '@angular/core';
import { DragulaService } from 'ng2-dragula/ng2-dragula';
import { TaskerService } from '../task-manager/tasker.service';
import { PhaseService } from './phase.service';
import { Task } from '../tasks/task';
import { Observable } from 'rxjs/Observable';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertBoxComponent } from '../alert-box/alert-box.component';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import * as uniqBy from 'lodash/uniqBy';
import * as indexOf from 'lodash/indexOf';
import * as intersectionWith from 'lodash/intersectionWith';
import { AuthorizationService } from '../../shared/Authorization.service';
import { Privilages } from '../../shared/privilages.enum';
import { ToastrService } from '../shared/toaster.service';
import { TaskManagerComponent } from '../task-manager/task-manager.component';
import { CustomFieldTypes } from '../../workspace/custom-field-settings/customFieldTypes';

@Component({
  selector: 'app-phase',
  templateUrl: './phase.component.html',
  styleUrls: ['./phase.component.css']
})
export class PhaseComponent implements OnInit {


  @Input('phase') phase: any;
  tasks: Task[];
  wsId: string;
  phaseTitleEdit: boolean;
  filteredTasks: Task[];
  userFilter = [];
  priorityFilter = [];
  dueDateFilter: Date[] = [];
  tagFilter = [];
  customFilters = [];
  titleOrDescFilter = '';
  loading = false;
  apptask: any;
  Limit = 10;
  public privilages = Privilages;
  public customFieldTypes = CustomFieldTypes;

  constructor(
    private _router: Router,
    private _phaseService: PhaseService,
    private _activatedRoute: ActivatedRoute,
    private _taskerService: TaskerService,
    private dialog: MatDialog,
    public _toastr: ToastrService,
    public _authorizationService: AuthorizationService,
    @Host() public parent: TaskManagerComponent,
    private ngZone: NgZone,
  ) {
    this._activatedRoute.params.subscribe(res => this.wsId = res['Id']);
    this.tasks = new Array<Task>();
  }

  ngOnInit() {
    this._taskerService.taskFilter.subscribe(filter => {
      this.userFilter = filter.userFilter || [];
      this.priorityFilter = filter.priorityFilter || [];
      this.dueDateFilter = filter.dueDateFilter || [];
      this.tagFilter = filter.tagFilter || [];
      this.titleOrDescFilter = filter.titleOrDescFilter || '';
      this.customFilters = filter.customFilter || {};
      this.TaskFilter();
    });

    this.loading = true;

    this._phaseService.getTasks(this.phase._id).subscribe(tasks => {
      this.tasks = tasks
      this.filteredTasks = tasks.map(function (obj) { return { ...obj } });
      // this.TaskFilter();
      this.loading = false;
    });

    this._taskerService.taskAdded.filter(task => task.phase === this.phase._id).subscribe(task => {
      this.tasks.unshift(task)
    });

    this._taskerService.taskRemoved.filter(task => task.phase === this.phase._id).subscribe(task => {
      this.tasks.splice(this.tasks.findIndex(currTask => currTask._id === task._id), 1)
    });

    this._taskerService.taskUpdated.filter(task => task.phase === this.phase._id).subscribe(task => {
      const index = this.tasks.findIndex(currTask => currTask._id === task._id);
      this.tasks[index] = task;
    });
  }

  addNewTask() {
    this.parent.addNewTask(this.phase._id);
  }

  updatePhaseName(name, phaseId) {
    if (this._authorizationService.userCan(Privilages.CAN_EDIT_PHASE)) {
      if (name && name !== this.phase.name) {
        this._taskerService.updatePhaseName(name, phaseId, this.wsId).subscribe(phase => {
          this.phase = phase;
        });
      }
    } else {
      this._toastr.showError(this._authorizationService.errorMessage);
    }
  }

  gotoPhaseSettings() {
    const routeUrl = `/workspace/${this.wsId}/settings/phase/${this.phase._id}`;
    this._router.navigate([routeUrl]);
  }

  deletePhase(phaseId) {
    const dialogRefDelete = this.dialog.open(AlertBoxComponent, { width: '500px' });
    dialogRefDelete.afterClosed().subscribe((result: string) => {
      if (result) {
        this._taskerService.deletePhase(this.wsId, phaseId)
          .subscribe(() => { }, err => this._toastr.showError(this._authorizationService.errorMessage));
      }
    });
  }

  idCheckerSelectedTag(tags, SelectedTag) {
    return tags === SelectedTag;
  }

  showMore() {
    this.Limit += 10;
  }

  TaskFilter() {
    this.tasks = this.filteredTasks.filter(task => {

      let user, priority, dueDate, tag, titleOrDesc;

      // user filter
      if (this.userFilter.length === 0) {
        user = true;
      } else if (task.assignee) {
        user = indexOf(this.userFilter, task.assignee._id) !== -1;
      } else if (!task.assignee && (indexOf(this.userFilter, '') !== -1)) {
        user = true;
      } else {
        user = false;
      }
      // user filter

      // priority filter
      if (this.priorityFilter.length === 0) {
        priority = true;
      } else if (task.priority) {
        priority = indexOf(this.priorityFilter, task.priority) !== -1;
      } else if (!task.priority && (indexOf(this.priorityFilter, '') !== -1)) {
        priority = true
      } else {
        priority = false;
      }
      // priority filter

      // due date filter
      if (this.dueDateFilter.length === 0) {
        dueDate = true;
      } else if (task.dueDate && this.dueDateFilter.length === 1) {
        // tslint:disable-next-line:max-line-length
        dueDate = (this.dueDateFilter[0].getTime() === new Date(task.dueDate).getTime());
      } else if (task.dueDate && this.dueDateFilter.length === 2) {
        // tslint:disable-next-line:max-line-length
        dueDate = (this.dueDateFilter[0].getTime() <= new Date(task.dueDate).getTime() &&
          this.dueDateFilter[1].getTime() >= new Date(task.dueDate).getTime());
      } else {
        dueDate = false;
      }
      // due date filter

      // tag filter
      if (this.tagFilter.length === 0) {
        tag = true;
      } else if (task.tags && this.tagFilter.length) {
        tag = (intersectionWith(task.tags, this.tagFilter, this.idCheckerSelectedTag).length);
      } else {
        tag = false;
      }
      // tag filter

      // Search by title or description filter
      if (this.titleOrDescFilter.length === 0 || !this.titleOrDescFilter.length) {
        titleOrDesc = true;
      } else if (task.title || task.description) {
        titleOrDesc = task.title.toLowerCase().includes(this.titleOrDescFilter.toLowerCase());
        if (!titleOrDesc && task.description) {
          titleOrDesc = task.description.toLowerCase().includes(this.titleOrDescFilter.toLowerCase());
        }
      } else {
        titleOrDesc = false;
      }
      // Search by title or description filter
      let customFieldResult = this.customFilter(task);

      let temp = user && priority && dueDate && tag && titleOrDesc && customFieldResult;
      return temp;
    });
  }

  customFilter(task: Task): boolean {
    return Object.keys(this.customFilters).every(filter => {
      if (task.customFields.findIndex(field => field.customFieldId._id === filter) < 0) {
        if (this.customFilters[filter].includes('null')) {
          return true;
        }
        return false;
      }
      if (!this.customFilters[filter]) {
        return true;
      }
      const field = task.customFields.find(field => field.customFieldId._id === filter);
      switch (field.customFieldId.type) {
        case this.customFieldTypes.Text || this.customFieldTypes.TextArea:
          return this.ifTextContains(field.value, this.customFilters[field.customFieldId._id])
        case this.customFieldTypes.Dropdown:
          return this.ifDropdownContains(this.customFilters[field.customFieldId._id], field.value)
        default:
          return false;
      }
    });
    // return task.customFields.every(field => {
    //   if (!this.customFilters[field.customFieldId._id] || this.customFilters[field.customFieldId._id] === '') {
    //     return true;
    //   } else {
    //     if (field.value) {
    //       switch (field.customFieldId.type) {
    //         case this.customFieldTypes.Text || this.customFieldTypes.TextArea:
    //           return this.ifTextContains(field.value, this.customFilters[field.customFieldId._id])
    //         case this.customFieldTypes.Dropdown:
    //           return this.ifDropdownContains(this.customFilters[field.customFieldId._id], field.value)
    //       }
    //     } else {
    //       return false;
    //     }
    //   }
    // });
  }

  ifTextContains(text: string, contains: string): boolean {
    return text.toLowerCase().includes(contains.toLowerCase());
  }

  ifDropdownContains(items: [any], contains: any): boolean {
    let exist = items.map(item => item.toString()).includes(contains);
    return exist;
  }

}
