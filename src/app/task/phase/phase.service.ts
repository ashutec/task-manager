import { Injectable } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map'
import { environment } from 'environments/environment';
// import { TaskerService } from '../task-manager/tasker.service';

@Injectable()
export class PhaseService {

  constructor(private _http: Http) { }

  getTasks(phase) {
    return this._http.get('/task?phase=' + phase).map(res => res.json());
  }

  getSinglePhase(phaseId) {
    return this._http.get('/phaseInfo/' + phaseId).map(res => res.json());
  }

  createPhase(wsId, name) {
    let phase = {
      wsId,
      name
    }
    return this._http.post('/phase', phase).map(res => res.json());
  }

  deletePhase(wsId, phaseId) {
    let phase = {
      wsId,
      phaseId
    }
    return this._http.delete('/phase', { body: phase }).map(res => res.json())
  }

  getPhases(wsId) {
    return this._http.get('/phase/' + wsId).map(res => res.json());
  }

  updatePhaseName(name, phaseId) {
    return this._http.put('/phase', {
      phase: {
        name
      },
      phaseId,
    }).map(res => res.json())
  }

  updatePhasePosition(wsId, phases) {
    return this._http.put('/phase/updatePhasePosition', {
      wsId,
      phases
    }).map(res => res.json())
  }

}
