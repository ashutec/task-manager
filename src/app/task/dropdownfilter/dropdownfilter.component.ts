import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'dropdownfilter',
  templateUrl: './dropdownfilter.component.html',
  styleUrls: ['./dropdownfilter.component.css']
})
export class DropdownfilterComponent implements OnInit, OnDestroy {

  @Input('name') name: any;
  @Input('customFieldProperties') customFieldProperties: any;
  @Input('clear') clear: EventEmitter<any>;
  @Output('updatedfield') updatedField = new EventEmitter<String>();
  dropdownFilter;
  collapsible = false;
  subscription: Subscription;
  selectedString = '';

  constructor() { }

  ngOnInit() {
    this.subscription = this.clear.subscribe(() => this.clearFilter());
  }

  saveFilter() {
    const search = {};
    search[this.customFieldProperties._id] = this.dropdownFilter;
    this.updatedField.emit(JSON.stringify(search));
  }

  clearFilter(ev?: any) {
    if (ev) {
      ev.stopPropagation();
    }
    this.dropdownFilter = null;
    this.saveFilter();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
