import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { FieldNameAlias } from '../shared/fieldNameAlias';
import { CustomField } from '../../workspace/custom-field-settings/CustomField';

@Component({
  selector: 'app-inline-textarea',
  templateUrl: './inline-textarea.component.html',
  styleUrls: ['./inline-textarea.component.css']
})
export class InlineTextareaComponent implements OnInit {

  @Input('name') name: any;
  @Input('value') value: any;
  @Input('customFieldProperties') customFieldProperties: CustomField;
  @Output() updatedField = new EventEmitter<String>();
  @ViewChild('textArea') private textArea: ElementRef;
  inputText: boolean;
  modelValue: string;

  constructor() { }

  ngOnInit() {
    if (!this.customFieldProperties) {
      this.customFieldProperties = new CustomField();
    }
    this.modelValue = this.value;
  }
  updateField() {
    const obj = {}
    if (!this.customFieldProperties._id) {
      obj['type'] = 'taskField';
      obj['field'] = FieldNameAlias[this.name];
    } else {
      obj['type'] = 'customField';
      obj['customFieldId'] = this.customFieldProperties._id;
    }
    obj['value'] = this.value;
    const objString = JSON.stringify(obj);
    // this.value = value;
    this.updatedField.emit(objString);
  }

}
