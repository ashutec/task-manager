import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InlineTextareaComponent } from './inline-textarea.component';

describe('InlineTextareaComponent', () => {
  let component: InlineTextareaComponent;
  let fixture: ComponentFixture<InlineTextareaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InlineTextareaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InlineTextareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
