import { Component, OnInit, Inject, OnDestroy, ElementRef, ViewChild, Renderer } from '@angular/core';
import { Task } from '../tasks/task';
import { MatDialog, MatDialogRef, MatDialogConfig, MAT_DIALOG_DATA } from '@angular/material';
import { MemberService } from '../invite-members/member.service';
import { Router } from '@angular/router';
import { TaskerService } from '../task-manager/tasker.service';
import { SpeechRecognitionService } from '../speech-recognizer/speech-recognition.service';
import { Subscription } from 'rxjs/Subscription';
import { Priority } from '../shared/Priority';

@Component({
  selector: 'app-add-new-task',
  templateUrl: './add-new-task.component.html',
  styleUrls: ['./add-new-task.component.css']
})
export class AddNewTaskComponent implements OnInit, OnDestroy {

  task: Task = new Task();
  name;
  Users;
  selectedUser = { firstname: '', lastname: '' }
  UserCopy;
  selected = true;
  dueDateLimit = new Date();
  subscription: Subscription;
  @ViewChild('autoCompleteInput') private autoCompleteInput: ElementRef;
  @ViewChild('taskTitle') private title: ElementRef;
  @ViewChild('description') private description: ElementRef;
  @ViewChild('textArea') private textArea: ElementRef;

  public priorities = Priority;

  constructor(
    private _dialogRef: MatDialogRef<AddNewTaskComponent>,
    private _memberService: MemberService,
    private _router: Router,
    private _taskerSerice: TaskerService,
    private _speechRecognitionService: SpeechRecognitionService,
    private renderor: Renderer,
    @Inject(MAT_DIALOG_DATA) private dialogData,
  ) { }

  ngOnInit() {
    this.subscription = this._taskerSerice.workspace.subscribe(workspace => {
      this.task.wsId = workspace._id;
      this.task.phase = this.dialogData.phase || workspace.phases[0];
      if (workspace.member) {
        this.Users = workspace.member.map(member => member.user);
        this.UserCopy = JSON.parse(JSON.stringify(this.Users));
      }
    });
    // this._speechRecognitionService.enterAssignee.subscribe((speechData) => {
    //   console.log('assignee: ' + speechData);
    //   this.autoCompleteInput.nativeElement.focus();
    // })
    this._speechRecognitionService.searchUser.subscribe((speechData) => {
      console.log('user: ' + speechData);
      this.name = speechData;
      this.filter();
      this.selectedUser = this.Users[0];
      this.task.assignee = this.Users[0];
      this.autoCompleteInput.nativeElement.blur();
    })

    this._speechRecognitionService.activateLoader.subscribe((labelName) => {
      if (labelName === 'title') {
        this.renderor.setElementClass(this.title.nativeElement, 'loading', true);
      } else if (labelName === 'description') {
        this.renderor.setElementClass(this.description.nativeElement, 'loading', true);
      }
    })
    // this.task.wsId = this.wsData._id;
  }

  saveTask() {
    this._taskerSerice.CreateTask(this.task).subscribe(task => {
      this._dialogRef.close({ task, isContinue: false });
    });
  }

  saveAndEditTask() {
    this._taskerSerice.CreateTask(this.task).subscribe(task => {
      this._dialogRef.close({ task, isContinue: true });
    });
  }

  selectUser(user) {
    this.task.assignee = user._id;
    this.selectedUser = user;
    this.selected = false;
  }

  filter() {
    this.Users = this.UserCopy.filter(user => new RegExp(`^${this.name}`, 'gi').test(user.firstname + ' ' + user.lastname));
  }

  deleteUser() {
    this.selected = true;
    this.selectedUser = null;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
