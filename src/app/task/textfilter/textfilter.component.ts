import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'textfilter',
  templateUrl: './textfilter.component.html',
  styleUrls: ['./textfilter.component.css']
})
export class TextfilterComponent implements OnInit, OnDestroy {

  @Input('name') name: any;
  @Input('customFieldProperties') customFieldProperties: any;
  @Input('clear') clear: EventEmitter<any>;
  @Output('updatedfield') updatedField = new EventEmitter<String>();
  textValue: string;
  subscription: Subscription;

  constructor() { }

  ngOnInit() {
    this.subscription = this.clear.subscribe(() => this.clearFilter());
  }

  saveFilter() {
    const search = {};
    search[this.customFieldProperties._id] = this.textValue;
    this.updatedField.emit(JSON.stringify(search));
  }

  clearFilter(ev?: any) {
    if (ev) {
      ev.stopPropagation();
    }
    this.textValue = null;
    this.saveFilter();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
