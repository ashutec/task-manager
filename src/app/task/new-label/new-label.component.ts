import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ToastrService } from '../shared/toaster.service';

@Component({
  selector: 'app-new-label',
  templateUrl: './new-label.component.html',
  styleUrls: ['./new-label.component.css']
})
export class NewLabelComponent implements OnInit {

  tag;
  colors = ['#e06925', '#1d3a5b', '#3a628f', '#943939', '#769146', '#378699'];
  customColor = '#2dcc70';

  constructor(
    @Inject(MAT_DIALOG_DATA) private tagData,
    private _dialogRef: MatDialogRef<NewLabelComponent>,
    private _toastrService: ToastrService,
  ) {
    this.tag = Object.assign({}, this.tagData);
  }

  ngOnInit() { }

  selectColor(color) {
    this.tag.color = color;
  }

  saveTag() {
    if (!this.tag.tagName)
      return this._toastrService.showError('Tag Name is required!');
    if (!this.tag.color)
      return this._toastrService.showError('Please select a color!');
    this.tag.color = this.tag.color;
    this._dialogRef.close(this.tag);
  }

  closeDialog() {
    this._dialogRef.close();
  }

}
