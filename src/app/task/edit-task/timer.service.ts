import { Injectable } from '@angular/core';
import { Http } from '@angular/http'
import { Observable } from 'rxjs/rx';
import { TaskerService } from '../task-manager/tasker.service';

@Injectable()
export class TimerService {

  constructor(private http: Http,
    private _taskerService: TaskerService) { }

  onStart(task) {
    return this.http.post('/task/onStart', { _id: task }).map(result => result.json()).map(task => {
      this._taskerService.timerUpdates(task);
    });
  }

  onStop(task) {
    return this.http.post('/task/onStop', { _id: task }).map(result => result.json()).map(task => {
      this._taskerService.timerUpdates(task);
    });
  }

  setTimeSheet(timeSheet) {
    this.http.post('/timeSheet/saveTimesheet', timeSheet).map(result => result.json()).subscribe(() => {
      console.log('subscribe');
    });
  }

  updateTimeSheet(stopTimer) {
    console.log(stopTimer);
    this.http.post('/timeSheet/updateTimeSheet', stopTimer).map(result => result.json()).subscribe(() => {
      console.log('subscribe update Time');
    });
  }

}
