import { Component, OnInit, Inject, Input, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { Task } from '../tasks/task';
import { Router, ActivatedRoute } from '@angular/router';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { TaskerService } from '../task-manager/tasker.service';
import { MemberService } from '../invite-members/member.service';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { TimerService } from './timer.service';
import { duration } from 'moment/moment';
import { TokenService } from '../../http-interceptor/token.service';
import { WorkspaceService } from '../../workspace/workspace/workspace.service';
import { Workspace } from '../../workspace/workspace/workspace';
import { Phase } from '../phase/phase';
import { AuthorizationService } from '../../shared/Authorization.service';
import { Privilages } from '../../shared/privilages.enum';
import { ToastrService } from '../shared/toaster.service';

@Component({
  selector: 'app-edit-task',
  templateUrl: './edit-task.component.html',
  styleUrls: ['./edit-task.component.css']
})

export class EditTaskComponent implements OnInit, OnDestroy {

  task: Task = new Task();
  time;
  timeToggle: boolean;
  subRef: Subscription;
  one: number;
  remainingTime;
  name;
  Users = [];
  selectedUser;
  selectedUserId;
  UserCopy;
  buttonText;
  isComment;
  activeTabId: Number;
  following = true;
  public privilages = Privilages;

  // Time Sheet Data
  public workSpaceId;
  public taskId;
  public assigneeId;
  public startTime;
  public stopTime;
  public isValid: boolean;
  @ViewChild('titleinput') titleRef: ElementRef;

  constructor( @Inject(MAT_DIALOG_DATA) private data,
    private _taskerService: TaskerService,
    private _dialogRef: MatDialogRef<EditTaskComponent>,
    private _memberService: MemberService,
    private _router: Router,
    private _timerService: TimerService,
    private _tokenService: TokenService,
    public _toastr: ToastrService,
    public _authorizationService: AuthorizationService,
    private _activatedRoute: ActivatedRoute,
  ) {

    this.task = this.data;

    console.log('Tasks :');
    console.log(this.task);
    this.workSpaceId = this.task.wsId._id;
    this.taskId = this.task._id;

    if (this.task.assignee !== undefined) {
      this.isValid = true;
    } else {
      this.isValid = false;
    }
    if (this.data && this.data.activeTabId) {
      this.activeTabId = this.data.activeTabId;
    }
    this.time = this.timeConvert(this.data.consumedTime);
    this.remainingTime = this.timeConvert(this.data.estimateTime - this.data.consumedTime)
    this.timeToggle = (this.data.timeStamp != null);
    this.buttonText = 'Start';
  }

  ngOnInit() {

    console.log(this.task);
    if (this.task.timeStamp != null) {
      this.timeToggle = true;
      this.buttonText = 'Stop';
      const time = ((+new Date()) - (+new Date(this.task.timeStamp)))
      this.time.consumedTime = this.task.consumedTime + time;
      const timer = Observable.interval(1000)
      this.subRef = timer.subscribe(t => {
        this.time = this.timeConvert(this.time.consumedTime + 1000);
        this.remainingTime = this.timeConvert(this.task.estimateTime - this.time.consumedTime);
      });
    }
    if (this.task.watchers.findIndex(watcher => watcher === this._tokenService.getLoggedInUser().id) !== -1) {
      this.following = false;
    }
  }

  timeConvert(consumedTime) {
    let second, minute, hour;
    hour = Math.floor(duration(consumedTime).asHours());
    minute = Math.floor(duration(consumedTime).asMinutes() % 60);
    second = Math.floor(duration(consumedTime).asSeconds() % 60);
    hour = (hour < 10 && hour >= 0) ? ('0' + hour) : hour;
    minute = (minute < 10 && minute >= 0) ? ('0' + minute) : minute;
    second = (second < 10 && second >= 0) ? ('0' + second) : second;
    return ({ hour, minute, second, consumedTime });
  }

  onToggle(hour, minute, second) {
    if (this._authorizationService.userCan(this.privilages.CAN_EDIT_TASK)) {
      this.timeToggle = !this.timeToggle;

      if (this.timeToggle) {
        console.log(this.task.title);
        this.startTime = hour + ':' + minute + ':' + second;
        const timeSheet = {
          wsId: this.workSpaceId,
          taskId: this.taskId,
          taskTitle : this.task.title,
          assigneeId: this.task.assignee._id,
          startTime: this.startTime,
          stopTime: null
        }
        this.onStart();
        this.buttonText = 'Stop';
        this._timerService.setTimeSheet(timeSheet);
      } else {
        this.stopTime = hour + ':' + minute + ':' + second;

        const stopTimer = {
          taskId: this.taskId,
          stopTime: this.stopTime
        }
        this._timerService.updateTimeSheet(stopTimer);
        this.onStop();
        this.buttonText = 'Start';

      }
    } else {
      this._toastr.showError(this._authorizationService.errorMessage);
    }
  }

  followTask() {
    this._taskerService.followTask(this.task._id, this.following).subscribe();
    this.following = !this.following;
  }

  selectUser(user) {
    this.selectedUserId = user._id;
    this.selectedUser = user.firstname + ' ' + user.lastname;
  }

  customUpdate(updatedField) {
    const update = {};
    const updateDoc: Object = {};
    update[updatedField] = this.task[updatedField];
    updateDoc['task'] = update;
    updateDoc['_id'] = this.task._id;
    this._taskerService.updateTask(updateDoc).subscribe();
  }

  onStart() {
    this._timerService.onStart(this.task._id).subscribe(result => {
      const timer = Observable.interval(1000)
      this.subRef = timer.subscribe(t => {
        this.time = this.timeConvert(this.time.consumedTime + 1000);
        this.remainingTime = this.timeConvert(this.task.estimateTime - this.time.consumedTime);
      });
    });

  }

  onStop() {
    this._timerService.onStop(this.task._id).subscribe(result => {
      this.subRef.unsubscribe();
    })
  }

  ngOnDestroy() {
    if (this.task.timeStamp != null) {
      this.subRef.unsubscribe();
    }
  }
}
