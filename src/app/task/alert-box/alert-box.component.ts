import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material'
@Component({
  selector: 'app-alert-box',
  templateUrl: './alert-box.component.html',
  styleUrls: ['./alert-box.component.css']
})
export class AlertBoxComponent implements OnInit {

  constructor(public dialogRefDelete: MatDialogRef<AlertBoxComponent>) { }

  ngOnInit() {
  }

}
