import { Component, OnInit, Input, Host, AfterViewInit } from '@angular/core';
import { Task } from './task';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { EditTaskComponent } from '../edit-task/edit-task.component';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';
import { TaskerService } from '../task-manager/tasker.service';
import { AlertBoxComponent } from '../alert-box/alert-box.component';
import { PhaseComponent } from '../phase/phase.component';
import { TimerService } from '../edit-task/timer.service';
import { ToastrService } from '../shared/toaster.service';
import { AuthorizationService } from '../../shared/Authorization.service';
import { Privilages } from '../../shared/privilages.enum';
import * as moment from 'moment';
import { Tabs } from './tabs.enum'
import { Phase } from '../phase/phase';
import { SpeechRecognitionService } from '../speech-recognizer/speech-recognition.service';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';
import { TokenService } from 'app/http-interceptor/token.service';
import { duration } from 'moment/moment';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit, AfterViewInit {

  @Input() task: Task
  @Input() phase: Phase
  dialogref: MatDialogRef<AlertBoxComponent>;
  @Host() parent: PhaseComponent;
  dialogrefTask: MatDialogRef<EditTaskComponent>;
  type;
  taskDue: number;
  isTimerOn = false;
  isDialogueOpened = false;
  tagLabels = [];
  public privilages = Privilages;


  timeToggle: boolean;
  subRef: Subscription;
  time;
  remainingTime;
  following;

  // Time Sheet Data
  public workSpaceId;
  public taskId;
  public assigneeId;
  public startTime;
  public stopTime;
  public isValid: boolean;
  constructor(public dialog: MatDialog,
    private _taskerService: TaskerService,
    private _timerService: TimerService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _toastrService: ToastrService,
    public _authorizationService: AuthorizationService,
    private _speechRecognitionService: SpeechRecognitionService,
    private _tokenService: TokenService
  ) {
  }

  ngAfterViewInit(): void { }

  ngOnInit() {

    if (this.task.assignee !== undefined) {
      this.isValid = true;
    } else {
      this.isValid = false;
    }

    this.time = this.timeConvert(this.task.consumedTime);
    this.remainingTime = this.timeConvert(this.task.estimateTime - this.task.consumedTime)
    if (this.task.timeStamp != null) {
      this.timeToggle = true;
      const time = ((+new Date()) - (+new Date(this.task.timeStamp)))
      this.time.consumedTime = this.task.consumedTime + time;
      const timer = Observable.interval(1000)
      this.subRef = timer.subscribe(t => {
        this.time = this.timeConvert(this.time.consumedTime + 1000);
        this.remainingTime = this.timeConvert(this.task.estimateTime - this.time.consumedTime);
      });
    }
    if (this.task.watchers.findIndex(watcher => watcher === this._tokenService.getLoggedInUser().id) !== -1) {
      this.following = false;
    }


    this._speechRecognitionService.openTask.subscribe((task) => {
      if (this.task._id === task._id) {
        this.openTask(task);
      }
      // this._speechRecognitionService.openTask.unsubscribe();
    });

    this.task.attachmentCount = this.task.attachments.filter(attachment => !attachment.isDeleted).length;
    if (this.task.dueDate && !this.phase.isEndOfProcess) {
      this.taskDue = moment(this.task.dueDate).diff(moment(new Date()), 'days');
    }
    if (this.task.timeStamp !== null) {
      this.isTimerOn = true;
    }
    if (this.task.wsId && this.task.wsId.tags && this.task.tags.length) {
      this.tagLabels = this.task.tags.map(tag => {
        const Tag = this.task.wsId.tags.find(wstag => wstag._id === tag);
        if (Tag) {
          return {
            tagName: Tag.tagName,
            color: Tag.color,
          };
        } else {
          return null;
        }
      }).filter(tag => (tag));
    }
  }


  timeConvert(consumedTime) {
    let second, minute, hour;
    hour = Math.floor(duration(consumedTime).asHours());
    minute = Math.floor(duration(consumedTime).asMinutes() % 60);
    second = Math.floor(duration(consumedTime).asSeconds() % 60);
    hour = (hour < 10 && hour >= 0) ? ('0' + hour) : hour;
    minute = (minute < 10 && minute >= 0) ? ('0' + minute) : minute;
    second = (second < 10 && second >= 0) ? ('0' + second) : second;
    return ({ hour, minute, second, consumedTime });
  }

  startStopTask(ev, hour, minute, second) {

    console.log(this.task);
    console.log(hour + ':' + minute + ':' + second);
    if (ev) {
      ev.stopPropagation();
    }
    if (this._authorizationService.userCan(this.privilages.CAN_EDIT_TASK)) {
      if (!this.isTimerOn) {
        this.startTime = hour + ':' + minute + ':' + second;
        const timeSheet = {
          wsId: this.task.wsId,
          taskId: this.task._id,
          taskTitle: this.task.title,
          assigneeId: this.task.assignee._id,
          startTime: this.startTime,
          stopTime: null
        }
        this._timerService.setTimeSheet(timeSheet);
        this._timerService.onStart(this.task._id).subscribe(() => this.isTimerOn = true
        );
      } else {
        this.stopTime = hour + ':' + minute + ':' + second;
        const stopTimer = {
          taskId: this.task._id,
          stopTime: this.stopTime
        }
        console.log(stopTimer);
        this._timerService.updateTimeSheet(stopTimer);
        this._timerService.onStop(this.task._id).subscribe(() => this.isTimerOn = false);
      }
    } else {
      this._toastrService.showError(this._authorizationService.errorMessage);
    }
  }
  // EditTask(task) {
  //   const config = new MatDialogConfig();
  //   config.data = Object.assign({}, task);
  //   config.width = '700px';
  //   config.disableClose = true;
  //   this.dialog.open(EditTaskComponent, config);
  // }
  openNewlyCreatedTaskByBot() {

  }

  deteletTask(taskId, ev) {
    if (ev) {
      ev.stopPropagation();
    }
    if (this._authorizationService.userCan(this.privilages.CAN_DELETE_TASK)) {
      this.dialogref = this.dialog.open(AlertBoxComponent, { width: '500px' });
      this.dialogref.afterClosed().subscribe(res => {
        if (res) {
          this._taskerService.deleteTask(taskId).subscribe();
        }
      })
    } else {
      this._toastrService.showError(this._authorizationService.errorMessage);
    }
  }

  openTask(task) {
    const wsId = this._activatedRoute.snapshot.params['Id'];
    this._router.navigate(['workspace/' + wsId + '/tasks/' + task._id]);
    this.openDialogueWithTab(task, 'edit');
  }

  openDialogueWithTab(task, tabName, ev?: any) {
    if (ev) {
      ev.stopPropagation();
    }
    const config = new MatDialogConfig();
    if (tabName === 'comment') {
      this.type = Tabs.Comment;
      // debugger;
    } else if (tabName === 'edit') {
      this.type = Tabs.Edit;
    } else if (tabName === 'attachment') {
      this.type = Tabs.Attachment;
    }
    config.data = Object.assign({}, task, { activeTabId: this.type });
    config.width = '700px';
    config.disableClose = true;
    this.dialogrefTask = this.dialog.open(EditTaskComponent, config);
    this.dialogrefTask.afterClosed().subscribe((result) => {
      if (result && result != false) {
        const wsId = this._activatedRoute.snapshot.params['Id'];
        this._router.navigate(['workspace/' + wsId]);
      }
    })
  }
}
