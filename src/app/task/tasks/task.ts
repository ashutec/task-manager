import { CreatedBy } from '../comments-tab/createdby';

export class Task {
    _id: string;
    title: string;
    description: string;
    dueDate?: Date;
    phase: string;
    timeStamp: Date;
    consumedTime: number;
    priority: number;
    estimateTime: number;
    attachments: [{
        _id: String,
        originalFileName: String,
        fileName: String,
        uploadBy: String,
        uploadOn: Date,
        deletedBy: String,
        deletedOn: Date,
        isDeleted: Boolean,
    }];
    assignee: CreatedBy;
    createdBy: { firstname: String, lastname: String, profile: String };
    createdOn: String;
    wsId: any;
    tags: [String];
    attachmentCount: number;
    commentCount: number;
    watchers: [string];
    customFields: [any];
}



