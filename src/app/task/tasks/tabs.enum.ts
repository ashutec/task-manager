// if there is any new tab is added it should be added here

export enum Tabs {
  Edit = 0,
  Comment = 1,
  Attachment = 2,
  ActivityLog = 3,
}
