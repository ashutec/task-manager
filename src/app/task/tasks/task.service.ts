import { Injectable } from '@angular/core';
import { Http, RequestOptions, RequestOptionsArgs } from '@angular/http';
import { Task } from '../tasks/task';

@Injectable()
export class TaskService {

  constructor(private _http: Http) { }

  createTask(task: Task) {
    // Task.phase = phase;
    return this._http.post('/task', task).map(result => result.json());
  }

  getSingleTask(taskId) {
    return this._http.get('/task/' + taskId).map(result => result.json());
  }

  updateTask(task: Task) {
    return this._http.put('/task', task).map(result => result.json());
  }

  deleteTask(taskId) {
    return this._http.delete('/task/' + taskId).map(result => result.json())
  }

  moveTask(task) {
    return this._http.post('/task/movephase', task).map(result => result.json())
  }

  moveTaskByWS(task) {
    return this._http.put('/task/moveTaskByWS', task).map(result => result.json());
  }

  updateCustomField(customField) {
    return this._http.put('/task/updateCustomField', customField).map(result => result.json());
  }

  deleteCustomField(customField) {
    const body: RequestOptionsArgs = {
      body: customField
    }
    return this._http.delete('/task/deletecustomfield', body).map(result => result.json());
  }

  // deletePhase(wsId, phaseId) {
  //   let phase = {
  //     wsId: wsId,
  //     phasename: phaseId
  //   }
  //   return this._http.delete('/phase', { body: phase }).map(res => res.json())
  // }

  // getPhases(wsId) {
  //   let myParams = new URLSearchParams();
  //   myParams.set('wsId', wsId);
  //   let options = new RequestOptions({ params: myParams });
  //   return this._http.get('/phase/' + wsId).map(res => res.json());
  // }

}
