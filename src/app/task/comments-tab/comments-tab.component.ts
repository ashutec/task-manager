import { Component, OnInit, Input, AfterViewChecked, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { CommentService } from './comment.service';
import { MatAutocompleteTrigger, MatAutocomplete } from '@angular/material'
import { Task } from '../tasks/task';
import { Comment } from './comment';
import { TokenService } from '../../http-interceptor/token.service';
import { TabService } from '../shared/tabs.service';
import { EditTabComponent } from '../edit-tab/edit-tab.component';
import { User } from '../../authentication/user';
import { Subscription } from 'rxjs/Subscription';
import { MemberService } from '../invite-members/member.service';
import { TaskerService } from '../task-manager/tasker.service';
import * as cloneDeep from 'lodash/cloneDeep';


@Component({
  selector: 'app-comments-tab',
  templateUrl: './comments-tab.component.html',
  styleUrls: ['./comments-tab.component.css'],
})
export class CommentsTabComponent implements OnInit, OnDestroy, AfterViewChecked {
  @Input() task: Task;
  @Input() TabElement: any;
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;
  @ViewChild('message') private description: ElementRef;
  @ViewChild('focusRef') private filterUser: ElementRef;
  @ViewChild('highlightContainer') private highlightContainer: ElementRef;
  @ViewChild('auto') private autocomplete: MatAutocomplete;

  comment: Comment = new Comment();
  comentList: Comment[] = new Array<Comment>();
  userId;
  currentProfile;
  disableScrollDown = false;
  tagMember = false;
  users = new Array<User>();
  isRefreshing: Boolean = false;
  subscription: Subscription;
  isedit = false;
  editCommentId;
  commentObj: Comment = new Comment();
  key1;
  name = '';
  Users;
  selectedUser = { firstname: '', lastname: '' }
  UserCopy;
  selected = true;
  autocompleteSearch;
  loading = false;
  userSelected = false;
  highlighter;
  flag = 0;
  commentDescription = new Array<string>();
  descriptionArray = new Array<string>();
  tagUser = { userName: String };
  textWithIndex = new Map<string, { strtInd: number, endInd: number }>();
  strtIndex = 0;
  endIndex = 0;
  isEditStarted;
  arrayindexToEdit;

  constructor(private commentService: CommentService,
    private _tabService: TabService,
    private _tokenService: TokenService,
    private _memberService: MemberService,
    private _taskerService: TaskerService) {
  }

  ngOnInit() {
    // this.subscription = this._tabService.Tab.filter(tab => tab === this.TabElement.textLabel).subscribe(() => {
      if (this.comentList.length === 0) {
        this.loading = true;
      }
      const user = this._tokenService.getLoggedInUser();
      this.userId = user.id;
      this.currentProfile = user.profile;
      this.isRefreshing = true;
      this.commentService.getCommentsByTask(this.task._id).subscribe((result) => {
        this.comentList = result;
        this.disableScrollDown = false;
        this.scrollToBottom();
        this.isRefreshing = false;
        this.loading = false;
      });
    // });
    this.getWsUser();
  }

  ngAfterViewChecked(): void {
    this.scrollToBottom();
  }

  postComment() {
    if (Object.keys(this.comment).length !== 0) {
      // console.log(this.highlightContainer.nativeElement.innerHTML);
      this.comment.taskId = this.task._id;
      // this.comment.description = this.highlightContainer.nativeElement.innerHTML
      this.commentService.postComment(this.comment).subscribe((result) => {
        this.disableScrollDown = false;
        this.comment = new Comment();
        this.task.commentCount = this.task.commentCount ? this.task.commentCount + 1 : 1;
        this._taskerService.taskUpdated.next(this.task); // updated subject for comment count.
      });
    }
  }

  editComment(comment) {
    const clone = cloneDeep(comment);
    const element = this.description.nativeElement;
    element.click();
    this.comment = clone;
    this.isedit = true;
  }

  updateComment() {
    if (Object.keys(this.comment).length !== 0) {
      this.commentService.updateComment(this.comment).subscribe((result) => {
        this.comentList.map((comment) => {
          if (comment._id === result._id) {
            comment.description = result.description;
            comment.isUpdated = result.isUpdated;
            comment.updatedOn = result.updatedOn;
          }
        });
        this.comment = new Comment();
        this.isedit = false;
      });
    }
  }

  onScroll() {
    const element = this.myScrollContainer.nativeElement;
    const atBottom = element.scrollHeight - element.scrollTop === element.clientHeight;
    if (this.disableScrollDown && atBottom) {
      this.disableScrollDown = false;
    } else {
      this.disableScrollDown = true;
    }
  }

  scrollToBottom(): void {
    if (this.disableScrollDown) {
      return
    }
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch (err) { }
  }

  ngOnDestroy() {
    if (!!this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  onEnterCtrl(event) {
    if (event.ctrlKey && event.keyCode === 10 && Object.keys(this.comment).length !== 0) {
      if (!this.isedit) {
        this.postComment();
        this.highlightContainer.nativeElement.innerHTML = '';
        this.isEditStarted = false;
        this.textWithIndex.clear();
        this.commentDescription.length = 0;
        this.descriptionArray.length = 0;
        this.tagMember = false;
      } else {
        this.updateComment();
        this.highlightContainer.nativeElement.innerHTML = '';
        this.isEditStarted = false;
        this.textWithIndex.clear();
        this.commentDescription.length = 0;
        this.descriptionArray.length = 0;
        this.tagMember = false;
      }
    }
  }

  cancelcomment() {
    const element = this.myScrollContainer.nativeElement;
    if (Object.keys(this.comment).length !== 0) {
      this.comment = new Comment();
      this.highlightContainer.nativeElement.innerHTML = '';
      this.isEditStarted = false;
      this.textWithIndex.clear();
      this.commentDescription.length = 0;
      this.descriptionArray.length = 0;
      if (!this.isedit) {
        this.isedit = true;
      } else {
        this.isedit = false;
      }
    }
  }

  trceCommentDescription(event) {
    // console.log('old map: ');
    // console.log(this.textWithIndex);
    if (event === '') {
      this.highlightContainer.nativeElement.innerHTML = '';
      this.textWithIndex.clear();
      this.isEditStarted = false;
      this.tagMember = false;
    }
    this.comment.description = event;
    if (this.isEditStarted) {
      let firstNode;
      let secondNode;
      if (this.arrayindexToEdit === 0) {
        firstNode = '';
        secondNode = this.highlightContainer.nativeElement.childNodes[this.arrayindexToEdit + 1].innerText;
        const mapIterator = this.textWithIndex.keys();
        for (let i = 0; i < this.textWithIndex.size; i++) {
          const iterateObj = mapIterator.next();
          if (iterateObj.value === this.descriptionArray[this.arrayindexToEdit]) {
            this.commentDescription.pop();
            this.commentDescription.push(this.comment.description);
            const obj = this.textWithIndex.get(iterateObj.value);
            if (obj.endInd <= obj.strtInd + this.comment.description.split(secondNode)[0].length - 1) {
              this.updateMap(1);
            } else {
              this.updateMap(-1);
            }
            obj.endInd = obj.strtInd + this.comment.description.split(secondNode)[0].length;
            this.endIndex = obj.endInd;
            this.strtIndex = obj.endInd + secondNode.length;
            iterateObj.value = this.comment.description.split(secondNode)[0];
            this.textWithIndex.set(this.comment.description.split(secondNode)[0], obj);
            this.textWithIndex.delete(this.descriptionArray[this.arrayindexToEdit]);
            this.descriptionArray[this.arrayindexToEdit] = this.comment.description.split(secondNode)[0];
          }
        }
        this.highlightContainer.nativeElement.childNodes[this.arrayindexToEdit].textContent = this.comment.description
          .split(secondNode)[0];

      } else if (!this.highlightContainer.nativeElement.childNodes[this.arrayindexToEdit - 1]) {
        secondNode = '';
        firstNode = this.highlightContainer.nativeElement.childNodes[this.arrayindexToEdit].innerText;
        const mapIterator = this.textWithIndex.keys();
        this.highlightContainer.nativeElement.childNodes[this.arrayindexToEdit + 1].textContent = this.comment.description
          .split(firstNode)[0];
      } else {
        firstNode = this.highlightContainer.nativeElement.childNodes[this.arrayindexToEdit + (this.arrayindexToEdit - 1)].innerText;
        secondNode = this.highlightContainer.nativeElement.childNodes[this.arrayindexToEdit + (this.arrayindexToEdit + 1)].innerText;
        const mapIterator = this.textWithIndex.keys();
        for (let i = 0; i < this.textWithIndex.size; i++) {
          const iterateObj = mapIterator.next();
          if (iterateObj.value === this.descriptionArray[this.arrayindexToEdit]) {
            this.commentDescription.pop();
            this.commentDescription.push(this.comment.description);
            const obj = this.textWithIndex.get(iterateObj.value);
            if (obj.endInd <= obj.strtInd + this.comment.description.match(new RegExp(firstNode + '(.*)' + secondNode))[1].length) {
              this.updateMap(1)
            } else {
              this.updateMap(-1);
            }
            obj.endInd = obj.strtInd + this.comment.description.match(new RegExp(firstNode + '(.*)' + secondNode))[1].length;
            this.endIndex = obj.endInd;
            this.strtIndex = obj.endInd + secondNode.length;
            iterateObj.value = this.comment.description.match(new RegExp(firstNode + '(.*)' + secondNode))[1];
            this.textWithIndex.set(this.comment.description.match(new RegExp(firstNode + '(.*)' + secondNode))[1], obj);
            this.textWithIndex.delete(this.descriptionArray[this.arrayindexToEdit]);
            this.descriptionArray[this.arrayindexToEdit] = this.comment.description.match(new RegExp(firstNode + '(.*)' + secondNode))[1];
          }
        }
        this.highlightContainer.nativeElement.childNodes[this.arrayindexToEdit * 2].textContent = this.comment.description
          .match(new RegExp(firstNode + '(.*)' + secondNode))[1];
      }
    }
    if (this.comment.description.substr(this.comment.description.length - 1) === '@' && this.comment.description !== '@') {
      this.tagMember = true;
      const descriptionClone = cloneDeep(this.comment.description);
      this.autocompleteSearch = descriptionClone;
      setTimeout(() => {
        this.filterUser.nativeElement.focus();
      });

    } else {
      this.tagMember = false;
      this.name = '';
    }
  }

  getWsUser() {
    this._taskerService.workspace.subscribe(result => {
      this.Users = result.member.map(member => member.user);
      this.UserCopy = JSON.parse(JSON.stringify(this.Users));
    });
  }

  clear(event) {
    let cursorPosition;
    if (event.type === 'click') {
      cursorPosition = event.srcElement.selectionStart;
    }
    cursorPosition = this.description.nativeElement.selectionStart;
    const mapIterator = this.textWithIndex.keys();
    for (let i = 0; i < this.textWithIndex.size; i++) {
      const key = mapIterator.next();
      this.descriptionArray.indexOf(key.value);
      const keyarray = this.textWithIndex.get(key.value);
      if (event.keyCode === 39) {
        if (cursorPosition >= keyarray.strtInd && cursorPosition < keyarray.endInd) {
          this.isEditStarted = true;
          this.arrayindexToEdit = this.descriptionArray.indexOf(key.value);
          // console.log('FORWARD: cursor in section of: ' + key.value + 'cursor at: ' + cursorPosition);
        }
      } else if (event.keyCode === 37) {
        if (cursorPosition > keyarray.strtInd && cursorPosition <= keyarray.endInd) {
          this.isEditStarted = true;
          this.arrayindexToEdit = this.descriptionArray.indexOf(key.value);
          // console.log('Backward: cursor in section of: ' + key.value + 'cursor at: ' + cursorPosition);
        }
      } else if (event.type === 'click') {
        if (cursorPosition >= keyarray.strtInd && cursorPosition < keyarray.endInd) {
          this.isEditStarted = true;
          this.arrayindexToEdit = this.descriptionArray.indexOf(key.value);
          // console.log('click: cursor in secrtion of : ' + key.value + 'cursor at: ' + cursorPosition);
        }
      } else {
        if (cursorPosition > this.endIndex) {
          this.isEditStarted = false;
        }
      }
    }
    if (event.keyCode === 8) {
      // console.log(this.highlightContainer.nativeElement);
      let highligtArray = [];
      highligtArray = this.highlightContainer.nativeElement.querySelectorAll('.highlight');
      const n = this.comment.description.slice(0, this.description.nativeElement.selectionStart).split(' ');
      const removeUser = n[n.length - 2] + ' ' + n[n.length - 1];
      for (let i = 0; i < highligtArray.length; i++) {
        if (highligtArray[i].innerHTML === removeUser) {
          if (highligtArray.length === 1) {
            this.highlightContainer.nativeElement.innerHTML = '';
          } else {
            highligtArray[i].remove();
          }
          this.comment.description = this.comment.description.replace(removeUser, '');
        }
      }
    }
  }


  filter() {
    this.Users = this.UserCopy.filter(user => new RegExp(`^${this.name}`, 'gi').test(user.firstname));
  }

  selectUser(user) {
    this.userSelected = true;
    this.task.assignee = user._id;
    this.selectedUser = user;
    const newString = this.comment.description.replace(this.commentDescription.pop(), '');
    // console.log('new string:' + newString);
    this.endIndex = this.strtIndex + newString.length - 1;
    const range = this.strtIndex + '-' + this.endIndex;
    this.textWithIndex.set(newString.slice(0, newString.lastIndexOf('@')), { strtInd: this.strtIndex, endInd: this.endIndex });
    this.comment.description = this.comment.description.slice(0, this.comment.description.lastIndexOf('@'));
    const userName: String = this.selectedUser.firstname + ' ' + this.selectedUser.lastname;
    this.descriptionArray.push(newString.slice(0, this.comment.description.lastIndexOf('@')));
    this.highlightContainer.nativeElement.innerHTML = this.highlightContainer.nativeElement.innerHTML +
      newString.slice(0, this.comment.description.lastIndexOf('@')) + '<span class="highlight">' + userName.toUpperCase() + '</span>';
    this.comment.description = this.comment.description + userName.toUpperCase();
    this.selected = false;
    this.tagMember = false;
    this.commentDescription.push(this.comment.description);
    this.name = '';
    this.strtIndex = this.endIndex + userName.length;
    // console.log('array: ' + this.descriptionArray);
  }

  searchUser(event) {
    if (event) {
      this.name = event;
      if (event.lastIndexOf('@') !== -1) {
        this.name = event.substr(event.lastIndexOf('@') + 1, event.length);
      } else {
        this.name = '';
        this.tagMember = false;
        this.comment.description = event;
      }
      this.filter();
    }
  }

  updateMap(flag) {
    // const mapIterator = this.textWithIndex.keys();
    if (flag === 1) {
      for (let i = this.arrayindexToEdit + 1; i < this.descriptionArray.length; i++) {
        // const iterateObj = mapIterator.next();
        // if (iterateObj.value !== this.descriptionArray[i]) {
        const obj = this.textWithIndex.get(this.descriptionArray[i]);
        obj.strtInd = obj.strtInd + 1;
        obj.endInd = obj.endInd + 1;
        this.textWithIndex.set(this.descriptionArray[i], obj);
        // }
      }
    } else {
      for (let i = this.arrayindexToEdit + 1; i < this.descriptionArray.length; i++) {
        // const iterateObj = mapIterator.next();
        // if (iterateObj.value !== this.descriptionArray[i]) {
        const obj = this.textWithIndex.get(this.descriptionArray[i]);
        obj.strtInd = obj.strtInd - 1;
        obj.endInd = obj.endInd - 1;
        this.textWithIndex.set(this.descriptionArray[i], obj);
        // }
      }
    }
  }
}
