import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Comment } from './comment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class CommentService {

  constructor(private http: Http) { }

  getCommentsByTask(taskId): Observable<Comment[]> {
    return this.http.get('/comment/tasks/' + taskId).map((response: Response) => {
      const commentList = response.json() && response.json().data;
      return commentList;
    });
  }

  postComment(commentObj): Observable<Comment> {
    return this.http.post('/comment', commentObj).map((response: Response) => {
      const comment = response.json() && response.json().data;
      return comment;
    });
  }

  updateComment(commentObj): Observable<Comment> {
    return this.http.put(`/comment/${commentObj._id}`, commentObj).map((response: Response) => {
      const comment = response.json() && response.json().data;
      return comment;
    });
  }

}
