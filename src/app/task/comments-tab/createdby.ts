export class CreatedBy {
  _id: string;
  profile: string;
  firstname: string;
  lastname: string;
}
