import { CreatedBy } from './createdby';

export class Comment {
  public _id: string;
  public taskId: string;
  public description: string;
  public createdOn: string;
  public updatedOn: string;
  public createdBy: CreatedBy;
  public isUpdated: boolean;
}
