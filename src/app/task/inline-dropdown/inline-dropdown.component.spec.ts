import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InlineDropdownComponent } from './inline-dropdown.component';

describe('InlineDropdownComponent', () => {
  let component: InlineDropdownComponent;
  let fixture: ComponentFixture<InlineDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InlineDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InlineDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
