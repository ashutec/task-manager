import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CummulativeChartComponent } from './cummulative-chart.component';

describe('CummulativeChartComponent', () => {
  let component: CummulativeChartComponent;
  let fixture: ComponentFixture<CummulativeChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CummulativeChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CummulativeChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
