import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { PhaseService } from '../phase/phase.service';
import { Task } from 'app/task/tasks/task';
import { TaskerService } from '../task-manager/tasker.service'
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { CummulativeChartService } from './cummulative-chart.service';
import * as moment from 'moment';
import _ from 'lodash';
declare let d3: any;

@Component({
  selector: 'app-cummulative-chart',
  templateUrl: './cummulative-chart.component.html',
  styleUrls: ['./cummulative-chart.component.css'],
})
export class CummulativeChartComponent implements OnInit {

  @Input() phase: any;
  tasks: Task[];
  public newData = new Array();
  public chartData = new Array();
  public stackedChartData = new Array();
  public values = new Array();
  public pieChartValues = new Array();
  public loading: Boolean;
  public tableView: Boolean;
  public pieChartView: Boolean;
  allTableData;
  options;
  stackedOptions;
  data;
  stackedData;
  public lifeTimestackedChartData = new Array();
  lifeTimeStackedOptions;
  lifeTimeStackedData;
  wsId;
  userId: number;
  todayMilliSeconds;
  yeasterDayMilliSeconds;
  public todayView: Boolean;
  public lifeTimeView: Boolean
  public tempData = new Array();
  public newTempArray = new Array();
  public newUpdatedDate = new Array();
  public missingValue: any;
  public flag = true;
  public selectedIndex;
  constructor(private _phaseService: PhaseService,
    private _taskerService: TaskerService, route: ActivatedRoute, private _cummulativeChartService: CummulativeChartService) {
    this.tasks = new Array<Task>();

    route.params.subscribe(wsId => this.wsId = wsId);
    this.tableView = true;
    this.pieChartView = false;
    this.todayView = false;
    this.lifeTimeView = false;
  }

  ngOnInit() {

    //  this.pieChart();
    this.loading = true;
    this._cummulativeChartService.getTaskLength(this.wsId.wsId).subscribe(res => {
      this.allTableData = res;
    });

    this.stackedgraph();

    this.lifeTimeStackedGraph();

    // Stacked Graph
  }

  public tableData() {
    this.chartData = undefined;
    this.tableView = true;
    this.pieChartView = false;
    this._cummulativeChartService.getTaskLength(this.wsId.wsId).subscribe(res => {
      this.allTableData = res;
    });
  }

  public pieChart() {

    this.tableView = false;
    this.pieChartView = true;
    console.log('Temp Data');
    this._cummulativeChartService.getTaskLength(this.wsId.wsId).subscribe(res => {
      this.loading = false;
      this.pieChartValues = res;

      console.log(this.pieChartValues);

      // for (let i = 0; i < res.length; i++) {
      //   this.phaseName.push(res[i].key);
      //   this.taskLength.push(res[i].value);
      // }

      res.map(r => {
        this.chartData.push({
          key: r.key,
          y: r.value,
        })
      })


      //    console.log(this.chartData);
      this.options = {
        chart: {
          type: 'pieChart',
          height: 500,
          x: function (d) { return d.key; },
          y: function (d) { return d.y; },
          showLabels: true,
          duration: 500,
          labelThreshold: 0.01,
          labelSunbeamLayout: true,
          legend: {
            margin: {
              top: 5,
              right: 35,
              bottom: 5,
              left: 0
            }
          }
        }
      }

      this.data = this.chartData;

    });
  }


  selectedIndexChange(val) {

    if (val === 0) {
      this.todayView = true;
      this.lifeTimeView = false;
    }
    if (val === 6) {
      this.todayView = false
      this.lifeTimeView = true;
    }
  }

  public stackedgraph() {
    this.todayView = true;
    this._cummulativeChartService.getTaskLength(this.wsId.wsId).subscribe(res => {
      this.loading = false;
      this.values = res;

      const date1 = new Date();

      const ydate = '' + date1.getFullYear() + '-' + (date1.getMonth() + 1) + '-' + (date1.getDate() - 1);
      const tdate = '' + date1.getFullYear() + '-' + (date1.getMonth() + 1) + '-' + date1.getDate();


      this.todayMilliSeconds = new Date(tdate).getTime();
      this.yeasterDayMilliSeconds = new Date(ydate).getTime();



      res.map(r => {
        this.stackedChartData.push({
          key: r.key,
          values: [[this.yeasterDayMilliSeconds, r.value], [this.todayMilliSeconds, r.value]],
        })
      })

      console.log('Stack');
      console.log(this.stackedChartData);

      this.stackedOptions = {
        chart: {
          type: 'stackedAreaChart',
          height: 450,
          margin: {
            top: 20,
            right: 20,
            bottom: 30,
            left: 40
          },
          x: function (d) { return d[0]; },
          y: function (d) { return d[1]; },
          useVoronoi: false,
          clipEdge: true,
          duration: 100,
          useInteractiveGuideline: true,
          xAxis: {
            showMaxMin: false,
            tickFormat: function (d) {
              return d3.time.format('%x')(new Date(d))
            }
          },
          yAxis: {
            tickFormat: function (d) {
              return d3.format(',.1f')(d);
            }
          },
          zoom: {
            enabled: true,
            scaleExtent: [1, 10],
            useFixedDomain: false,
            useNiceScale: false,
            horizontalOff: false,
            verticalOff: true,
            unzoomEventType: 'dblclick.zoom'
          },
        },
        title: {
          enable: true,
          text: 'hello',
        },
      };
      // [[1025409600000, 23.041422681023]]
      this.stackedData = this.stackedChartData;
    });
  }

  public lifeTimeStackedGraph() {

    this._cummulativeChartService.getAllTaskLength(this.wsId.wsId).subscribe(res => {
      this.values = res;
      this.loading = false;
      console.log(this.values[0].allDate);
      // console.log(this.values);
      // new Date(task[0].updatedDate.toString()).getTime()
      res.map(resValue => {
        this.tempData = [];
        const allDates = this.values[0].allDate;
        const taskDates = resValue.values.map(task => task[0].updatedDate);

        this.flag = true;
        resValue.values.map(task => {
          const d = new Date(task[0].updatedDate.toString()).getTime();
          const len = task.length;

          // this.tempData.sort(function (a, b) {
          //   return a[0] - b[0];
          // });
          this.tempData.push([d, len]);

          this.tempData.sort(function (a, b) {
            return a[0] - b[0];
          })
          allDates.forEach(element => {
            if (this.flag) {
              if (!taskDates.includes(element)) {
                // console.log('Elemement :');
                // console.log(element);
                //   this.missingValue = element;
                // console.log(this.tempData.map(data => data[0]).includes(element))
                // tslint:disable-next-line:curly
                if (!this.tempData.map(data => data[0]).includes(new Date(element.toString()).getTime()))
                  this.tempData.push([new Date(element.toString()).getTime(), len]);
                this.tempData.sort(function (a, b) {
                  return a[0] - b[0];
                })
                // console.log(this.tempData);
                // this.flag = false;
              }

            }

          });
        });

        this.lifeTimestackedChartData.push({
          key: resValue.key,
          values: this.tempData
        })
      });
      console.log(this.lifeTimestackedChartData);



      this.lifeTimeStackedOptions = {
        chart: {
          type: 'stackedAreaChart',
          height: 450,
          margin: {
            top: 20,
            right: 20,
            bottom: 30,
            left: 40
          },
          x: function (d) { return d[0]; },
          y: function (d) { return d[1]; },
          useVoronoi: false,
          clipEdge: true,
          duration: 100,
          useInteractiveGuideline: true,
          xAxis: {
            showMaxMin: false,
            tickFormat: function (d) {
              return d3.time.format('%x')(new Date(d))
            }
          },
          yAxis: {
            tickFormat: function (d) {
              return d3.format(',.2f')(d);
            }
          },
          zoom: {
            enabled: true,
            scaleExtent: [1, 10],
            useFixedDomain: false,
            useNiceScale: false,
            horizontalOff: false,
            verticalOff: true,
            unzoomEventType: 'dblclick.zoom'
          }
        },
        title: {
          enable: true,
          text: '123',
        },
      }

      // [[1025409600000, 23.041422681023]]
      this.lifeTimeStackedData = this.lifeTimestackedChartData;


    });
  }
}
