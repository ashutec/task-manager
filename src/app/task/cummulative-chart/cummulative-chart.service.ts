import { Injectable } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map'
import { environment } from 'environments/environment';
import { Subject } from 'rxjs/Subject';
// import { TaskerService } from '../task-manager/tasker.service';

@Injectable()
export class CummulativeChartService {

  taskLength: Subject<any> = new Subject<any>();
  constructor(private _http: Http) { }


  getTaskLength(wsId) {
    return this._http.get('/chart/cummulativeChart?id=' + wsId).map(res => res.json());
  }

  getAllTaskLength(wsId) {
   // debugger;
    return this._http.get('/chart/allTaskLength?id=' + wsId).map(res => res.json());
  }


}
