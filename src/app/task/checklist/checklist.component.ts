import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FieldNameAlias } from '../shared/fieldNameAlias';
import { CustomField } from '../../workspace/custom-field-settings/CustomField';

@Component({
  selector: 'app-checklist',
  templateUrl: './checklist.component.html',
  styleUrls: ['./checklist.component.css']
})
export class ChecklistComponent implements OnInit {
  

  @Input('name') name: any;
  @Input('value') value: Array<any> = new Array();
  @Input('customFieldProperties') customFieldProperties: CustomField;
  @Output() updatedField = new EventEmitter<String>();
  inputText: boolean;
  list = [];
  count = 0;

  constructor() {
  }

  ngOnInit() {
    if (!this.value) {
      this.value = [];
    }
    this.count = this.value.length;
    console.log(this.customFieldProperties);
    if (!this.customFieldProperties) {
      this.customFieldProperties = new CustomField();
    }
    this.list = this.customFieldProperties.options.map((option) => {
      const checked = this.value ? this.value.includes(option.id) || false : false;
      return {
        id: option.id,
        value: option.value,
        checked
      }
    });
  }

  updateField() {
    this.value = this.list.filter(option => option.checked).map(option => option.id);
    this.count = this.value.length;
    const obj = {};
    if (!this.customFieldProperties._id) {
      obj['type'] = 'taskField';
      obj['field'] = FieldNameAlias[this.name];
    } else {
      obj['type'] = 'customField';
      obj['customFieldId'] = this.customFieldProperties._id;
    }
    obj['value'] = this.value;
    const objString = JSON.stringify(obj);
    // this.value = value;
    this.updatedField.emit(objString);
  }


}
