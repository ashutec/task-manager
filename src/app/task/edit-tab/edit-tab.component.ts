import { Component, OnInit, Inject, Pipe, PipeTransform, ViewContainerRef, OnDestroy, ElementRef, ViewChild } from '@angular/core';
import { Task } from '../tasks/task';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { TaskerService } from '../task-manager/tasker.service';
import { TaskService } from '../tasks/task.service';
import { MemberService } from '../invite-members/member.service';
import { Router } from '@angular/router';
import { duration } from 'moment/moment';
import { Priority } from '../shared/Priority';
import { ToastrService } from '../shared/toaster.service';
import { WorkspaceService } from '../../workspace/workspace/workspace.service';
import { Workspace } from '../../workspace/workspace/workspace';
import { EditTaskComponent } from '../edit-task/edit-task.component';
import { Phase } from '../phase/phase';
import { SpeechRecognitionService } from '../speech-recognizer/speech-recognition.service';
import { CustomFieldService } from '../../workspace/custom-field-settings/customfield.service';
import { AuthorizationService } from '../../shared/Authorization.service';
import { Privilages } from '../../shared/privilages.enum';
// import _ from 'lodash';
import * as  cloneDeep from 'lodash/cloneDeep';
import * as pick from 'lodash/pick';
import { Subscription } from 'rxjs/subscription';
import { CustomFieldTypes } from '../../workspace/custom-field-settings/customFieldTypes';

@Component({
  selector: 'app-edit-tab',
  templateUrl: './edit-tab.component.html',
  styleUrls: ['./edit-tab.component.css']
})

export class EditTabComponent implements OnInit, OnDestroy {

  // task: Task = new Task();
  wsName: Workspace = new Workspace();
  name: string;
  Users = [];
  selectedUser;
  selectedUserId;
  selectedUserProfile;
  UserCopy;
  hour;
  minute;
  titleEdit;
  descEdit;
  priorityEdit;
  dueDtEdit;
  estimateEdit;
  asigneeEdit;
  workspaceSubject;
  workspaces;
  workspacesCopy;
  wsData: Workspace = new Workspace();
  phases;
  phase: Phase = new Phase();
  phasesCopy;
  ws: string;
  searchPhase: string;
  customFields = [];
  allCustomFields = [];
  public Priority = Priority;
  public privilages = Privilages;
  date;
  subscription: Subscription;
  public customFieldTypes = CustomFieldTypes;

  @ViewChild('userFilterElement') userFilterElement: ElementRef;

  constructor( @Inject(MAT_DIALOG_DATA) public task: Task,
    private _taskerService: TaskerService,
    private _dialogRef: MatDialogRef<EditTabComponent>,
    private _memberService: MemberService,
    private _router: Router,
    public _toastr: ToastrService,
    private _workspaceService: WorkspaceService,
    private dialogrefTask: MatDialogRef<EditTaskComponent>,
    private _speechRecognitionService: SpeechRecognitionService,
    public _authorizationService: AuthorizationService,
    private _customFieldService: CustomFieldService,
    private _taskService: TaskService,
  ) {
  }

  ngOnInit() {
    if (this.task.assignee != null) {
      this.selectedUserId = this.task.assignee._id;
    }

    this.workspaceSubject = this._taskerService.workspace.subscribe((workspace) => {
      this.getWsUser(workspace);
      this.hour = Math.floor(duration(this.task.estimateTime).asHours())
      this.minute = Math.floor(duration(this.task.estimateTime).asMinutes() % 60);
      this.workspaces = workspace;
      this.workspacesCopy = cloneDeep(this.workspaces);
      if (this.task.dueDate) {
        this.date = this.task.dueDate.toString().split('T')[0].split('-');
        this.date = new Date(Number.parseInt(this.date[0]), Number.parseInt(this.date[1]) - 1, Number.parseInt(this.date[2]) + 1);
      }
      this.getCustomFields();
    });
  }

  getCustomFields() {
    // this._customFieldService.getFields(this.workspaces._id)
    this._taskerService.getCustomFieldsSubject().subscribe(customFields => {
      this.allCustomFields = customFields
      this.allCustomFields = this.allCustomFields.filter(task => !task.isRequiredInTask).map((field) => {
        field.status = this.task.customFields.map(customfield => customfield.customFieldId._id).includes(field._id)
        return field;
      });
      this.customFields = this.task.customFields.map(customfield => {
        let customField = {};
        customField = customfield.customFieldId; // populated object hence the name is customFieldId. It has the whole custom Field object.
        (<any>customField).value = customfield.value;
        return customField;
      });
      // Filtering all required fields and the ones that are not already configured in task.
      this.customFields.push(...customFields.filter(field => field.isRequiredInTask
        && !this.customFields.map(taskfield => taskfield._id).includes(field._id))
      );
    });
  }

  updateTask() {
    const newTask = new Task()
    newTask.title = this.task.title;
    newTask.description = this.task.description;
    newTask.dueDate = this.task.dueDate;
    newTask._id = this.task._id;
    newTask.assignee = this.selectedUserId;
    this._taskerService.updateTask(newTask).subscribe(res => this._dialogRef.close(res));
  }

  updateEstimateTime() {
    const estimateTime = (this.hour * 60 * 60 * 1000) + (this.minute * 60 * 1000);
    this.task.estimateTime = estimateTime;
    this.customUpdate('estimateTime', 'Estimate Time');
  }

  customUpdate(updatedField, fieldName?: string) {
    if (this._authorizationService.userCan(this.privilages.CAN_EDIT_TASK)) {
      if (updatedField === 'dueDate') {
        this.task.dueDate = new Date(this.date);
      }
      const update = {};
      const updateDoc: Object = {};
      update[updatedField] = this.task[updatedField];
      updateDoc['task'] = update;
      updateDoc['_id'] = this.task._id;
      this._taskerService.updateTask(updateDoc).subscribe(() => {
        this._toastr.showInfo(this.getFirstCapital(updatedField) + ' Updated Successfully!');
      });
    } else {
      this._toastr.showError(this._authorizationService.errorMessage);
    }
  }

  changeUser() {
    this.task.assignee = this.selectedUserId;
    this.customUpdate('assignee', 'Assignee');
  }

  selectUser(user) {
    this.selectedUserId = user._id;
    this.selectedUser = user.firstname + ' ' + user.lastname;
    this.userFilterElement.nativeElement.blur();
    this.selectedUserProfile = user.profile;
  }

  getWsUser(workspace) {
    // result.member.map(function (obj) { return { ...obj } });
    this.Users = workspace.member.map(member => member.user);
    this.UserCopy = this.Users.map(function (obj) { return { ...obj } });
    for (let i = 0; i < this.Users.length; i++) {
      if (this.Users[i]._id === this.selectedUserId) {
        this.selectedUser = this.Users[i].firstname + ' ' + this.Users[i].lastname;
        this.selectedUserProfile = this.Users[i].profile;
      }
    }
  }

  getPhases(workspace) {
    this.wsName = workspace;
    this.phase = new Phase();
    this.phases = [];
    this.phases = workspace.phases;
    this.phasesCopy = cloneDeep(this.phases);
  }

  workspaceFilter() {
    this.workspaces = this.workspacesCopy.filter(workspace => new RegExp(`^${this.ws}`, 'gi').test(workspace.wsname));
  }

  phaseFilter() {
    this.phases = this.phasesCopy.filter(phase => new RegExp(`^${this.searchPhase}`, 'gi').test(phase.name));
  }

  filter() {
    this.Users = this.UserCopy.filter(user => new RegExp(`^${this.name}`, 'gi').test(user.firstname));
  }

  deleteUser() {
    this.selectedUserId = null;
    this.selectedUser = null;
    this.selectedUserProfile = null;
  }

  componentReply(updatedField) {
    const object = JSON.parse(updatedField);
    if (object.type === 'taskField') {
      this.task[object.field] = object.value;
      this.customUpdate(object.field);
    } else if (object.type === 'customField') {
      this.updateCustomField(object);
    }
  }

  customFieldToggle(customField) {
    if (!customField.status) {
      let customFieldObj = {};
      customFieldObj = customField;
      (<any>customFieldObj).value = '';
      this.customFields.push(customFieldObj);
      this.updateCustomField({ customFieldId: customField._id, value: '' })
    } else {
      this.customFields.splice(this.customFields.indexOf(this.customFields.find(field => field._id === customField._id)), 1);
      this.updateCustomField({ customFieldId: customField._id, value: '' }, true);
    }
    customField.status = !customField.status;
  }

  updateCustomField(customField, isDelete = false) {
    const updateCustomField = {
      _id: this.task._id,
      customField: {
        customFieldId: customField.customFieldId,
        value: customField.value,
      },
    };
    if (!isDelete) {
      this._taskerService.updateCustomField(updateCustomField).subscribe();
    } else {
      this._taskerService.deleteCustomField(updateCustomField).subscribe();
    }
  }

  getFirstCapital(str: string) {
    return str.charAt(0).toUpperCase() + str.slice(1);
  }

  setTaskTag(tag) {
    this.task.tags = JSON.parse(tag);
    this.customUpdate('tags', 'Tags');
  }

  selectPhase(phase) {
    this.phase = phase;
  }

  moveTaskByWS() {
    const task = new Task();
    task._id = this.task._id;
    task.phase = this.phase._id;
    task.wsId = this.wsName._id;
    task.title = this.task.title;
    task.description = this.task.description;

    this._taskerService.moveTaskByWS(task).subscribe(result => {
      this.dialogrefTask.close();
    });
  }

  redirectCustomFields() {
    const URL = `/workspace/${this.workspaces._id}/settings/customfields`;
    this._dialogRef.close(false);
    this._router.navigate([URL]);
  }

  copyTaskByWS() {
    const phaseId = this.phase._id;
    this.task.phase = phaseId;
    this.task.wsId = this.wsName._id;
    const getAttributes = ['phase', 'title', 'wsId', 'description'];
    const task = pick(this.task, getAttributes);
    this._taskerService.CreateTask(task).subscribe(result => {
      this.dialogrefTask.close();
    });
  }

  ngOnDestroy() {
    if (!!this.workspaceSubject) {
      this.workspaceSubject.unsubscribe();
    }
  }

}


@Pipe({
  name: 'PriorityPipe'
})

export class PriorityPipe implements PipeTransform {
  transform(value: any): any {
    return Priority.find(priority => priority.priority === value).level
  }
}
