import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ReportService } from './report.service';
import { Observable } from 'rxjs/Observable';
import { DataSource } from '@angular/cdk/collections';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
// import { MatPaginator, PageEvent } from '@angular/material';
import 'rxjs/add/observable/of';
import * as moment from 'moment';
// tslint:disable-next-line:no-empty-interface
export interface Data { }

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})


export class ReportComponent implements OnInit {
  public reportDetails = [];
  public dataSource;
  public data;
  public temp;
  public array = [];
  public date = [];
  public workSpaceArray = [];
  public taskTitleArray = [];
  public wsIds = [];
  public fromDate;
  public toDate;
  public taskTitle: string;
  public userId;
  public isFilterPagination: boolean;
  public length;
  pageEvent;
  pageSize = 5;
  pageSizeOptions = [5, 10, 25, 100];
  displayedColumns;

  constructor(private reportService: ReportService) {

    this.reportService.getAllReportDetails().subscribe(report => {
      const flags = [];
      for (let i = 0; i < report.length; i++) {
        // tslint:disable-next-line:curly
        if (flags[report[i].wsId]) continue;
        flags[report[i].wsId] = true;
        this.workSpaceArray.push({
          wsId: report[i].wsId,
          WorkspaceName: report[i].workSpaceName
        });
      }

      for (let i = 0; i < report.length; i++) {
        // tslint:disable-next-line:curly
        if (flags[report[i].taskId]) continue;
        flags[report[i].taskId] = true;
        this.taskTitleArray.push({
          taskId: report[i].taskId,
          title: report[i].title
        });
      }
    });
  }


  ngOnInit() {
    let user = JSON.parse(localStorage.getItem('userDetails'));
    this.userId = user.id;
    this.isFilterPagination = false;
    this.displayedColumns = ['wsName', 'title', 'Assignee', 'startTime', 'stopTime', 'ellipseTime', 'createdOn'];
    this.reportService.getReportDetails(0, 5, this.userId).subscribe(report => {
      this.data = report;
      // tslint:disable-next-line:no-shadowed-variable
      report.map((report) => {
        this.array.push({
          wsId: report.wsId,
          taskId: report.taskId,
          WorkspaceName: report.workSpaceName,
          title: report.title,
          assignee: report.assignee,
          startTime: report.startTime,
          stopTime: report.stopTime,
          consumedTime: report.consumedTime,
          startDate: report.createdOn
        })
        this.length = report.length;
        console.log(this.length)
      });
      this.reportDetails.push(report);
      this.dataSource = new MyDataSource(report);
    });
  }


  public workSpaceFilter(event) {
    if (event.value.length === 0) {
      this.ngOnInit();
    }
    this.wsIds = event.value;
  }

  public selectedFromDate(fDate) {
    this.fromDate = moment(fDate._validSelected).format('YYYY-MM-DD');
  }


  public applyFilter() {

    console.log('filter');
    this.isFilterPagination = true;
    // tslint:disable-next-line:max-line-length
    this.reportService.getAllFilterReportDetails(this.fromDate, this.toDate, this.wsIds, this.taskTitle, 0, 5, this.userId).subscribe(report => {
      this.data = report;
      // tslint:disable-next-line:no-shadowed-variable
      report.map((report) => {
        this.array.push({
          wsId: report.wsId,
          taskId: report.taskId,
          WorkspaceName: report.workSpaceName,
          title: report.title,
          assignee: report.assignee,
          startTime: report.startTime,
          stopTime: report.stopTime,
          consumedTime: report.consumedTime,
          startDate: report.createdOn
        })
        this.length = report.length;
        console.log(this.length)
      });
      this.reportDetails.push(report);
      this.dataSource = new MyDataSource(report);
    });
  }

  public clearFilter() {
    this.fromDate = null;
    this.toDate = null;
    this.wsIds = [];
    this.taskTitle = null;
    this.ngOnInit();
  }
  public selectToDate(tDate) {
    this.toDate = moment(tDate._validSelected).format('YYYY-MM-DD');
  }

  onPaginateChange(event) {
    const skip = (event.pageSize * event.pageIndex);
    if (this.isFilterPagination === false) {
      this.reportService.getReportDetails(skip, event.pageSize, this.userId).subscribe(report => {
        this.data = report;
        // tslint:disable-next-line:no-shadowed-variable
        report.map((report) => {
          this.array.push({
            wsId: report.wsId,
            taskId: report.taskId,
            WorkspaceName: report.workSpaceName,
            title: report.title,
            assignee: report.assignee,
            startTime: report.startTime,
            stopTime: report.stopTime,
            consumedTime: report.consumedTime,
            startDate: report.createdOn
          })
        });
        this.reportDetails.push(report);
        this.dataSource = new MyDataSource(report);
      });
    } else {
      // tslint:disable-next-line:max-line-length
      this.reportService.getAllFilterReportDetails(this.fromDate, this.toDate, this.wsIds, this.taskTitle, skip, event.pageSize, this.userId).subscribe(report => {
        this.data = report;
        // tslint:disable-next-line:no-shadowed-variable
        report.map((report) => {
          this.array.push({
            wsId: report.wsId,
            taskId: report.taskId,
            WorkspaceName: report.workSpaceName,
            title: report.title,
            assignee: report.assignee,
            startTime: report.startTime,
            stopTime: report.stopTime,
            consumedTime: report.consumedTime,
            startDate: report.createdOn
          })
          this.length = report.length;
        });
        this.reportDetails.push(report);
        this.dataSource = new MyDataSource(report);
      });
    }

  }
  public exportToCSV() {
    this.reportService.exportToCSV();
  }
}

export class MyDataSource extends DataSource<any> {
  constructor(private data: Data[]) {
    super();
  }
  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<Data[]> {
    return Observable.of(this.data);
  }

  disconnect() { }

}
