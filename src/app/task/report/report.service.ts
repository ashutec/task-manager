import { Injectable } from '@angular/core';
import { Http, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map'
import { environment } from 'environments/environment';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http/src/static_response';
// import { TaskerService } from '../task-manager/tasker.service';

@Injectable()
export class ReportService {

  taskLength: Subject<any> = new Subject<any>();
  constructor(private _http: Http) { }


  public getAllReportDetails() {
    return this._http.get('/timeSheet/getAllReportDetails').map(res => res.json());
  }


  public getReportDetails(skip, limit, userId) {
    return this._http.post('/timeSheet/getReportDetails', { skip: skip, limit: limit, userId: userId }).map(res => res.json());
  }


  public getFilterDateReportDetails(fromDate, toDate) {
    return this._http.post('/timeSheet/getFilterReportDetails', { fromDate: fromDate, toDate: toDate }).map(res => res.json());
  }

  public getFilterReportDetailByWorkSpace(objectId) {
    return this._http.post('/timeSheet/getFilterReportByWorkSpace', { objectId: objectId }).map(res => res.json());
  }


  public getAllFilterReportDetails(fromDate, toDate, objectId, taskTitle, skip, limit, userId) {
    // tslint:disable-next-line:max-line-length
    return this._http.post('/timeSheet/getAllFilterReportDetails', { fromDate: fromDate, toDate: toDate, objectId: objectId, taskTitle: taskTitle, skip: skip, limit: limit, userId: userId }).map(res => res.json());
  }


  public exportToCSV() {

    console.log('CSV');
    // tslint:disable-next-line:no-shadowed-variable
    return this._http.get('/timeSheet/exportToCSV').subscribe(Response => this.downloadFile(Response)),
      // tslint:disable-next-line:no-unused-expression
      error => console.log('Error downloading the file.'),
      // tslint:disable-next-line:no-console
      () => console.info('OK');
  }

  downloadFile(res) {
    const blob = new Blob([res._body], { 'type': 'text/csv;charset=utf8;' });
    const uri = 'data:attachment/csv;charset=utf-8,' + encodeURI(res._body);
    const link = document.createElement('a');
    link.href = URL.createObjectURL(blob);
    link.setAttribute('visibility', 'hidden');
    link.download = 'file.csv';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
    // const blob = new Blob([data], { type: 'text/csv' });
    // const url = window.URL.createObjectURL(blob);
    // window.open(url);
  }


}
