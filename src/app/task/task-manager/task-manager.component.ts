import { Component, OnInit, Input, Output, EventEmitter, OnDestroy, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { MatDialog, MatDialogRef, MatDialogConfig, MatSidenav } from '@angular/material';
import { TaskerService } from './tasker.service';
import { EditTaskComponent } from '../edit-task/edit-task.component';
import { PhaseComponent } from '../phase/phase.component';
import { Workspace } from '../../workspace/workspace/workspace';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { AddNewTaskComponent } from '../add-new-task/add-new-task.component';
import { Task } from '../Tasks/task';
import { DragulaService } from 'ng2-dragula';
import { Subscription } from 'rxjs/Subscription';
import { WorkspaceService } from '../../workspace/workspace/workspace.service';
import { TokenService } from '../../http-interceptor/token.service';
import { SocketService } from '../shared/socket.service';
import { SocketActionService } from '../../shared/SocketAction.service';
import { SpeechRecognitionService } from '../speech-recognizer/speech-recognition.service';
import { SocketAction } from '../../shared/Socket.actions';
import { ToastrService } from '../shared/toaster.service';
import { AuthorizationService } from '../../shared/Authorization.service';
import { Privilages } from '../../shared/privilages.enum';
import { SpeechRecognizerComponent } from '../speech-recognizer/speech-recognizer.component';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/mergeMap';

@Component({
  selector: 'app-task-manager',
  templateUrl: './task-manager.component.html',
  styleUrls: ['./task-manager.component.css'],
})

export class TaskManagerComponent implements OnInit, OnDestroy, AfterViewInit {
  phases: any = new Array();
  wsData: Workspace = new Workspace();
  isNewPhase: Boolean = false;
  dialogref: MatDialogRef<AddNewTaskComponent>;
  speechRecoDialogue: MatDialogRef<SpeechRecognizerComponent>;
  phase: any;
  dropmodel: Subscription;
  workspaces;
  workspacesCopy;
  name: String;
  isNewWorkspace = false;
  following = true;
  dialogrefTask: MatDialogRef<EditTaskComponent>;
  public privilages = Privilages;
  @ViewChild('sidenav') sidenav: MatSidenav;

  constructor(public dialog: MatDialog,
    private _taskerService: TaskerService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _dragulaService: DragulaService,
    private _location: Location,
    private _workspaceService: WorkspaceService,
    private _tokenService: TokenService,
    private _socketService: SocketService,
    private _socketActionService: SocketActionService,
    private _speechRecognitionService: SpeechRecognitionService,
    private _toastrService: ToastrService,
    public _authorizationService: AuthorizationService,
  ) {
  }

  ngAfterViewInit(): void {
    // throw new Error("Method not implemented.");
    // this._router.events.subscribe(() => {
    this._activatedRoute.params.subscribe(params => {
      if (this._router.url.includes('/tasks')) {
        const taskId = this._activatedRoute.firstChild.snapshot.params['taskId'];
        this._taskerService.getSingleTask(taskId).subscribe((task) => {
          setTimeout(() => {
            this.EditTask(task);
          });
        });
      }
    });

    // })

  }

  ngOnInit() {
    const localthis = this;
    const body = document.getElementsByTagName('body')[0];
    body.classList.add('body');
    this._activatedRoute.params.map(res => res['Id'])
      .flatMap(wsId => this._taskerService.getPhases(wsId))
      .map(res => this.wsData = res)
      .subscribe(() => {
        if (this.wsData.watchers.findIndex(watcher => watcher === this._tokenService.getLoggedInUser().id) !== -1) {
          this.following = false;
        }
      }
      , err => {
        this._toastrService.showError(err._body);
        this._router.navigate(['/workspace']);
      });

    this._taskerService.phases.subscribe(phases => this.phases = phases)

    this._speechRecognitionService.createTask.subscribe(data => {
      this.addNewTask();
    })

    this._socketActionService.RegisterNewSubscription(SocketAction.Task_Phase_Move, (notificaionData) => {
      this._taskerService.movePhaseAction(notificaionData);
    });

    this._socketActionService.RegisterNewSubscription(SocketAction.New_Task_Added, (notificaionData) => {
      this._taskerService.TaskAddedAction(notificaionData);
    });

    this._socketActionService.RegisterNewSubscription(SocketAction.Task_Field_Updated, (notificaionData) => {
      this._taskerService.TaskUpdatedAction(notificaionData);
    });

    this._socketActionService.RegisterNewSubscription(SocketAction.Task_Deleted, (notificaionData) => {
      this._taskerService.TaskDeletedAction(notificaionData);
    });

    this.dropmodel = this._dragulaService.dropModel.subscribe(value => {
      if (value[0] === 'taskBag') {
        this._taskerService.updateOnDrop(value)
      }
      if (value[0] === 'phaseBag') {
        this._taskerService.updatePhasePosition(this.wsData._id, this.phases).subscribe();
      }
    });

    this._dragulaService.setOptions('phaseBag', {
      moves: function (el, container, handle) {
        if (handle.className.indexOf('handle') !== -1) {
          return true;
        }
      },
    });

    this._dragulaService.setOptions('taskBag', {
      accepts: function (el, target, source, sibling) {
        return localthis.wsData.phases.find(phase => phase._id === target.id).allowedActions.includes(source.id) || target.id === source.id;
      }
    });

    this._workspaceService.getWorkspaces().subscribe((result: any[]) => {
      this.workspaces = result.filter(workspace => workspace._id !== this.wsData._id);
      this.workspacesCopy = result.map(function (obj) { return { ...obj } });
    });

    // this.chatbotFilters();
  }

  addNewTask(phase?: string) {
    this.dialogref = this.dialog.open(AddNewTaskComponent, {
      width: '425px',
      data: {
        phase
      }
    });
    this.dialogref.afterClosed().subscribe(data => {
      if (data.isContinue) {
        this.EditTask(data.task);
      }
    });
  }

  gotoSettings() {
    if (this._authorizationService.userCan(this.privilages.CAN_MANAGE_WORKSPACE_SETTINGS)) {
      this._router.navigateByUrl('/workspace/' + this.wsData._id + '/settings');
    } else {
      this._toastrService.showError(this._authorizationService.errorMessage);
    }
  }

  gotoAnalytics() {
    // if (this._authorizationService.userCan(this.privilages.CAN_MANAGE_WORKSPACE_SETTINGS)) {
    this._router.navigateByUrl('/workspace/' + this.wsData._id + '/analytics');
    // } else {
    //   this._toastrService.showError(this._authorizationService.errorMessage);
    // }
  }

  followWorkSpace() {
    this._taskerService.followWorkSpace(this.wsData._id, this.following).subscribe();
    this.following = !this.following;
  }

  createPhase(phaseName) {
    this._taskerService.createPhase(this.wsData._id, phaseName).subscribe(
      phases => this.wsData.phases = phases,
      err => this._toastrService.showError(err._body));
    this.isNewPhase = false
  };

  goBack() {
    this._location.back();
  }

  reload() {
    this._taskerService.getPhases(this.wsData._id).subscribe();
  }

  filter() {
    this.workspaces = this.workspacesCopy.filter(workspace => new RegExp(`^${this.name}`, 'gi').test(workspace.wsname));
  }

  filterByTitleOrDesc(value) {
    this._taskerService.taskFilter.next({ titleOrDescFilter: value });
  }

  openWorkspace(workSpace, event) {
    this.name = workSpace.wsname;
    this.workspaces = this.workspacesCopy.filter(ws => ws._id !== workSpace._id);
    this._router.navigate(['/workspace/', workSpace._id]);
    this.isNewWorkspace = true;
  }

  EditTask(task) {
    const config = new MatDialogConfig();
    config.data = Object.assign({}, task);
    config.width = '700px';
    config.disableClose = true;
    this.dialogrefTask = this.dialog.open(EditTaskComponent, config);
    this.dialogrefTask.afterClosed().subscribe(() => {
      const wsId = this._activatedRoute.snapshot.params['Id'];
      this._router.navigate(['workspace/' + wsId]);
    })
  }

  openChatBotDialogue() {
    this.speechRecoDialogue = this.dialog.open(SpeechRecognizerComponent, {
      width: '600px',
    });
  }

  public ngOnDestroy() {
    const body = document.getElementsByTagName('body')[0];
    body.classList.remove('body');

    if (this._dragulaService.find('phaseBag') !== undefined) {
      this._dragulaService.destroy('phaseBag');
    }

    if (this._dragulaService.find('taskBag') !== undefined) {
      this._dragulaService.destroy('taskBag');
    }

    if (!!this.dropmodel) {
      this.dropmodel.unsubscribe();
    }
  }

  // chatbotFilters() {
  //   this._speechRecognitionService.assigneeFilter.subscribe(() => {
  //     this.speechRecoDialogue.close();
  //     this.sidenav.toggle();
  //   });

  // }

}
