import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { DragulaService } from 'ng2-dragula/ng2-dragula';
import { Task } from '../tasks/task';
import { environment } from '../../../environments/environment';
import { PhaseService } from '../phase/phase.service';
import { TaskService } from '../tasks/task.service';
import { WorkspaceService } from '../../workspace/workspace/workspace.service';
import { CustomFieldService } from '../../workspace/custom-field-settings/customfield.service';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AuthorizationService } from '../../shared/Authorization.service';
import { TokenService } from '../../http-interceptor/token.service';
import 'rxjs/add/operator/map';

@Injectable()
export class TaskerService {

  phases: Subject<any> = new Subject<any>();
  taskAdded: Subject<any> = new Subject<any>();
  taskRemoved: Subject<any> = new Subject<any>();
  taskUpdated: Subject<any> = new Subject<any>();
  taskFilter: Subject<any> = new Subject<any>();
  workspace: BehaviorSubject<any> = new BehaviorSubject<any>({});

  constructor(private _http: Http,
    private _phaseService: PhaseService,
    private _taskService: TaskService,
    private _workspaceService: WorkspaceService,
    private _authorizationService: AuthorizationService,
    private _tokenService: TokenService,
    private _customFieldService: CustomFieldService,
  ) {
  }

  updateOnDrop(value: any) {
    const phaseChange = {
      id: value[1].getAttribute('id'),
      phase: value[2].getAttribute('id'),
      previousValue: value[3].getAttribute('phasename'),
      previousPhaseId: value[3].getAttribute('id'),
      newValue: value[2].getAttribute('phasename')
    }
    if (phaseChange.previousValue !== phaseChange.newValue) {
      this.moveTask(phaseChange).subscribe();
    }
  }

  getCustomFields() {
    return this._customFieldService.getFields(this.workspace.value._id)
  }

  getCustomFieldsSubject() {
    if (this._customFieldService.allFields.value.length == 0 && this.workspace.value._id) {
      this._customFieldService.getFields(this.workspace.value._id).subscribe();
    }
    return this._customFieldService.allFields;
  }

  movePhaseAction(notificaionData) {
    this.taskRemoved.next({ phase: notificaionData.previousValue, _id: notificaionData.data._id });
    this.taskAdded.next(notificaionData.data);
  }

  TaskAddedAction(notificaionData) {
    this.taskAdded.next(notificaionData);
  }

  TaskUpdatedAction(notificaionData) {
    this.taskUpdated.next(notificaionData);
  }

  TaskDeletedAction(notificaionData) {
    this.taskRemoved.next(notificaionData);
  }

  followTask(taskId, isFollow: boolean) {
    return this._http.put('/task/follow', { taskId, isFollow })
      .map(res => res.json())
      .map((task) => {
        this.taskUpdated.next(task);
        return task
      });
  }

  followWorkSpace(wsId, isFollow: boolean) {
    return this._http.put('/workspace/follow', { wsId, isFollow })
      .map(res => res.json())
      .map((workspace) => {
        return workspace;
      });
  }

  createPhase(wsId, phaseName) {
    return this._phaseService.createPhase(wsId, phaseName)
      .map(res => {
        this.phases.next(res);
        return res;
      })
  }

  deletePhase(wsId, phaseId) {
    return this._phaseService.deletePhase(wsId, phaseId)
      .map(res => {
        this.phases.next(res.phases);
        return res;
      });
  }

  getSingleWorkSpace(wsId: string, currentWorkspace = false) {
    return this._workspaceService.getSingleWorkspace(wsId).map(workspace => {
      this.workspace.next(workspace);
      this.registerNewUserPrivilages(workspace);
      return workspace
    });
  }

  updatePhaseName(phaseName, phaseId, wsId) {
    /* console.log(phaseId); */
    return this._phaseService.updatePhaseName(phaseName, phaseId);
  }

  updatePhasePosition(wsId, phases) {
    return this._phaseService.updatePhasePosition(wsId, phases).map(workspace => workspace.phases);
  }

  getPhases(wsId) {
    return this._workspaceService.getSingleWorkspace(wsId).map(workSpace => {
      if (workSpace && workSpace.phases) {
        this.phases.next(workSpace.phases);
        this.workspace.next(workSpace);
        this.registerNewUserPrivilages(workSpace);
        return workSpace;
      } else {
        throw new Error(workSpace);
      }
    })
  }

  registerNewUserPrivilages(workspace) {
    const currentUser = workspace.member.find(member => member.user._id === this._tokenService.getLoggedInUser().id);
    this._authorizationService.newUserPrivilages(currentUser.role.privilages);
  }

  CreateTask(task): Observable<any> {
    return this._taskService.createTask(task).map(newTask => {
      this.taskAdded.next(newTask);
      return newTask;
    })
  }

  getSingleTask(taskId) {
    return this._taskService.getSingleTask(taskId);
  }

  updateTask(task: any): Observable<any> {
    const oldTask = task
    return this._taskService.updateTask(task).map(updatedTask => {
      this.taskUpdated.next(updatedTask);
      return updatedTask;
    });
  }

  updateCustomField(customField: any): Observable<any> {
    return this._taskService.updateCustomField(customField).map(updatedTask => {
      this.taskUpdated.next(updatedTask);
      return updatedTask;
    });
  }

  deleteCustomField(customField: any): Observable<any> {
    return this._taskService.deleteCustomField(customField).map(updatedTask => {
      this.taskUpdated.next(updatedTask);
      return updatedTask;
    });
  }

  deleteTask(taskId) {
    return this._taskService.deleteTask(taskId).map(task => {
      this.taskRemoved.next(task);
      return task;
    });
  }

  moveTask(task) {
    const oldTask = task;
    return this._taskService.moveTask(task).map(newTask => {
      this.taskUpdated.next(task);
      return newTask;
    });
  }

  updateEstimateTime(task) {
    return this._http.post('/task/estimateTime', task).map(result => result.json());
  }

  uploadFile(fileObj) {
    return this._http.post('/task/fileUpload', fileObj).map(result => result.json());
  }

  timerUpdates(updatedTask) {
    this.taskUpdated.next(updatedTask);
    return updatedTask;
  }

  filterTask(task) {
    this.taskFilter.next(task);
  }

  getPhasesByWSId(wsId) {
    return this._phaseService.getPhases(wsId).map(workSpace => {
      return workSpace;
    })
  }

  moveTaskByWS(task: Task) {
    return this._taskService.moveTaskByWS(task).map(updatedTask => {
      this.taskRemoved.next(updatedTask.data);
      return updatedTask.data;
    })
  }

}
