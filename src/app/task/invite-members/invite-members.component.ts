import { Component, OnInit, Input, ElementRef, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef, MatSnackBar, MatSnackBarContainer, MatDialog } from '@angular/material'
import { MemberService } from './member.service';
import { Http } from '@angular/http'
import { Router } from '@angular/router';
import { AlertBoxComponent } from '../alert-box/alert-box.component';
import { TokenService } from '../../http-interceptor/token.service'
import { DataSource } from '@angular/cdk/collections';
import { Observable } from 'rxjs/Observable';
import * as differenceWith from 'lodash/differencewith';
import * as findIndex from 'lodash/findIndex';
import { ToastrService } from '../shared/toaster.service';
import { TaskerService } from '../task-manager/tasker.service';
import { RoleService } from '../../shared/roles.service';
import { MemberSettingsService } from '../../workspace/members-setting/members-setting.service';
import { AuthorizationService } from '../../shared/Authorization.service';
import { Privilages } from '../../shared/privilages.enum';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-invite-members',
  templateUrl: './invite-members.component.html',
  styleUrls: ['./invite-members.component.css']
})


export class InviteMembersComponent implements OnInit {
  workspace;
  Users = [];
  SelectedUsers = [];
  name;
  displayedColumns = ['members', 'role', 'actions'];
  dataSource: MemberDataSource;
  deleteDialogRef: MatDialogRef<AlertBoxComponent>
  hideTable = true;
  loggedInUser;
  roles;
  isEdit: string = "";
  selectedRole: string;
  public privilages = Privilages;

  @ViewChild('userFilterElement') userFilterElement: ElementRef;

  constructor(private dialogRef: MatDialogRef<InviteMembersComponent>,
    private _memberService: MemberService,
    private http: Http,
    private _router: Router,
    private _dialog: MatDialog,
    private _taskerService: TaskerService,
    private _tokenService: TokenService,
    private _toastrService: ToastrService,
    private _roleService: RoleService,
    private _memberSettingsService: MemberSettingsService,
    public _authorizationService: AuthorizationService,
  ) { }

  ngOnInit() {
    this.loggedInUser = this._tokenService.getLoggedInUser();
    this.dataSource = new MemberDataSource(this._memberService);
    this.Users = [];
    this._taskerService.workspace.subscribe(workspace => {
      this.workspace = workspace;
      this._memberService.newWsUser(workspace.member);
      this.hideTable = false;
    });
    this._roleService.getRoles().subscribe(roles => this.roles = roles);
  }

  addUser(user) {
    this.SelectedUsers.push(user);
    for (let i = 0; i < this.SelectedUsers.length; i++) {
      this.Users.splice(this.Users.indexOf(user))
    }
    this.userFilterElement.nativeElement.blur();
  }

  editUserRole(userId) {
    if (this._authorizationService.userCan(this.privilages.CAN_EDIT_MEMBER)) {
      this.isEdit = userId;
    } else {
      this._toastrService.showError(this._authorizationService.errorMessage);
    }
  }

  deleteUser(SelectedUser) {
    this.SelectedUsers.splice(this.SelectedUsers.indexOf(SelectedUser), 1);
  }

  filter() {
    this._memberService.getUser(this.name, this.workspace._id).subscribe(result => {
      this.Users = [];
      this.Users = differenceWith(result, this.SelectedUsers, this.idChecker);
    });
  }

  idChecker(result, SelectedUser) {
    return result._id === SelectedUser._id;
  }

  SaveUserRole(row) {
    if (!this.selectedRole)
      return this._toastrService.showError('Please Select a Role for ' + row.user.firstname);
    this._memberSettingsService.updateRole(this.workspace._id, row.user._id, this.selectedRole)
      .map(workspace => { this._taskerService.workspace.next(workspace); return workspace; })
      .subscribe((workspace) => this._memberService.newWsUser(workspace.member));
    this.reset();
  }

  reset() {
    this.selectedRole = '';
    this.isEdit = '';
  }

  addAllUser() {
    this._memberService.addAllUser(this.SelectedUsers, this.workspace._id)
      .map(workspace => this._taskerService.workspace.next(workspace))
      .subscribe(() => {
        this.SelectedUsers = [];
        this._toastrService.showSuccess('Members added successfully');
        // this.getNewWsUsers();
      });
  }

  public getNewWsUsers() {
    const a = this._router.url.split('/');
    const wsid = a[a.length - 1];
    this._memberService.getWsUser({ wsid }).subscribe(members => {
      members.splice(findIndex(members, (member) => {
        return member._id === this._tokenService.getLoggedInUser().id;
      }), 1);
      this._memberService.newWsUser(members);
      if (members.length) {
        this.hideTable = false;
      }
    })
  }

  public deleteMember(memberId) {
    const a = this._router.url.split('/');
    const wsid = a[a.length - 1];
    this.deleteDialogRef = this._dialog.open(AlertBoxComponent, { width: '500px' });
    this.deleteDialogRef.afterClosed().subscribe(res => {
      if (res) {
        this._memberService.deleteMember(wsid, memberId)
          .map(workspace => this._taskerService.workspace.next(workspace))
          .subscribe();
      }
    });
  }

}

class MemberDataSource extends DataSource<any> {
  constructor(private _memberService: MemberService) {
    super();
  }

  connect(): Observable<any> {
    this._memberService.memberSubject.subscribe(result => { });
    return this._memberService.memberSubject;
  }

  disconnect() { }
}
