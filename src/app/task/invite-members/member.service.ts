import { Injectable } from '@angular/core';
import { Http, RequestOptionsArgs, Headers } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { TokenService } from '../../http-interceptor/token.service'

@Injectable()
export class MemberService {

  memberSubject = new BehaviorSubject<any>([]);
  constructor(private http: Http, private _tokenService: TokenService) { }

  public addMember(MemberArray) {
    const wsid = JSON.parse(localStorage.getItem('currentws'))._id;
    const data = { MemberArray, wsid }
    return this.http.post('/workspace/addMember', data).map(result => result.json());
  }

  public getUser(name,  wsid) {
    return this.http.post('/workspace/getUser', { name, wsid }).map(result => result.json());
  }

  public addAllUser(SelectedUsers, wsid) {
    return this.http.post('/workspace/addAllUser', { users: SelectedUsers, wsid }).map(result => result.json());
  }

  // for get subject
  public newWsUser(wsUser) {
    this.memberSubject.next(wsUser);
  }

  public getWsUser(wsid) {
    return this.http.post('/workspace/getWsUser', wsid).map(result => result.json());
  }

  public deleteMember(wsid, userid) {
    const body: RequestOptionsArgs = {
      body: { wsid, userid }
    }
    return this.http.delete('/workspace/deleteMember', body).map(result => result.json());
  }
}
