export class QueryRequest {
  sessionId: string;
  query: string;
  contexts: Array<{}>;
  lang: string;
  event: Object;
}
