export class Entity {
  sessionId: string;
  name: string;
  extend: boolean;
  entries: Array<{}>;
}
