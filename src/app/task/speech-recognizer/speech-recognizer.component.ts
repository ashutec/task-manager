import { Component, OnInit, OnDestroy, ElementRef, ViewChild, AfterViewChecked } from '@angular/core';
import { SpeechRecognitionService } from './speech-recognition.service';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import { SpeechResponse } from './SpeechResponse';

import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';
@Component({
  selector: 'app-speech-recognizer',
  templateUrl: './speech-recognizer.component.html',
  styleUrls: ['./speech-recognizer.component.css']
})
export class SpeechRecognizerComponent implements OnInit, OnDestroy, AfterViewChecked {
  @ViewChild('mic') private mic: ElementRef;
  @ViewChild('chatbotDiv') private chatbotDiv: ElementRef;
  showMic: boolean;
  speechData: string;
  intent: SpeechResponse = new SpeechResponse();
  conversationList: SpeechResponse[] = new Array<SpeechResponse>();
  loader = false;
  private observableObject: Observable<string>;
  finished: boolean;
  anyErrors: boolean;
  showInput = false;
  isListening = false;

  constructor(private _speechRecognitionService: SpeechRecognitionService,
    public dialog: MatDialog) {
    this.showMic = true;
    this.speechData = '';
  }

  ngOnInit() {
    this.loader = true;
    this._speechRecognitionService.getIntent(this.intent.message, true).subscribe((data) => {
      Observable.from(data.result.fulfillment.data.messages).zip(
        Observable.interval(500), (a, b) => a)
        .subscribe(
        (message) => {
          this.loader = false;
          const response: SpeechResponse = new SpeechResponse();
          response.message = message;
          response.user = 'system';
          this.conversationList.push(response);
          this.loader = true;
        }, () => { }, () => this.loader = false);
      this.intent = new SpeechResponse();
    });
    // this.loader = false;
  }

  ngAfterViewChecked() {
    const element = this.chatbotDiv.nativeElement;
    element.scrollTop = element.scrollHeight;
  }

  activateSpeechSearch(): void {
    this.loader = true;
    this.intent.user = 'user';
    this.conversationList.push(this.intent);
    this._speechRecognitionService.getIntent(this.intent.message, false).subscribe((data) => {
      if (data.result.fulfillment.data) {
        Observable.from(data.result.fulfillment.data.messages).zip(
          Observable.interval(800), (a, b) => a)
          .subscribe(
          (message) => {
            this.loader = false;
            const response: SpeechResponse = new SpeechResponse();
            response.message = message;
            response.user = 'system';
            this.conversationList.push(response);
            this.loader = true;
            this.showMic = true;
          }, () => { }, () => this.loader = false);
        this.intent = new SpeechResponse();
      } else {
        this.loader = false;
        const response: SpeechResponse = new SpeechResponse();
        response.message = data.result.fulfillment.speech;
        response.user = 'system';
        this.conversationList.push(response);
        this.intent = new SpeechResponse();
        this.showMic = true;
      }


      // this.intent.message = '';
    })
    // if (!this.showMic) {
    //   this.showMic = true;
    //   // this.mic.nativeElement.src = '//google.com/intl/en/chrome/assets/common/images/content/mic-animate.gif';
    //   this.speechRecognitionService.record()
    //     .subscribe(
    //     // listener
    //     (value) => {
    //       this.speechData = this.intent;
    //       // this.speechRecognitionService.getIntent(value).subscribe((data) => {
    //       //   console.log('data: ' + data);
    //       // })
    //       // console.log(value);
    //     },
    //     // errror
    //     (err) => {
    //       console.log(err);
    //       if (err.error === 'no-speech') {
    //         console.log('--restatring service--');
    //         this.activateSpeechSearch();
    //       }
    //     },
    //     // completion
    //     () => {
    //       this.showMic = false;
    //       console.log('--complete--');
    //       // this.mic.nativeElement.src = '//google.com/intl/en/chrome/assets/common/images/content/mic.gif';
    //       // this.activateSpeechSearch();
    //     });
    // } else {
    //   // this.mic.nativeElement.src = '//google.com/intl/en/chrome/assets/common/images/content/mic.gif';
    //   this.speechRecognitionService.DestroySpeechObject();
    //   this.showMic = false;
    // }
    // })
  }

  traceInput(event) {
    this.intent.message = event;
    if (event === '') {
      this.showMic = true;
    } else {
      this.showMic = false;
    }
  }
  showHide() {
    if (this.showInput) {
      this.showInput = false;
    } else {
      this.showInput = true;
    }

    // this.micro.nativeElement.css = 'margin-right:0px;'
  }

  listen() {
    this.showInput = false;
    // if (!this.showMic) {
    //   // this.showMic = true;
    //   this.mic.nativeElement.src = '//google.com/intl/en/chrome/assets/common/images/content/mic-animate.gif';
    //   this._speechRecognitionService.record()
    //     .subscribe(
    //     // listener
    //     (value) => {
    //       this.speechData = this.intent.message.toString();
    //       // this._speechRecognitionService.getIntent(value).subscrsdibe((data) => {
    //       //   console.log('data: ' + data);
    //       // })
    //       console.log(value);
    //     },
    //     // errror
    //     (err) => {
    //       console.log(err);
    //       if (err.error === 'no-speech') {
    //         console.log('--restatring service--');
    //         // this.activateSpeechSearch();
    //       }
    //     },
    //     // completion
    //     () => {
    //       this.showMic = false;
    //       console.log('--complete--');
    //       // this.mic.nativeElement.src = '//google.com/intl/en/chrome/assets/common/images/content/mic.gif';
    //       // this.activateSpeechSearch();
    //     });
    // } else {
    //   // this.mic.nativeElement.src = '//google.com/intl/en/chrome/assets/common/images/content/mic.gif';
    //   this._speechRecognitionService.DestroySpeechObject();
    //   this.showMic = false;
    // }
  }

  ngOnDestroy() {
    this._speechRecognitionService.DestroySpeechObject();
  }

}
