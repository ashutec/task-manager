import { Injectable, NgZone } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Subject } from 'rxjs/Subject';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import * as trim from 'lodash/trim';
import * as find from 'lodash/find';
import { environment } from '../../../environments/environment';
import { Entity } from './Entity';
import { QueryRequest } from './QueryRequest';
import { TokenService } from '../../http-interceptor/token.service';
import { TaskerService } from '../task-manager/tasker.service';

interface IWindow extends Window {
  webkitSpeechRecognition: any;
  SpeechRecognition: any;
}

@Injectable()
export class SpeechRecognitionService {

  speechRecognition: any;
  command = '';
  apiAiURL = 'https://api.api.ai/v1';

  createTask: Subject<any> = new Subject<any>();
  assigneeFilter: Subject<any> = new Subject<any>();
  searchUser: Subject<any> = new Subject<any>();
  activateLoader: Subject<any> = new Subject<any>();
  openTask: Subject<any> = new Subject<any>();
  userFilter: Subject<any> = new Subject<any>();
  clearAllFilter: Subject<any> = new Subject<any>();

  constructor(private zone: NgZone,
    private http: Http,
    private tokenService: TokenService,
    public _taskerService: TaskerService) {
  }

  record(): Observable<string> {

    return Observable.create(observer => {
      const { webkitSpeechRecognition }: IWindow = <IWindow>window;
      this.speechRecognition = new webkitSpeechRecognition();
      // this.speechRecognition = SpeechRecognition;
      this.speechRecognition.continuous = true;
      // this.speechRecognition.interimResults = true;
      this.speechRecognition.lang = 'en-us';
      this.speechRecognition.maxAlternatives = 1;

      this.speechRecognition.onresult = speech => {
        let term = '';
        if (speech.results) {
          const result = speech.results[speech.resultIndex];
          const transcript = result[0].transcript;
          if (result.isFinal) {
            if (result[0].confidence < 0.3) {
              console.log('Unrecognized result - Please try again');
            } else {
              term = trim(transcript);
              console.log('Did you said? -> ' + term + ' , If not then say something else...');
            }
          }
        }
        this.zone.run(() => {
          observer.next(term);
        });
      };

      this.speechRecognition.onerror = error => {
        observer.error(error);
      };

      this.speechRecognition.onend = () => {
        observer.complete();
      };

      this.speechRecognition.start();
      console.log('Say something - We are listening !!!');
    });
  }

  getIntent(text, defaultActionEvent) {
    const userId = this.tokenService.getLoggedInUser().id;
    const jwt = this.tokenService.getToken();
    const wsId = this.tokenService.getWorkspaceId();
    // let data;
    const headers = new Headers({ 'content-type': 'application/json', });
    headers.append('Authorization', 'Bearer' + environment.apiAIClentId);
    const options = new RequestOptions({ headers: headers });

    const queryRequest = new QueryRequest();
    if (defaultActionEvent) {
      queryRequest.event = { name: 'custom_event' }
    } else {
      queryRequest.query = text;
    }

    queryRequest.lang = 'en';
    queryRequest.contexts = new Array();
    queryRequest.sessionId = userId;
    queryRequest.contexts.push({
      'name': 'auth',
      'lifespan': 1,
      'parameters': {
        'token': jwt
      }
    });
    queryRequest.contexts.push({
      'name': 'workspace',
      'lifespan': 1,
      'parameters': {
        'wsId': wsId
      }
    });
    return this.http.post(this.apiAiURL + '/query?v=20150910', queryRequest, options).map((response: Response) => {
      const responseData = response.json() && response.json();
      const contexts = responseData.result.contexts;
      let newTask;
      if (contexts.length > 0) {
        const newTaskParameters = find(contexts, { name: 'created_task' });
        if (newTaskParameters) {
          newTask = newTaskParameters.parameters.createdTask;
          console.log(newTask);
          this._taskerService.taskAdded.next(newTask);
        }
      }
      // console.log(newTask);
      // console.log(responseData);
      if (responseData.status.code === 200) {
        if (responseData.result) {
          if (responseData.result.metadata) {
            if (responseData.result.metadata.intentName) {
              if (responseData.result.metadata.intentName === 'Create task') {
                // this.createTask.next('create task');
                console.log('create task');
              } else if (responseData.result.metadata.intentName === 'open created task'
                || responseData.result.metadata.intentName === 'wants to open a task? yes') {
                this.openTask.next(newTask);
              } else if (responseData.result.metadata.intentName === 'Filter - Tasks Assigned to') {
                this.assigneeFilter.next();
                setTimeout(() => {
                  this.userFilter.next(responseData.result.parameters.assignedTo);
                })
              } else if (responseData.result.metadata.intentName === 'Filter- clear all') {
                this.clearAllFilter.next(responseData.result.parameters.filter);
              }
            }
          }
        }
      }
      return responseData;
    })

  }

  setUserEntity() {
    const jwt = this.tokenService.getToken();
    const headers = new Headers({ 'content-type': 'application/json', });
    headers.append('Authorization', 'Bearer' + environment.apiAIClentId);
    const options = new RequestOptions({ headers: headers });
    const data = {
      value: jwt,
      synonyms: ['user', 'userDetails', 'userId']
    }
    const userEntity = new Entity();
    userEntity.sessionId = 'ashutecAPIAI';
    userEntity.extend = false;
    userEntity.name = 'user';
    userEntity.entries = new Array();
    userEntity.entries.push(data);
    return this.http.post(this.apiAiURL + '/userEntities?v=20150910&sessionId=ashutecAPIAI',
      userEntity, options).map((response: Response) => {
        const responseData = response.json() && response.json();
        console.log(responseData);
      })
  }


  DestroySpeechObject() {
    if (this.speechRecognition) {
      this.speechRecognition.stop();
    }
  }


}
