import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTaskTabsComponent } from './edit-task-tabs.component';

describe('EditTaskTabsComponent', () => {
  let component: EditTaskTabsComponent;
  let fixture: ComponentFixture<EditTaskTabsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditTaskTabsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTaskTabsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
