import { Component, OnInit, Input, ViewChildren, ViewChild, AfterViewInit, QueryList } from '@angular/core';
import { Task } from '../tasks/task';
import { MatTabChangeEvent, MatTab, MatTabGroup } from '@angular/material';
import { TabService } from '../shared/tabs.service';

@Component({
  selector: 'app-edit-task-tabs',
  templateUrl: './edit-task-tabs.component.html',
  styleUrls: ['./edit-task-tabs.component.css']
})
export class EditTaskTabsComponent implements OnInit, AfterViewInit {

  @Input() activeTabId = 0;
  @Input() obj: Task;
  @ViewChild('tabGroup') tabGroup: MatTabGroup;
  @ViewChildren(MatTab) tabs: QueryList<MatTab>;
  constructor(private _tabService: TabService
  ) { }

  ngOnInit() {

  }
  ngAfterViewInit() {
    this.tabGroup.selectedIndex = this.activeTabId;
  }

  tabChanged(event: MatTabChangeEvent) {
    console.log(event);
    const textLabel = event.tab.textLabel.toString();
    this._tabService.newActiveTab(textLabel);
  }

}
