import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TaskManagerComponent } from './task/task-manager/task-manager.component';
import { WorkspaceComponent } from './workspace/workspace/workspace.component';
import { HomeComponent } from './workspace/home/home.component';
import { AuthGuard } from './authentication/guards/auth.guard';
import { AuthenticationComponent } from './authentication/authentication.component';
import { ProfilePictureComponent } from './authentication/profile-picture/profile-picture.component';


// const appRoutes: Routes = [
//   { path: '', component: LoginSignupComponent },

//   { path: 'taskmanager', component: TaskManagerComponent },
//   { path: 'workspace', component: WorkspaceComponent, canActivate: [AuthGuard] }
// ];


const appRoutes: Routes = [
  {
    path: '',
    children: [
      { path: '', redirectTo: '/workspace', pathMatch: 'full' },
    ],
    component: HomeComponent, canActivate: [AuthGuard]
  },
  // { path: '**', canActivate: [AuthGuard], redirectTo: '' }
]


export const appRouting: ModuleWithProviders = RouterModule.forRoot(appRoutes);
