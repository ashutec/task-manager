import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpModule, Http, XHRBackend, RequestOptions } from '@angular/http';
import { httpFactory } from './http.factory';
import { LoaderService } from './loader/loader.service';
import { LoaderComponent } from './loader/loader.component';
import { MaterialModule } from "../material/material.module";
// import { MaterialModule } from '@angular/material';
import { TokenService } from './token.service';


@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    MaterialModule
  ],
  exports: [
    LoaderComponent
  ],
  declarations: [LoaderComponent],
  providers: [
    TokenService,
    LoaderService,
    {
      provide: Http,
      useFactory: httpFactory,
      deps: [XHRBackend, RequestOptions, LoaderService, TokenService]
    }
  ]
})
export class HttpInterceptorModule {

}
