import { Injectable } from '@angular/core';
import { User } from '../authentication/user';
import { JwtHelper } from 'angular2-jwt';

@Injectable()
export class TokenService {

  public loggedinUser: User;
  public token: string;
  jwtHelper: JwtHelper = new JwtHelper();


  constructor() { }

  getToken() {
    const token = localStorage.getItem('token');
    if (token) {
      this.token = JSON.parse(token);
    }
    return this.token;
  }

  getLoggedInUser() {
    const user = localStorage.getItem('userDetails');
    if (user) {
      this.loggedinUser = JSON.parse(user);
    }
    return this.loggedinUser;
  }

  logout(): boolean {
    // clear token remove user from local storage to log user out
    this.token = null;
    localStorage.removeItem('token');
    localStorage.removeItem('userDetails');
    return true;
  }

  getExpirationTime(): any {
    
    return this.jwtHelper.getTokenExpirationDate(this.getToken());
  }

  setUserDetails(user) {
    localStorage.setItem('userDetails', JSON.stringify(user));
    this.getLoggedInUser();
  }

  setToken(token) {
    localStorage.setItem('token', JSON.stringify(token));
  }

  removeToken() {
    localStorage.removeItem('token');
  }
  removeUserDetails() {
    localStorage.removeItem('userDetails');
  }

  getWorkspaceId() {
    return localStorage.getItem('wsId');
  }

}
