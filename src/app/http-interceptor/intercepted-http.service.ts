import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { ConnectionBackend, RequestOptions, Request, RequestOptionsArgs, Response, Http, Headers } from '@angular/http';
import { environment } from '../../environments/environment';
import { AuthGuard } from '../authentication/guards/auth.guard';
import { LoaderService } from './loader/loader.service';
import { TokenService } from './token.service';
import { Router, ActivatedRoute } from '@angular/router';


@Injectable()
export class InterceptedHttpService extends Http {

    private router: Router;

    constructor(backend: ConnectionBackend,
        defaultOptions: RequestOptions,
        private loaderService: LoaderService,
        private _tokenService: TokenService,
        private _router: Router
    ) {
        super(backend, defaultOptions);
    }

    request(url: string | Request, options?: RequestOptionsArgs): Observable<Response> {
        return super.request(url, options);
    }

    get(url: string, options?: RequestOptionsArgs): Observable<any> {
        this.showLoader();
        return super.get(this.updateUrl(url), this.getRequestOptionArgs(options))
            .catch(this.onCatch)
            .do((res: Response) => {
                this.onSuccess(res);
            }, (error: any) => {
                this.onError(error);
            })
            .finally(() => {
                this.onEnd();
            });

    }

    post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        this.showLoader();
        if (!url.startsWith('http')) {
            url = this.updateUrl(url);

            return super.post(url, body, this.getRequestOptionArgs(options))
                .catch(this.onCatch)
                .do((res: Response) => {
                    this.onSuccess(res);
                }, (error: any) => {
                    this.onError(error);
                })
                .finally(() => {
                    this.onEnd();
                });
        } else {
            return super.post(url, body, options)
                .catch(this.onCatch)
                .do((res: Response) => {
                    this.onSuccess(res);
                }, (error: any) => {
                    this.onError(error);
                })
                .finally(() => {
                    this.onEnd();
                });
        }
    }

    put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response> {
        url = this.updateUrl(url);
        return super.put(url, body, this.getRequestOptionArgs(options));
    }

    delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
        url = this.updateUrl(url);
        return super.delete(url, this.getRequestOptionArgs(options));
    }

    private updateUrl(req: string) {
        return environment.apiURL + req;
    }

    private getRequestOptionArgs(options?: RequestOptionsArgs): RequestOptionsArgs {
        if (options == null) {
            options = new RequestOptions();
        }
        if (options.headers == null) {
            options.headers = new Headers();
        }
        options.headers.append('Content-Type', 'application/json');
        if (this._tokenService.getToken()) {
            const token = this._tokenService.getToken();
            options.headers.append('x-access-token', token);
        }
        const wsId = this.getWsId();
        if (wsId) {
            options.headers.append('x-wsId', wsId);
        }

        return options;
    }

    private onSuccess(res: Response): void {
        // console.log('Request successful');
    }

    private onError(res: Response): void {
        // commented by Yash!
        if (res.status === 401) {
          window.location.href = environment.unAuthorizedRedirectURL;
        }
    }

    private onEnd(): void {
        this.hideLoader();
    }

    private showLoader(): void {
        this.loaderService.show();
    }

    private hideLoader(): void {
        this.loaderService.hide();
    }
    private onCatch(error: any, caught: Observable<any>): Observable<any> {
        return Observable.throw(error);
    }

    private getWsId() {
        return localStorage.getItem('wsId');
    }

}
