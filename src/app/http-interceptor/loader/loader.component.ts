import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { LoaderService } from './loader.service';
import { LoaderState } from './loader';

@Component({
    selector: 'app-loader',
    templateUrl: 'loader.component.html',
    styleUrls: ['loader.component.css']
})
export class LoaderComponent implements OnInit, OnDestroy {

    show = false;

    private subscription: Subscription;

    constructor(
        private loaderService: LoaderService,
        private cdRef: ChangeDetectorRef
    ) { }

    ngOnInit() {
        this.subscription = this.loaderService.loaderState
            .subscribe((state: LoaderState) => {
               // this.show = state.show;
                setTimeout(() => this.show = state.show);
                this.cdRef.detectChanges();
            });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
