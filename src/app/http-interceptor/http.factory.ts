import { XHRBackend, Http, RequestOptions } from '@angular/http';
import { InterceptedHttpService } from './intercepted-http.service';
import { LoaderService } from './loader/loader.service';
import { TokenService } from './token.service';
import { Router, ActivatedRoute } from '@angular/router';

export function httpFactory(xhrBackend: XHRBackend,
    requestOptions: RequestOptions,
    loaderservice: LoaderService,
    _tokenService: TokenService,
     _router: Router): Http {
    return new InterceptedHttpService(xhrBackend, requestOptions, loaderservice, _tokenService,_router);
}
