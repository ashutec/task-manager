import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { appRouting } from './app.routing';
import { AuthenticationModule } from './authentication/authentication.module';
import { HttpInterceptorModule } from './http-interceptor/http-interceptor.module';
import { WorkspaceModule } from './workspace/workspace.module';
import { AuthGuard } from './authentication/guards/auth.guard';
import { RouterModule } from '@angular/router';
import { ServiceWorkerModule } from '@angular/service-worker'
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
  ],

  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AuthenticationModule.forRoot(),
    RouterModule,
    appRouting,
    HttpInterceptorModule,
    WorkspaceModule,
    environment.production ? ServiceWorkerModule.register('/ngsw-worker.js') : []
  ],
  providers: [
    AuthGuard,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
