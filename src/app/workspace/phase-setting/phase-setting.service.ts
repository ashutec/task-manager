import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Phase } from '../../task/phase/phase';
import { TaskerService } from '../../task/task-manager/tasker.service';
import { Workspace } from "../workspace/workspace";
import 'rxjs/add/operator/map';

@Injectable()
export class PhaseSettingsService {

    constructor(private _http: Http, private _taskerService: TaskerService) { }

    getWorkSpace(wsId): Observable<Workspace> {
        return this._taskerService.getSingleWorkSpace(wsId);
    }

    savePhase(phase) {
        let newPhaseData = new Phase();
        newPhaseData._id = phase._id;
        newPhaseData.allowedActions = phase.allowedActions;
        newPhaseData.canAddTask = phase.canAddTask;
        newPhaseData.isEndOfProcess = phase.isEndOfProcess;
        newPhaseData.name = phase.name;
        let body = {
            phase: newPhaseData,
            phaseId: phase._id
        }
        return this._http.put('/phase', body).map(res => res.json())
    }


}