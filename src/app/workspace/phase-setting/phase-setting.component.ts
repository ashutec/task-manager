import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PhaseSettingsService } from './phase-setting.service';
import { Phase } from "../../task/phase/phase";
import { Workspace } from '../workspace/workspace';
import { Router } from '@angular/router';
import { AuthorizationService } from "../../shared/Authorization.service";
import { Privilages } from "../../shared/privilages.enum";
import { ToastrService } from '../../task/shared/toaster.service';

@Component({
  selector: 'app-phase-setting',
  templateUrl: './phase-setting.component.html',
  styleUrls: ['./phase-setting.component.css'],
  providers: [PhaseSettingsService],
})
export class PhaseSettingComponent implements OnInit {
  loading: Boolean = true;
  currentPhase: Phase = new Phase();
  workspace: Workspace = new Workspace();
  allphases: Phase[] = new Array<Phase>();
  phasePos;
  workspacePhases;
  public privilages = Privilages;

  constructor(private _activatedRoute: ActivatedRoute,
    private _phaseSettingsService: PhaseSettingsService,
    private _router: Router,
    public _toastr: ToastrService,
    public _authorizationService: AuthorizationService,
  ) { }

  ngOnInit() {
    // if (this._authorizationService.userCan(this.privilages.CAN_EDIT_PHASE)) {
      this._activatedRoute.params.subscribe(res => this.currentPhase._id = res['Id']);
      this._activatedRoute.parent.params.map(res => this.workspace._id = res['wsId'])
        .flatMap(() => this._phaseSettingsService.getWorkSpace(this.workspace._id))
        .map(workspace => this.workspace = workspace)
        .map(() => this.checkAccessRights())
        .map(() => this.currentPhase._id = this.currentPhase._id ? this.currentPhase._id : this.workspace.phases[0]._id)
        .map(() => this.allphases = this.workspace.phases.slice(0))
        .map(() => { this.currentPhase = this.workspace.phases.find(phase => phase._id == this.currentPhase._id) })
        .map(() => this.getActionValues())
        .subscribe(() => { this.loading = false; }, err => this.redirectToWorkspace());
    // } else {
    //   this._router.navigate(['/workspace']);
    //   this._toastr.showError(this._authorizationService.errorMessage);
    // }
  }

  checkAccessRights() {
    if (!this._authorizationService.userCan(this.privilages.CAN_DELETE_MEMBER, this.privilages.CAN_ADD_MEMBER)) {
      throw new Error(this._authorizationService.errorMessage);
    }
  }

  redirectToWorkspace() {
    this._router.navigate(['/workspace']);
    this._toastr.showError(this._authorizationService.errorMessage);
  }

  getActionValues() {
    this.workspace.phases.map(phase => {
      (<any>phase).isChecked = this.currentPhase.allowedActions.includes(phase._id);
      return phase;
    });
  }

  onAction(phase) {
    if (phase.isChecked)
      this.currentPhase.allowedActions.push(phase._id);
    else
      this.currentPhase.allowedActions.splice(this.currentPhase.allowedActions.indexOf(phase._id), 1);
  }

  savePhase() {
    this._phaseSettingsService.savePhase(this.currentPhase).subscribe(() => {
      this._router.navigate(['/workspace', this.workspace._id]);
    });
  }

  cancelSettings() {
    this._router.navigate(['/workspace', this.workspace._id]);
  }

}
