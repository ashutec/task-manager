import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhaseSettingComponent } from './phase-setting.component';

describe('PhaseSettingComponent', () => {
  let component: PhaseSettingComponent;
  let fixture: ComponentFixture<PhaseSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhaseSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhaseSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
