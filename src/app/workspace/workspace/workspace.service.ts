import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/rx';
import { Workspace } from './workspace';

@Injectable()
export class WorkspaceService {

  constructor(private http: Http) { }

  public addWorkspace(workspace: Workspace) {
    return this.http.put('/workspace', workspace).map(result => result.json())
  }

  public getWorkspaces() {
    return this.http.get('/workspace/getws').map(result => result.json())
  }

  public deleteWorkspace(workspace: Workspace) {
    const body = { wsId: workspace._id };
    const options = new RequestOptions({ body });
    return this.http.delete('/workspace/deleteWorkspace', options)
  }

  updateWorkspace(workspace: Workspace) {
    return this.http.put('/workspace/updateWorkspace', workspace).map((response: Response) => {
      const upadetedWS = response.json() && response.json().data;
      return upadetedWS;
    })
  }

  getSingleWorkspace(wsId) {
    return this.http.get('/workspace/getws/' + wsId).map(result => result.json())
  }


}
