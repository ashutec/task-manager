import { Phase } from '../../task/phase/phase';

export class Workspace {
    _id: string;
    wsname: string;
    userid: String;
    phases: Phase[];
    defaultPhase: number;
    watchers: [string];
    member: [{
        user: object,
        role: object | string,
    }]
}
