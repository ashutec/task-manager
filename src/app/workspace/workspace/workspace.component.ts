import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { WorkspaceService } from './workspace.service'
import { Workspace } from './workspace'
import { Router, ActivatedRoute } from '@angular/router';
import { AlertBoxComponent } from '../../task/alert-box/alert-box.component'
import { ToastrService } from '../../task/shared/toaster.service'
import { TokenService } from '../../http-interceptor/token.service';
import { AuthorizationService } from '../../shared/Authorization.service';
import { Privilages } from '../../shared/privilages.enum'

@Component({
  selector: 'app-workspace',
  templateUrl: './workspace.component.html',
  styleUrls: ['./workspace.component.css']
})

export class WorkspaceComponent implements OnInit {

  dialogRefDelete: MatDialogRef<AlertBoxComponent>;
  // phases = [
  //   { name: 'open', checked: true, disable: true },
  //   { name: 'in process', checked: true },
  //   { name: 'closed', checked: true }
  // ];
  workspacename = 'Task List';
  isnew = true;
  workspace: Workspace = new Workspace();
  Workspaces: Workspace[] = [];
  selectedPhases: String[] = [];
  isEdit = false;
  editWorkspace: Workspace = new Workspace();
  updatedWorkspaceName;
  loading = false;
  public privilages = Privilages;

  constructor(
    private _model: MatDialog,
    private _workspaceService: WorkspaceService,
    private dialog: MatDialog,
    private router: Router,
    private _activatedRoute: ActivatedRoute,
    private toaster: ToastrService,
    private _tokenService: TokenService,
    private _authorizationService: AuthorizationService,
  ) { }

  ngOnInit() {
    this.loading = true;
    this._workspaceService.getWorkspaces().subscribe(result => {
      this.Workspaces = result;

      console.log(this.Workspaces);
      this.Workspaces.map(this.isUserFollowing.bind(this));
      this.loading = false;
    });
  }

  isUserFollowing(workspace) {
    workspace.isFollowing = workspace.watchers.includes(this._tokenService.getLoggedInUser().id);
    return workspace;
  }

  addWorkspace(): void {
    this._workspaceService.addWorkspace(this.workspace).subscribe(result => this.Workspaces.push(result));
    this.workspace = new Workspace();
    this.selectedPhases = [];
    this.isnew = true;
  }

  deleteWorkspace(currentWorkspace): void {
    if (this._authorizationService.userCan(this.privilages.CAN_DELETE_WORKSPACE)) {
      const dialogRefDelete = this.dialog.open(AlertBoxComponent, { width: '500px' });
      dialogRefDelete.afterClosed().subscribe((result: string) => {
        if (result) {
          this.Workspaces.splice(this.Workspaces.indexOf(currentWorkspace), 1);
          this._workspaceService.deleteWorkspace(currentWorkspace).subscribe();
        }
      });
    } else {
      this.toaster.showError(this._authorizationService.errorMessage);
    }
  }

  openWorkSpace(workSpace) {
    this.router.navigate(['/workspace/', workSpace._id]);
  }

  ediWorkspace(workSpace) {
    this.isEdit = true;
    this.editWorkspace = workSpace;
  }

  updateWorkspace() {
    this.editWorkspace.wsname = this.updatedWorkspaceName;
    this._workspaceService.updateWorkspace(this.editWorkspace).subscribe((result: Workspace) => {
      this.editWorkspace = result;
      this.isEdit = false;
      this.toaster.showSuccess('workspace updated!!');
    }, err => {
      this.toaster.showError('There is some issue in update');
    })
  }

  onChange(value) {
    this.updatedWorkspaceName = value;
  }

}
