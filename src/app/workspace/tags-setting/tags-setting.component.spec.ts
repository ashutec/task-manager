import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TagsSettingComponent } from './tags-setting.component';

describe('TagsSettingComponent', () => {
  let component: TagsSettingComponent;
  let fixture: ComponentFixture<TagsSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagsSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagsSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
