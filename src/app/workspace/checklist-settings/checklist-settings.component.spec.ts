import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChecklistSettingsComponent } from './checklist-settings.component';

describe('ChecklistSettingsComponent', () => {
  let component: ChecklistSettingsComponent;
  let fixture: ComponentFixture<ChecklistSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChecklistSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChecklistSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
