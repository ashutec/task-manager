import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { CustomField } from '../custom-field-settings/CustomField';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CustomFieldTypes } from '../custom-field-settings/customFieldTypes';
import { DragulaService } from 'ng2-dragula';

@Component({
  selector: 'app-checklist-settings',
  templateUrl: './checklist-settings.component.html',
  styleUrls: ['./checklist-settings.component.css']
})
export class ChecklistSettingsComponent implements OnInit {

  options = new Array<string>();
  customField: CustomField;
  inputText = true;

  constructor(private _dragulaService: DragulaService,
    private _matDialogRef: MatDialogRef<ChecklistSettingsComponent>,
    @Inject(MAT_DIALOG_DATA) private field) { }

  ngOnInit() {
    this._dragulaService.setOptions('optionbag', {
      moves: function (el, container, handle) {
        if (handle.className.indexOf('handle') !== -1) {
          return true;
        }
      },
    });
    this.customField = new CustomField();
    if (this.field) {
      this.customField = Object.assign({}, this.field);
    } else {
      this.customField.name = 'Check List Name';
      this.customField.isHelpTextEnabled = false;
      this.customField.description = 'Describe the purpose of this field. When and why to use it.';
      this.customField.helpText = 'Hint text about the field for user.';
      this.customField.type = CustomFieldTypes.CheckList;
      this.customField.options = [];
      this.customField.options.push(this.createNewObject('Sample Option'));
    }
  }

  removeOption(index) {
    this.customField.options.splice(index, 1);
  }

  addOption(index) {
    this.customField.options.push(this.createNewObject('Option' + (index + 1)));
  }

  ngOnDestroy() {
    if (this._dragulaService.find('optionbag') !== undefined) {
      this._dragulaService.destroy('optionbag');
    }
  }

  createNewObject(value) {
    return {
      id: Date.now(),
      value,
      isChecked: false
    }
  }

  saveField() {
    this._matDialogRef.close(this.customField);
  }

}
