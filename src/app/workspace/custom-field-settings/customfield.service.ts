import { Injectable } from '@angular/core';
import { Http, Response, RequestOptionsArgs } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';
import { CustomField } from '../custom-field-settings/CustomField';

@Injectable()
export class CustomFieldService {

  allFields = new BehaviorSubject<CustomField[]>(new Array<CustomField>());
  constructor(private http: Http) { }

  getFields(wsId) {
    return this.http.get('/customfields/' + wsId).map(res => res.json())
      .map(allFields => {
        this.allFields.next(allFields);
        return allFields;
      });
  }

  saveCustomField(customField: CustomField, wsId: string) {
    const fieldObj = {
      wsId,
      customField
    }
    return this.http.post('/customfields', fieldObj).map(res => res.json())
      .map(allFields => this.allFields.next(allFields));
  }

  updateCustomField(customField: CustomField, wsId: string) {
    const updatedField = {
      name: customField.name,
      description: customField.description,
      helpText: customField.helpText,
      isHelpTextEnabled: customField.isHelpTextEnabled,
      minLength: customField.minLength,
      maxLength: customField.maxLength,
      isRequired: customField.isRequired,
      customError: customField.customError,
      options: customField.options,
      isRequiredInTask: customField.isRequiredInTask,
    }
    const fieldObj = {
      wsId,
      customField: updatedField,
      customFieldId: customField._id
    }
    return this.http.put('/customfields', fieldObj).map(res => res.json())
      .map(allFields => this.allFields.next(allFields));
  }

  deleteCustomField(customFieldId, wsId) {
    const body: RequestOptionsArgs = {
      body: {
        wsId,
        customFieldId
      }
    }
    return this.http.delete('/customfields', body).map(res => res.json())
      .map(allFields => this.allFields.next(allFields));
  }

}
