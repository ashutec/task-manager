export const CustomFieldTypes = {
    Text: 'Short Text',
    TextArea: 'Multi Line Text',
    Dropdown: 'Dropdown',
    CheckList: 'Check List',
};
