import { Component, OnInit, ViewChild, ViewContainerRef, AfterContentInit, ElementRef, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CustomFieldService } from './customfield.service';
import { CustomField } from './CustomField';
import { CustomFieldTypes } from './customFieldTypes';
import { InlineTextComponent } from '../../task/inline-text/inline-text.component';
import { DragulaService } from 'ng2-dragula';
import { TextBoxSettingsComponent } from '../text-box-settings/text-box-settings.component'
import { TextAreaSettingsComponent } from '../text-area-settings/text-area-settings.component';
import { DropdownSettingsComponent } from '../dropdown-settings/dropdown-settings.component';
import { ChecklistSettingsComponent } from '../checklist-settings/checklist-settings.component';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';
import { AlertBoxComponent } from '../../task/alert-box/alert-box.component';

@Component({
  selector: 'app-custom-field-settings',
  templateUrl: './custom-field-settings.component.html',
  styleUrls: ['./custom-field-settings.component.css'],
})
export class CustomFieldSettingsComponent implements OnInit, AfterContentInit, OnDestroy {

  fields: Array<CustomField> = [];
  config = new MatDialogConfig();
  wsId = '';
  dialogref: MatDialogRef<AlertBoxComponent>;

  constructor(private _activatedRouter: ActivatedRoute,
    private dragulaService: DragulaService,
    private _dialog: MatDialog,
    private _customFieldService: CustomFieldService,
    private elRef: ElementRef
  ) { }

  ngOnInit() {
    this.config.height = '700px';
    this.config.width = '1000px';
    this._activatedRouter.parent.params
      .map(res => {
        this.wsId = res['wsId'];
      }).flatMap(() => this._customFieldService.getFields(this.wsId))
      .subscribe();
    this._customFieldService.allFields.subscribe(allFields => this.fields = allFields);
  }

  ngAfterContentInit() {
    this.dragulaService.setOptions('fieldBag', {
      copy: true,
      invalid: function (el, handle) {
        if (el.className.indexOf('no-drag') === -1) {
          return false;
        }
        return true;
      },
    });

    this.dragulaService.drop.subscribe(value => {
      value[1].className += ' no-drag newelem';
      if (value[1].id === 'textbox') {
        this.newTextBox();
      } else if (value[1].id === 'textarea') {
        this.newTextArea();
      } else if (value[1].id === 'dropdown') {
        this.newDropDown();
      } else if (value[1].id === 'checklist') {
        this.newCheckList();
      }
    });
  }

  newTextBox(field?: CustomField) {
    if (field) {
      this.config.data = field;
    }
    const dialogRef = this._dialog.open(TextBoxSettingsComponent, this.config);
    this.dialogClose(dialogRef);
  }

  newTextArea(field?: CustomField) {
    if (field) {
      this.config.data = field;
    }
    const dialogRef = this._dialog.open(TextAreaSettingsComponent, this.config);
    this.dialogClose(dialogRef);
  }

  newDropDown(field?: CustomField) {
    if (field) {
      this.config.data = field;
    }
    const dialogRef = this._dialog.open(DropdownSettingsComponent, this.config);
    this.dialogClose(dialogRef);
  }

  newCheckList(field?: CustomField) {
    if (field) {
      this.config.data = field;
    }
    const dialogRef = this._dialog.open(ChecklistSettingsComponent, this.config);
    this.dialogClose(dialogRef);
  }

  dialogClose(dialogRef: any) {
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.saveData(result);
      }
      const el: HTMLElement = this.elRef.nativeElement.querySelector('.newelem')
      if (el) {
        el.parentNode.removeChild(el);
      }
      this.config.data = null;
    });
  }

  saveData(customField: CustomField) {
    if (customField._id) {
      this._customFieldService.updateCustomField(customField, this.wsId).subscribe();
    } else {
      this._customFieldService.saveCustomField(customField, this.wsId).subscribe();
    }
  }

  deleteCustomField(fieldId, ev) {
    if (ev) {
      ev.stopPropagation();
    }
    this.dialogref = this._dialog.open(AlertBoxComponent, { width: '500px' });
    this.dialogref.afterClosed().subscribe(res => {
      if (res) {
        this._customFieldService.deleteCustomField(fieldId, this.wsId).subscribe();
      }
    })
  }

  openCustomField(field: CustomField) {
    if (field.type === CustomFieldTypes.Text) {
      this.newTextBox(field);
    } else if (field.type === CustomFieldTypes.TextArea) {
      this.newTextArea(field);
    } else if (field.type === CustomFieldTypes.Dropdown) {
      this.newDropDown(field);
    } else if (field.type === CustomFieldTypes.CheckList) {
      this.newCheckList(field);
    }
  }

  ngOnDestroy() {
    if (this.dragulaService.find('fieldBag') !== undefined) {
      this.dragulaService.destroy('fieldBag');
    }
  }

}
