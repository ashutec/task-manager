export class CustomField {
    _id?: string;
    name: string;
    type: string;
    description: string;
    helpText: string;
    isHelpTextEnabled = false;
    minLength: number;
    maxLength: number;
    isRequired = false;
    customError: string;
    options: any;
    wsId: string;
    isRequiredInTask = false;
    value?: any;
};
