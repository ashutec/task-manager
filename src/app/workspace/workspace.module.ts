import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkspaceComponent } from './workspace/workspace.component';
import { WorkspaceRoutes } from './workspace.routing';
import { WorkspaceService } from './workspace/workspace.service';
import { HomeComponent } from './home/home.component'
import { MaterialModule } from '../material/material.module';
import { PopoverModule } from 'ngx-popover';
// import { MaterialModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { TaskModule } from '../task/task.module';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { SharedModule } from '../shared/shared.module'
import { PhaseSettingComponent } from './phase-setting/phase-setting.component';
import { RouterModule } from '@angular/router';
import { SettingsComponent } from './settings/settings.component';
import { GeneralSettingComponent } from './general-setting/general-setting.component';
import { TagsSettingComponent } from './tags-setting/tags-setting.component';
import { MembersSettingComponent } from './members-setting/members-setting.component';
import { MemberCardComponent } from './member-card/member-card.component';
import { MemberSettingsService } from './members-setting/members-setting.service';
import { CustomFieldSettingsComponent } from './custom-field-settings/custom-field-settings.component';
import { DragulaModule } from 'ng2-dragula';
import { TextBoxSettingsComponent } from './text-box-settings/text-box-settings.component';
import { TextAreaSettingsComponent } from './text-area-settings/text-area-settings.component';
import { DropdownSettingsComponent } from './dropdown-settings/dropdown-settings.component';
import { CustomFieldService } from './custom-field-settings/customfield.service';
import { ChecklistSettingsComponent } from './checklist-settings/checklist-settings.component'

@NgModule({
  imports: [
    CommonModule,
    WorkspaceRoutes,
    MaterialModule,
    FormsModule,
    RouterModule,
    ToasterModule,
    DragulaModule,
    PopoverModule,
    TaskModule,
    SharedModule
  ],

  declarations: [
    WorkspaceComponent,
    HomeComponent,
    PhaseSettingComponent,
    SettingsComponent,
    GeneralSettingComponent,
    TagsSettingComponent,
    MembersSettingComponent,
    MemberCardComponent,
    CustomFieldSettingsComponent,
    TextBoxSettingsComponent,
    TextAreaSettingsComponent,
    DropdownSettingsComponent,
    ChecklistSettingsComponent
  ],

  providers: [
    WorkspaceService,
    ToasterService,
    MemberSettingsService,
    CustomFieldService
  ],

  entryComponents: [TextBoxSettingsComponent, TextAreaSettingsComponent, DropdownSettingsComponent, ChecklistSettingsComponent],

  exports: [
    HomeComponent
  ]
})
export class WorkspaceModule { }
