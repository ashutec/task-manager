import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthorizationService } from '../../shared/Authorization.service';
import { Privilages } from '../../shared/privilages.enum';
import { ToastrService } from '../../task/shared/toaster.service';
import { MemberSettingsService } from './members-setting.service';
import { Workspace } from '../workspace/workspace';
import { AlertBoxComponent } from '../../task/alert-box/alert-box.component';
import { MatDialogRef, MatDialog, MatSnackBar, MatDialogConfig } from '@angular/material';
import { InviteMembersComponent } from '../../task/invite-members/invite-members.component';
import { TaskerService } from '../../task/task-manager/tasker.service';
import { MemberService } from '../../task/invite-members/member.service';
@Component({
  selector: 'app-members-setting',
  templateUrl: './members-setting.component.html',
  styleUrls: ['./members-setting.component.css'],
})

export class MembersSettingComponent implements OnInit {
  public loading: Boolean = true;
  public privilages = Privilages;
  workspace: Workspace = new Workspace();
  roles;
  members;
  deleteDialogRef: MatDialogRef<AlertBoxComponent>
  inviteMemberDialogRef: MatDialogRef<InviteMembersComponent>
  private _taskerService: TaskerService;
  constructor(private _activatedRoute: ActivatedRoute,
    private _memberService: MemberService,
    private _router: Router,
    private _memberSettingsService: MemberSettingsService,
    public _toastr: ToastrService,
    private _dialog: MatDialog,
    public _authorizationService: AuthorizationService) {
  }

  ngOnInit() {
    this._activatedRoute.parent.params.map(res => this.workspace._id = res['wsId'])
      .flatMap(() => this._memberSettingsService.getWorkspace(this.workspace._id))
      .map(workspace => this.workspace = workspace)
      .map(() => this.checkAccessRights())
      .flatMap(() => this._memberSettingsService.getRoles())
      .map(roles => this.roles = roles)
      .map(() => this.members = this.workspace.member)
      .subscribe(() => { this.loading = false; }, err => this.redirectToWorkspace());

      // this._taskerService.workspace.subscribe(workspace => {
      //   this.workspace = workspace;
      //   this._memberService.newWsUser(workspace.member);
      // });
  }

  inviteMember() {
    const config = new MatDialogConfig();
    const dialogRef = this._dialog.open(InviteMembersComponent, { height: '400px', width: '600px' });
    dialogRef.afterClosed().subscribe(result => {
      this._router.navigate(['/workspace',this.workspace._id]);
    });
  }

  checkAccessRights() {
    if (!this._authorizationService.userCan(this.privilages.CAN_DELETE_MEMBER, this.privilages.CAN_ADD_MEMBER)) {
      throw new Error(this._authorizationService.errorMessage);
    }
  }

  redirectToWorkspace() {
    this._router.navigate(['/workspace']);
    this._toastr.showError(this._authorizationService.errorMessage);
  }

  RoleUpdated(roleData) {
    this._memberSettingsService.updateRole(this.workspace._id, roleData.member, roleData.updatedRole._id)
      .map(workSpace => this.workspace = workSpace)
      .subscribe(() => this.members = this.workspace.member, err => this.redirectToWorkspace());
  }

  deleteMember(userId) {
    this.deleteDialogRef = this._dialog.open(AlertBoxComponent, { width: '500px' });
    this.deleteDialogRef.afterClosed().subscribe(res => {
      if (res) {
        this._memberSettingsService.deleteMember(this.workspace._id, userId).subscribe(workspace => {
          this.workspace = workspace;
          this.members = workspace.member;
        });
      }
    });
  }

}
