import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MembersSettingComponent } from './members-setting.component';

describe('MembersSettingComponent', () => {
  let component: MembersSettingComponent;
  let fixture: ComponentFixture<MembersSettingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MembersSettingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MembersSettingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
