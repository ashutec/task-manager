import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { WorkspaceService } from '../workspace/workspace.service';
import { RoleService } from '../../shared/roles.service';
import { MemberService } from '../../task/invite-members/member.service';
import { TaskerService } from '../../task/task-manager/tasker.service';

@Injectable()
export class MemberSettingsService {

    constructor(private _workspaceService: WorkspaceService, private _roleService: RoleService,
        private _http: Http, private _memberService: MemberService, private _taskerService:TaskerService) { }

    getWorkspace(wsId) {
        return this._taskerService.getSingleWorkSpace(wsId);
    }

    getRoles() {
        return this._roleService.getRoles();
    }

    updateRole(wsId, userId, roleId) {
        return this._http.post('/workspace/updateUserRole', { wsId, userId, roleId }).map(res => res.json());
    }

    deleteMember(wsId, userId) {
        return this._memberService.deleteMember(wsId, userId);
    }

}