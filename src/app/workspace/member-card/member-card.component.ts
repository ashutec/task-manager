import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { TokenService } from '../../http-interceptor/token.service';

@Component({
  selector: 'member-card',
  templateUrl: './member-card.component.html',
  styleUrls: ['./member-card.component.css']
})
export class MemberCardComponent implements OnInit {

  @Input('roles') roles;
  @Input('member') member;
  @Output('updatedRole') updatedRole = new EventEmitter();
  @Output('deleteMember') deleteMember = new EventEmitter();
  RoleEdit = false;
  selectedRole;
  showDeleteButton: boolean;

  constructor(private _tokenService: TokenService) { }

  ngOnInit() {
    this.selectedRole = this.member.role;
    this.showDeleteButton = this.member.user._id != this._tokenService.getLoggedInUser().id;
  }

  save() {
    let obj = {
      member: this.member.user._id,
      updatedRole: this.selectedRole
    };
    this.updatedRole.emit(obj);
  }

  delMember() {
    this.deleteMember.emit(this.member.user._id);
  }

}
