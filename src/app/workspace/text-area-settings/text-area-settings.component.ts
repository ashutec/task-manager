import { Component, OnInit, Inject } from '@angular/core';
import { CustomField } from '../custom-field-settings/CustomField';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { CustomFieldTypes } from '../custom-field-settings/customFieldTypes';

@Component({
  selector: 'app-text-area-settings',
  templateUrl: './text-area-settings.component.html',
  styleUrls: ['./text-area-settings.component.css']
})
export class TextAreaSettingsComponent implements OnInit {
  customField: CustomField;
  inputText = true;
  textValue = 'Testing some really really really really really really really really really really really really really long text!!';
  constructor(private _matDialogRef: MatDialogRef<TextAreaSettingsComponent>,
    @Inject(MAT_DIALOG_DATA) private field) { }

  ngOnInit() {
    this.customField = new CustomField();
    if (this.field) {
      this.customField = Object.assign({}, this.field);
    } else {
      this.customField.name = 'Your Field Name';
      this.customField.isRequired = false;
      this.customField.isHelpTextEnabled = false;
      this.customField.description = 'Describe the purpose of this field. When and why to use it.';
      this.customField.helpText = 'Hint text about the field for user.';
      this.customField.type = CustomFieldTypes.TextArea;
    }
  }

  saveField() {
    this._matDialogRef.close(this.customField);
  }

}
