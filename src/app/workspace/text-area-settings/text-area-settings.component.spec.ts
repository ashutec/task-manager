import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextAreaSettingsComponent } from './text-area-settings.component';

describe('TextAreaSettingsComponent', () => {
  let component: TextAreaSettingsComponent;
  let fixture: ComponentFixture<TextAreaSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextAreaSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextAreaSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
