import { Component, OnInit } from '@angular/core';
import { ToasterConfig } from 'angular2-toaster/angular2-toaster';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { SocketActionService } from '../../shared/SocketAction.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public toasterconfig: ToasterConfig =
  new ToasterConfig({
    limit: 5,
    preventDuplicates: false,
    tapToDismiss: true,
    newestOnTop: true,
    timeout: 8000,
    positionClass: 'toast-top-right',
    animation: 'fade',
    showCloseButton: true,
  });

  constructor(private router: Router,
    private _socketAction: SocketActionService,
    private _activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {

    // this.router.navigate(['/workspace']);
    this.getWsId(this.router.url.toString());
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.getWsId(event.url.toString());
      }
    });
    // this._activatedRoute.params.forEach(res => {
    //   console.log(res);
    // });
  }

  getWsId(url: string) {
    try {
      if (url) {
        let urlArray = url.split('/').filter(url => (url));
        if (urlArray.length > 1) {
          this.saveWsIdtoLocalStorage(urlArray[1]);
        } else {
          this.unsetWsId();
        }
      }
    } catch (err) {
      console.log('Something went wrong while getting workspace Id' + JSON.stringify(err));
    }
  }

  saveWsIdtoLocalStorage(wsId) {
    localStorage.setItem('wsId', wsId);
  }

  unsetWsId() {
    localStorage.removeItem('wsId');
  }

}
