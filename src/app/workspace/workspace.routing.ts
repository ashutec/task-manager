import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TaskManagerComponent } from '../task/task-manager/task-manager.component';
import { WorkspaceComponent } from './workspace/workspace.component';
import { AuthGuard } from '../authentication/guards/auth.guard';
import { HomeComponent } from '../workspace/home/home.component';
import { PhaseSettingComponent } from './phase-setting/phase-setting.component';
import { GeneralSettingComponent } from './general-setting/general-setting.component';
import { TagsSettingComponent } from './tags-setting/tags-setting.component';
import { MembersSettingComponent } from './members-setting/members-setting.component';
import { SettingsComponent } from './settings/settings.component';
import { TasksComponent } from '../task/tasks/tasks.component';
import { CummulativeChartComponent } from '../task/cummulative-chart/cummulative-chart.component';
import { CustomFieldSettingsComponent } from './custom-field-settings/custom-field-settings.component';

export const workspaceRoutes: Routes = [
  {
    path: 'workspace',
    component: HomeComponent, canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: WorkspaceComponent,
        canActivate: [AuthGuard]
      },
      {
        path: ':wsId/analytics',
        component: CummulativeChartComponent,
        canActivate: [AuthGuard]
      },
      {
        path: ':wsId/settings',
        component: SettingsComponent,
        canActivate: [AuthGuard],
        children: [
          {
            path: '',
            redirectTo: 'phase',
            pathMatch: 'full'
          },
          {
            path: 'general',
            component: GeneralSettingComponent,
            canActivate: [AuthGuard],
          },
          {
            path: 'phase/:Id',
            component: PhaseSettingComponent,
            canActivate: [AuthGuard],
          },
          {
            path: 'phase',
            component: PhaseSettingComponent,
            canActivate: [AuthGuard],
          },
          {
            path: 'tags',
            component: TagsSettingComponent,
            canActivate: [AuthGuard],
          },
          {
            path: 'member',
            component: MembersSettingComponent,
            canActivate: [AuthGuard]
          },
          {
            path: 'customfields',
            component: CustomFieldSettingsComponent,
            canActivate: [AuthGuard]
          },
        ],
      },
      {
        path: ':Id',
        children: [
          { path: 'tasks/:taskId', component: TasksComponent }
        ],
        component: TaskManagerComponent,
        canActivate: [AuthGuard],
        data: { name: 'TaskManager' }
      },
    ],
  }
];


export const WorkspaceRoutes: ModuleWithProviders = RouterModule.forChild(workspaceRoutes);
