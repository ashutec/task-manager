import { Injectable, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs/subject';

@Injectable()
export class AuthorizationService {

    public errorMessage = 'You don\'t have necessary privilages to perform this action!';
    private userPrivilages = new Array<any>();
    constructor() { }

    userCan(...privilages): Boolean {
        return privilages.every(privilage => this.userPrivilages.includes(privilage));
    }

    newUserPrivilages(privilages: any[]) {
        this.userPrivilages = privilages
    }

    clearPrivilages() {
        this.userPrivilages = <any>[];
    }

}