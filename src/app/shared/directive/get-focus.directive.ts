import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appGetFocus]'
})
export class GetFocusDirective {

  constructor(el: ElementRef) {
    setTimeout(() => {
      el.nativeElement.focus()
    }, 0);
  }

}
