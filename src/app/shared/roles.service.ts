import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class RoleService {

    constructor(private http: Http) { }

    getRoles(): Observable<any> {
        return this.http.get('/roles').map(res => res.json());
    }

}