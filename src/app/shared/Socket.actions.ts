export const SocketAction = {
    Task_Phase_Move: 'Task_Phase_Move',
    New_Notificaion: 'New_Notificaion',
    New_Task_Added: 'New_Task_Added',
    Task_Field_Updated: 'Task_Field_Updated',
    Task_Deleted: 'Task_Deleted',
}