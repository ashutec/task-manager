export interface Notification {
    action: string,
    data: object,
    message: string
}