import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { SocketService } from "../task/shared/socket.service";
import { SocketAction } from './Socket.actions';
import { TokenService } from '../http-interceptor/token.service';

@Injectable()
export class SocketActionService {

    newNotificaion: Subject<any> = new Subject<any>();
    notifyArray: {} = {};
    constructor(private _socketService: SocketService,
        private _tokenService: TokenService,
    ) {
        this._socketService.start();
        this._socketService.sendMessage('newUser', this._tokenService.getLoggedInUser().id);
        this._socketService.getMessage('newNotification').subscribe(notify => {
            this.takeNotificationAction(notify);
        });
    }

    takeNotificationAction(notify) {
        switch (notify.action) {
            case SocketAction.Task_Phase_Move: {
                (this.notifyArray[SocketAction.Task_Phase_Move])(notify);
                break;
            }
            case SocketAction.New_Notificaion: {
                (this.notifyArray[SocketAction.New_Notificaion])(notify.data);
                break;
            }
            case SocketAction.New_Task_Added: {
                (this.notifyArray[SocketAction.New_Task_Added])(notify.data);
                break;
            }
            case SocketAction.Task_Field_Updated: {
                (this.notifyArray[SocketAction.Task_Field_Updated])(notify.data);
                break;
            }
            case SocketAction.Task_Deleted: {
                (this.notifyArray[SocketAction.Task_Deleted])(notify.data);
                break;
            }
        }
    }

    RegisterNewSubscription(type, method) {
        this.notifyArray[type] = method;
    }
}