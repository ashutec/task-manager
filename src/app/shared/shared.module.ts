import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GetFocusDirective } from './directive/get-focus.directive';
import { SocketActionService } from './SocketAction.service';
import { AuthorizationService } from './Authorization.service';
import { RoleService } from './roles.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    GetFocusDirective
  ],
  providers: [SocketActionService, AuthorizationService, RoleService],
  exports: [
    GetFocusDirective
  ]
})
export class SharedModule { }
