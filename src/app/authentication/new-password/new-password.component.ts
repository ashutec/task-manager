import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';

import { User } from '../user';
import { AuthenticationService } from '../authentication.service';
import { CreatePasswordComponent } from '../create-password/create-password.component';

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.css']
})
export class NewPasswordComponent implements OnInit {

  constructor(private route: ActivatedRoute,
    private authService: AuthenticationService,
    private dialog: MatDialog,
  ) { }

  ngOnInit() {
    const uuid = this.route.snapshot.params['uuid'];
    const config = new MatDialogConfig();
    config.width = '500px';
    this.authService.verifyUUID(uuid).subscribe(result => {
      if (result) {
        config.data = Object.assign({}, result, { isValidUrl: true });
        this.dialog.open(CreatePasswordComponent, config);
      }
    }, err => {
      this.dialog.open(CreatePasswordComponent, config);
      console.log('UUID is not valid');
    });
  }

}
