import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { ImageCropperComponent, CropperSettings, Bounds } from 'ng2-img-cropper';
import { AuthenticationService } from '../authentication.service';
import { User } from '../user';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig } from '@angular/material';
import { TokenService } from '../../http-interceptor/token.service';
import { ToastrService } from '../../task/shared/toaster.service';

@Component({
  selector: 'app-image-uploader',
  templateUrl: './image-uploader.component.html',
  styleUrls: ['./image-uploader.component.css']
})
export class ImageUploaderComponent implements OnInit {
  name: string;
  data1: any;
  cropperSettings1: CropperSettings;
  croppedWidth: number;
  croppedHeight: number;
  isRefreshing: Boolean = false;
  user: User = new User();
  isUpload = false;

  @ViewChild('cropper', undefined) cropper: ImageCropperComponent;

  ngOnInit(): void {
  }

  constructor(private router: Router,
    private _authService: AuthenticationService,
    private tokenService: TokenService,
    private toaster: ToastrService,
    private _dialogRef: MatDialogRef<ImageUploaderComponent>,
    @Inject(MAT_DIALOG_DATA) private data, ) {
    this.name = 'Angular2'
    this.cropperSettings1 = new CropperSettings();
    this.cropperSettings1.width = 200;
    this.cropperSettings1.height = 200;
    this.cropperSettings1.noFileInput = true;
    this.cropperSettings1.croppedWidth = 200;
    this.cropperSettings1.croppedHeight = 200;

    this.cropperSettings1.canvasWidth = 500;
    this.cropperSettings1.canvasHeight = 300;

    this.cropperSettings1.minWidth = 10;
    this.cropperSettings1.minHeight = 10;

    this.cropperSettings1.rounded = false;
    this.cropperSettings1.keepAspect = false;

    this.cropperSettings1.cropperDrawSettings.strokeColor = 'rgba(255,255,255,1)';
    this.cropperSettings1.cropperDrawSettings.strokeWidth = 2;

    this.data1 = {};
    const user = Object.assign({}, this.data);

  }

  cropped(bounds: Bounds) {
    this.croppedHeight = bounds.bottom - bounds.top;
    this.croppedWidth = bounds.right - bounds.left;
  }

  fileChangeListener($event) {
    this.isUpload = true;
    const image: any = new Image();
    const file: File = $event.target.files[0];
    const myReader: FileReader = new FileReader();
    const that = this;
    myReader.onloadend = function (loadEvent: any) {
      image.src = loadEvent.target.result;
      that.cropper.setImage(image);

    };
    myReader.readAsDataURL(file);
  }

  uploadImage() {
    const imageObj = { data: '' };
    imageObj.data = this.data1.image;
    this.isRefreshing = true;
    this._authService.uploadProfile(imageObj).subscribe((data) => {
      this.user = data;
      this.tokenService.setUserDetails(this.user);
      this._dialogRef.close();
      this.toaster.showSuccess('profile picture updated!!');
      this.isRefreshing = false;
    }, err => {
      this.toaster.showError('Sorry!! Picture is not updated.');
    });
  }

  cancelImageUploading() {
    this._dialogRef.close();
  }
}
