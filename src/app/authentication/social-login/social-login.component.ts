import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../user';
import { AuthenticationService } from '../authentication.service';
import { FacebookService, LoginResponse, InitParams, LoginOptions } from 'ng2-facebook-sdk';
import { GoogleApiService } from '../google-api.service';

declare const gapi: any;

@Component({
  selector: 'app-social-login',
  templateUrl: './social-login.component.html',
  styleUrls: ['./social-login.component.css']
})
export class SocialLoginComponent implements OnInit {
  user: User = new User();

  ngOnInit() {
  }



  constructor(private googleApiService: GoogleApiService,
    private authService: AuthenticationService,
    private router: Router,
    private fb: FacebookService,
    ) {

    const params: InitParams = {
      appId: '130747344209125',
      cookie: false,  // enable cookies to allow the server to access
      // the session
      xfbml: true,  // parse social plugins on this page
      version: 'v2.10' // use graph api version 2.10
    };
    fb.init(params);

  }

  onFacebookLoginClick() {
    const options: LoginOptions = {
      scope: 'email',
    };
    this.fb.login(options).then(response => {
      if (response.authResponse) {
        this.authService.loginWithFB(response.authResponse).subscribe(result => {
          if (result) {
            this.router.navigate(['/workspace']);
          }
        }, err => {
          console.log(err);
        })
      }
    }).catch(err => {
      console.log('error in login with fb:' + err);
    });
  }

  onGoogleLoginClick() {
    this.googleApiService.signIn();
  }

}
