// third party and built in library should be import here
import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router, RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { MaterialModule, MatNativeDateModule, MatDatepickerModule } from '@angular/material';
import { MaterialModule } from "../material/material.module";
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { FacebookService } from 'ng2-facebook-sdk';
import { ClientConfig, GoogleApiModule, NG_GAPI_CONFIG } from 'ng-gapi';
import { ImageCropperModule } from 'ng2-img-cropper';


// all the components listed below for this module
import { AuthenticationComponent } from './authentication.component';
import { LocalLoginComponent } from './local-login/local-login.component';
import { LocalSignupComponent } from './local-signup/local-signup.component';
import { ProfilePictureComponent } from './profile-picture/profile-picture.component';
import { SocialLoginComponent } from './social-login/social-login.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { NewPasswordComponent } from './/new-password/new-password.component';
import { CreatePasswordComponent } from './create-password/create-password.component';
import { LoginSignupComponent } from './login-signup/login-signup.component';
import { LoginHistoryComponent } from './login-history/login-history.component';

// all the routes defined in below file
import { AuthRouting } from './auth.routing';

// all the services that are used are defined below
import { AuthenticationService } from './authentication.service';
import { GoogleApiService } from './google-api.service';
import { EqualValidatorDirective } from '../shared/directive/equal-validator.directive';
import { EmailVerificationComponent } from './email-verification/email-verification.component';
import { EmailVerifiedDialogComponent } from './email-verified-dialog/email-verified-dialog.component';
import { ImageUploaderComponent } from './image-uploader/image-uploader.component';
import { LoginHistoryService } from './login-history/login-history.service';
import { UnauthorizedMessageComponent } from './unauthorized-message/unauthorized-message.component';


// TODO: Add your CLIENT_ID
const gapiClientConfig: ClientConfig = {
  clientId: '211547030228-nq4hhr3nk37l4grb4khor98h425vqcc4.apps.googleusercontent.com',
  discoveryDocs: [''],
  scope: ['profile', 'email'].join(' '),
};



@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    AuthRouting,
    ToasterModule,
    ImageCropperModule,
    GoogleApiModule.forRoot({
      provide: NG_GAPI_CONFIG,
      useValue: gapiClientConfig
    }),
  ],
  declarations: [
    AuthenticationComponent,
    LocalLoginComponent,
    LocalSignupComponent,
    ProfilePictureComponent,
    SocialLoginComponent,
    ChangePasswordComponent,
    ForgotPasswordComponent,
    NewPasswordComponent,
    CreatePasswordComponent,
    LoginSignupComponent,
    EqualValidatorDirective,
    EmailVerificationComponent,
    EmailVerifiedDialogComponent,
    ImageUploaderComponent,
    LoginHistoryComponent,
    UnauthorizedMessageComponent,
  ],
  exports: [AuthenticationComponent],
  providers: [
    FacebookService,
    LoginHistoryService
  ],
  entryComponents: [
    ChangePasswordComponent,
    ForgotPasswordComponent,
    CreatePasswordComponent,
    EmailVerifiedDialogComponent,
    ImageUploaderComponent
  ]
})
export class AuthenticationModule {

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AuthenticationModule,
      providers: [AuthenticationService, GoogleApiService]
    }
  }

}
