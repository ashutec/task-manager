import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfilePictureComponent } from './profile-picture/profile-picture.component';
import { AuthenticationComponent } from './authentication.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { NewPasswordComponent } from './new-password/new-password.component';
import { LoginSignupComponent } from './login-signup/login-signup.component';
import { AuthGuard } from '../authentication/guards/auth.guard';
import { LoginHistoryComponent } from './login-history/login-history.component';
import { EmailVerificationComponent } from './email-verification/email-verification.component'
import { ReportComponent} from '../task/report/report.component';
import { UnauthorizedMessageComponent} from '../authentication/unauthorized-message/unauthorized-message.component';

export const userRoutes: Routes = [
  {
    path: 'auth',
    children: [
      { path: '', redirectTo: 'login', pathMatch: 'full' },
      { path: 'login', component: LoginSignupComponent },
      { path: 'profile', component: ProfilePictureComponent, canActivate: [AuthGuard], data: { name: 'Show Back' } },
      { path: 'forgotPassword', component: ForgotPasswordComponent },
      { path: 'loginHistory', component: LoginHistoryComponent, canActivate: [AuthGuard], data: { name: 'Show Back' } },
      { path: 'reset/:uuid', component: NewPasswordComponent },
      { path: 'verify/:uuid', component: EmailVerificationComponent },
      { path: 'report' , component : ReportComponent},
      { path: 'unAuthorized' , component : UnauthorizedMessageComponent}
    ],
    component: AuthenticationComponent
  }
];


export const AuthRouting: ModuleWithProviders = RouterModule.forChild(userRoutes);
