import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-unauthorized-message',
  templateUrl: './unauthorized-message.component.html',
  styleUrls: ['./unauthorized-message.component.css']
})
export class UnauthorizedMessageComponent implements OnInit {

  constructor(private _router:Router) { }

  ngOnInit() {
  }

  public login(){
    localStorage.clear();
    this._router.navigate(['/auth']);
  }

}
