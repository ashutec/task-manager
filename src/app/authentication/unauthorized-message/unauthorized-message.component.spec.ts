import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnauthorizedMessageComponent } from './unauthorized-message.component';

describe('UnauthorizedMessageComponent', () => {
  let component: UnauthorizedMessageComponent;
  let fixture: ComponentFixture<UnauthorizedMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnauthorizedMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnauthorizedMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
