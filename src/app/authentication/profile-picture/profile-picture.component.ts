import { Component, OnInit, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';
import { ToasterConfig } from 'angular2-toaster/angular2-toaster';
import { Subscription } from 'rxjs/Subscription';

import { AuthenticationService } from '../authentication.service';
import { User } from '../user';
import { TokenService } from '../../http-interceptor/token.service';
import { ChangePasswordComponent } from '../change-password/change-password.component';
import { ToastrService } from '../../task/shared/toaster.service';
import { ImageUploaderComponent } from '../image-uploader/image-uploader.component';


@Component({
  selector: 'app-profile-picture',
  templateUrl: './profile-picture.component.html',
  styleUrls: ['./profile-picture.component.css']
})
export class ProfilePictureComponent implements OnInit {
  username: string;
  fileValidate = true;
  profile;
  user: User = new User();
  isRefreshing: Boolean = false;
  fromSignup;


  constructor(
    private router: Router,
    private _authService: AuthenticationService,
    private tokenService: TokenService,
    private dialog: MatDialog,
    private toaster: ToastrService,
  ) {
  }

  ngOnInit() {
    this.isRefreshing = true;
    this._authService.getUser().subscribe(data => {
      this.user = data;
      this._authService.userSubject.subscribe(user => {
        if (user.profile) {
          this.user.profile = user.profile;
        }
      })
      this.username = data.firstname + ' ' + data.lastname;
    }, err => {
      console.log(`some issue: ${err}`);
    }, () => {
      this.isRefreshing = false;
    })
  }

  changePassword() {
    const config = new MatDialogConfig();
    config.width = '500px';
    this.dialog.open(ChangePasswordComponent, config);
  }

  imageUploadDialogue() {
    const config = new MatDialogConfig();
    const user = {};
    config.data = Object.assign({}, { profile: this.user.profile });
    config.width = '500px';
    this.dialog.open(ImageUploaderComponent, config);
  }

  updateProfile() {
    this._authService.updateProfile(this.user).subscribe((data) => {
      this.user = data;
      this.tokenService.setUserDetails(this.user);
      this.username = data.firstname + ' ' + data.lastname;
      this.toaster.showSuccess('profile updated successfully!!');
    }, err => {
      this.toaster.showError('Problem in profile update!!');
    })
  }

}
