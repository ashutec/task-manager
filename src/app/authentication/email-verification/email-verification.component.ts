import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';
import { User } from '../user';
import { AuthenticationService } from '../authentication.service';
import { EmailVerifiedDialogComponent } from '../email-verified-dialog/email-verified-dialog.component'

@Component({
  selector: 'app-email-verification',
  templateUrl: './email-verification.component.html',
  styleUrls: ['./email-verification.component.css']
})
export class EmailVerificationComponent implements OnInit {

  isEmailVerified = false;

  constructor(private route: ActivatedRoute,
    private authService: AuthenticationService,
    private dialog: MatDialog,
    private router: Router) { }

  ngOnInit() {
    const uuid = this.route.snapshot.params['uuid'];
    const config = new MatDialogConfig();
    config.width = '500px';
    this.authService.verifyEmailId(uuid).subscribe((result) => {
      if (result) {
        this.isEmailVerified = result;
        config.data = Object.assign({}, { isEmailVerified: this.isEmailVerified });
        this.dialog.open(EmailVerifiedDialogComponent, config);
      }
    }, (err) => {
      config.data = Object.assign({}, { isEmailVerified: this.isEmailVerified });
      this.dialog.open(EmailVerifiedDialogComponent, config);
      // this.router.navigate(['auth/login']);
    })
  }

}
