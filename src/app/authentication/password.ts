export class Password {
  email: string;
  password: string;
  newPassword: string;
  confirmPassword: string;
}
