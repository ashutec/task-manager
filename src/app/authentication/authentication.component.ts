import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd, Params } from '@angular/router'
import { Location } from '@angular/common';
import { ToasterConfig } from 'angular2-toaster/angular2-toaster';

import { User } from './user';


@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.css']
})

export class AuthenticationComponent implements OnInit {
  backButton = false;
  fromSignup;

  public toasterconfig: ToasterConfig =
  new ToasterConfig({
    limit: 5,
    preventDuplicates: false,
    tapToDismiss: true,
    newestOnTop: true,
    timeout: 3000,
    positionClass: 'toast-bottom-right',
  });

  ngOnInit(): void {
    this.showBackButton();
    this._router.events.filter(route => (route instanceof NavigationEnd)).subscribe((route: NavigationEnd) => {
      this.showBackButton();
    });
    this._activatedRoute.queryParams.subscribe((params: Params) => {
      this.fromSignup = params['signUp'];
    });
  }

  constructor(private _location: Location,
    private _activatedRoute: ActivatedRoute,
  private _router: Router) { }

  goBack() {
    this._location.back();
  }

  showBackButton() {
    this._activatedRoute.children[0].data.subscribe(data => {
      if (data.name === 'Show Back') {
        this.backButton = true;
      } else {
        this.backButton = false
      }
    });
  }

  skip() {
    this._router.navigate(['/workspace']);
  }

}
