export class User {
  public id: string;
  public firstname: string;
  public lastname: string;
  public email: string;
  public password: string;
  public profile: string;
  public confirmPassword: string;
  public isEmailverified: boolean;
}
