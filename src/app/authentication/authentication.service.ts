import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { User } from './user';
import { environment } from '../../environments/environment';
import { TokenService } from '../http-interceptor/token.service';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AuthenticationService {
    public token: string;
    public user: User;
    public loggedinUser: User;
    public userSubject: Subject<any> = new Subject<any>();

    constructor(private http: Http, private tokenService: TokenService) {
        // set token if saved in local storage
        this.loggedinUser = this.tokenService.getLoggedInUser();
        this.token = this.tokenService.getToken();
    }

    register(user): Observable<boolean> {
        return this.http.post('/user/register', user).map((response: Response) => {
            const JWT = response.json() && response.json().JWT;
            const userObj = response.json() && response.json().user;
            if (JWT) {
                // set token property
                this.token = JWT;

                // store username and jwt token in local storage to keep user logged in between page refreshes
                this.tokenService.setUserDetails(userObj);
                this.tokenService.setToken(JWT);

                // return true to indicate successful login
                return true;
            } else {
                // return false to indicate failed login
                return false;
            }
        });
    }

    login(user: User): Observable<boolean> {
        return this.http.post('/user/login', user)
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                const JWT = response.json() && response.json().JWT;
                const userObj = response.json() && response.json().user;
                if (JWT) {
                    // set token property
                    this.token = JWT;

                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    this.tokenService.setUserDetails(userObj);
                    this.tokenService.setToken(JWT);

                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            });
    }

    logout(): boolean {
        // clear token remove user from local storage to log user out
        this.token = null;
        this.tokenService.removeToken();
        this.tokenService.removeUserDetails();
        return true;
    }

    uploadProfile(image) {
        return this.http.post('/user/profileImage', image).map((response: Response) => {
            const user = response.json() && response.json().data;
            this.userSubject.next(user);
            return user;
        })
    }

    getUser(): Observable<User> {
        return this.http.get('/user/userDetails').map((response: Response) => {
            const user = response.json() && response.json().data;
            return user;
        })
    }

    updateProfile(user): Observable<User> {
        return this.http.put('/user/updateUser', user).map((response: Response) => {
            const upadtedUser = response.json() && response.json().data;
            return upadtedUser;
        })
    }

    updatePassword(password): Observable<boolean> {
        return this.http.put('/user/changePassword', password).map((res: Response) => {
            const isPasswordUpdated = res.json() && res.json().data;
            return isPasswordUpdated;
        })
    }

    forgotPassword(email): Observable<boolean> {
        return this.http.post('/forgotPassword', email).map((res: Response) => {
            const isMailSent = res.json() && res.json().data;
            return isMailSent;
        })
    }

    verifyUUID(uuid): Observable<User> {
        return this.http.get('/forgotPassword/reset/' + uuid).map((res: Response) => {
            const user = res.json() && res.json().user;
            return user;
        })
    }

    createNewPassword(user): Observable<boolean> {
        return this.http.put('/user/reset/', user).map((res: Response) => {
            const isPasswordCreated = res.json() && res.json().data;
            return isPasswordCreated;
        })
    }

    loginWithFB(fbOBj): Observable<boolean> {
        return this.http.post('/user/loginWithFB/', fbOBj).map((response: Response) => {
            const JWT = response.json() && response.json().JWT;
            const userObj = response.json() && response.json().user;
            if (JWT) {
                // set token property
                this.token = JWT;

                // store username and jwt token in local storage to keep user logged in between page refreshes
                this.tokenService.setUserDetails(userObj);
                this.tokenService.setToken(JWT);

                // return true to indicate successful login
                return true;
            } else {
                // return false to indicate failed login
                return false;
            }
        })
    }

    loginWithGoogle(user): Observable<boolean> {
        return this.http.post('/user/loginWithGoogle/', user).map((response: Response) => {
            const JWT = response.json() && response.json().JWT;
            const userObj = response.json() && response.json().user;
            if (JWT) {
                // set token property
                this.token = JWT;

                // store username and jwt token in local storage to keep user logged in between page refreshes
                this.tokenService.setUserDetails(userObj);
                this.tokenService.setToken(JWT);

                // return true to indicate successful login
                return true;
            } else {
                // return false to indicate failed login
                return false;
            }
        })
    }

    verifyEmailId(uuid): Observable<boolean> {
        return this.http.get('/user/verify/' + uuid).map((response: Response) => {
            const isEmailVerified = response.json() && response.json().data;
            return isEmailVerified;
        });
    }



}
