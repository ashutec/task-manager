import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MatDialogConfig } from '@angular/material';

import { User } from '../user';
import { AuthenticationService } from '../authentication.service';
import { ForgotPasswordComponent } from '../forgot-password/forgot-password.component';

@Component({
  selector: 'app-local-login',
  templateUrl: './local-login.component.html',
  styleUrls: ['./local-login.component.css']
})
export class LocalLoginComponent implements OnInit {
  user: User = new User();
  error;

  constructor(private router: Router,
    private _authService: AuthenticationService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
  }
  onLogin() {
    this._authService.login(this.user).subscribe(result => {
      if (result === true) {
        this.router.navigate(['/workspace']);
      } else {
        this.error = 'Username or password is incorrect';
      }
    }, err => {
      this.error = 'Username or password is incorrect';
    });
  }
  forgotPassword() {
    const config = new MatDialogConfig();
    config.width = '500px';
    this.dialog.open(ForgotPasswordComponent, config);
  }

}
