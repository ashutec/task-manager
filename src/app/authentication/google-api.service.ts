import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { GoogleAuthService } from 'ng-gapi/lib/GoogleAuthService';
import { AuthenticationService } from './authentication.service';
import GoogleUser = gapi.auth2.GoogleUser;
import GoogleAuth = gapi.auth2.GoogleAuth;

@Injectable()
export class GoogleApiService {

  constructor(private googleAuthService: GoogleAuthService,
    private authService: AuthenticationService,
    private ngZone: NgZone,
    private router: Router, ) { }

  public signIn() {
    this.googleAuthService.getAuth().subscribe((auth) => {
      auth.signIn().then(res => this.signInSuccessHandler(res), err => this.signInErrorHandler(err));
    });
  }

  private signInSuccessHandler(res: GoogleUser) {
    const token = res.getAuthResponse();
    this.ngZone.run(() => {
      this.authService.loginWithGoogle(token).subscribe(result => {
        if (result) {
          this.router.navigate(['/workspace']);
        }
      }, err => {
        console.log(err);
      })
    });
  }

  private signInErrorHandler(err) {
    console.warn(err);
  }

}
