import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalSignupComponent } from './local-signup.component';

describe('LocalSignupComponent', () => {
  let component: LocalSignupComponent;
  let fixture: ComponentFixture<LocalSignupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalSignupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalSignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
