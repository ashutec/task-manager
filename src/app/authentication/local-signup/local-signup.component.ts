import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../user';
import { AuthenticationService } from '../authentication.service';
import { ProfilePictureComponent } from '../profile-picture/profile-picture.component'

@Component({
  selector: 'app-local-signup',
  templateUrl: './local-signup.component.html',
  styleUrls: ['./local-signup.component.css']
})



export class LocalSignupComponent implements OnInit {
  user: User = new User();
  confirmPassword = '';
  image;
  error;

  constructor(private router: Router, private authService: AuthenticationService) { }

  ngOnInit() {
  }

  onRegister() {
    this.authService.register(this.user).subscribe(result => {
      if (result) {
        // this.router.navigate(['auth/profile'], );
         this.router.navigate(['auth/profile'], { queryParams: { signUp: true } });
      }
    }, err => {
      const error = JSON.parse(err._body);
      if (error && error.status === 409) {
        this.error = error.message;
      }
    });
  }

}
