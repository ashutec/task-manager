import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef } from '@angular/material';

import { Password } from '../password';
import { TokenService } from '../../http-interceptor/token.service';
import { AuthenticationService } from '../authentication.service';
import { ToastrService } from '../../task/shared/toaster.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  passwordObj: Password = new Password();
  confirmPassword: string;
  errMsg: string;
  isPasswordChanged = false;

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private _dialogRef: MatDialogRef<ChangePasswordComponent>,
    private tokenService: TokenService,
    private toaster: ToastrService,
  ) { }

  ngOnInit() {
  }

  updatePassword() {
    this.passwordObj.email = this.tokenService.getLoggedInUser().email;
    this.authService.updatePassword(this.passwordObj).subscribe((data) => {
      if (data) {
        // this._dialogRef.close();
        this.isPasswordChanged = true;
        this.tokenService.logout();
        // this.router.navigate(['auth/login']);
      }
    }, err => {
      this.errMsg = 'current password did not match';
    });
  }

  closeDialogue() {
    this._dialogRef.close();
    this.router.navigate(['auth/login']);
  }
}
