import { Injectable } from '@angular/core';
import { Http } from '@angular/http'

@Injectable()
export class LoginHistoryService {

  constructor(private _http: Http) { }

  getLoginHistory() {
    return this._http.get('/loginHistory').map(result => result.json());
  }

}
