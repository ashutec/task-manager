import { Component, OnInit } from '@angular/core';
import { LoginHistoryService } from './login-history.service'

@Component({
  selector: 'app-login-history',
  templateUrl: './login-history.component.html',
  styleUrls: ['./login-history.component.css']
})
export class LoginHistoryComponent implements OnInit {

  LoginHistories = [];

  constructor(private _loginHistoryService: LoginHistoryService) { }

  ngOnInit() {
    this._loginHistoryService.getLoginHistory().subscribe((loginHistories) => {
      this.LoginHistories = loginHistories;
    })
  }

}
