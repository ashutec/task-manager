import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig } from '@angular/material';
import { TokenService } from '../../http-interceptor/token.service'

@Component({
  selector: 'app-email-verified-dialog',
  templateUrl: './email-verified-dialog.component.html',
  styleUrls: ['./email-verified-dialog.component.css']
})
export class EmailVerifiedDialogComponent implements OnInit {

  emailDetails;
  isEmailVerified = false;
  constructor( @Inject(MAT_DIALOG_DATA) private data,
    private router: Router, private tokenService: TokenService, private _dialogRef: MatDialogRef<EmailVerifiedDialogComponent>) {
    this.emailDetails = Object.assign({}, this.data);
    this.isEmailVerified = this.emailDetails.isEmailVerified;
  }

  ngOnInit() {
  }
  goWorkspace() {
    this.router.navigate(['workspace']);
    this._dialogRef.close();
  }
  goLogin() {
    this.tokenService.logout();
    this.router.navigate(['auth/login']);
    this._dialogRef.close();
  }

}
