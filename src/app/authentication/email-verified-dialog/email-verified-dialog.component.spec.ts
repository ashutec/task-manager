import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailVerifiedDialogComponent } from './email-verified-dialog.component';

describe('EmailVerifiedDialogComponent', () => {
  let component: EmailVerifiedDialogComponent;
  let fixture: ComponentFixture<EmailVerifiedDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailVerifiedDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailVerifiedDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
