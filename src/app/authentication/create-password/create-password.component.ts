import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig } from '@angular/material';
import { Password } from '../password';
import { User } from '../user';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-create-password',
  templateUrl: './create-password.component.html',
  styleUrls: ['./create-password.component.css']
})
export class CreatePasswordComponent implements OnInit {

  passwordObj: Password = new Password();
  user: User = new User();
  isValidUrl = false;

  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private _dialogRef: MatDialogRef<CreatePasswordComponent>,
    @Inject(MAT_DIALOG_DATA) private data,
  ) {
    this.user = Object.assign({}, this.data);
    if (Object.keys(this.user).length !== 0) {
      this.isValidUrl = true;
    } else {
      this.isValidUrl = false;
    }
  }

  ngOnInit() {
  }

  createPassword() {
    this.authService.createNewPassword(this.user).subscribe(result => {
      if (result) {
        this._dialogRef.close();
        this.router.navigate((['/auth']));
      }
    }, err => {
      console.log(err);
    })
  }

  closeDialogue() {
    this.router.navigate((['/auth']));
    this._dialogRef.close();
  }

}
