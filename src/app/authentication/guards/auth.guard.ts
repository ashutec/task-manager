import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { TokenService } from '../../http-interceptor/token.service';
import { tokenNotExpired } from 'angular2-jwt';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private router: Router, private _tokenService: TokenService) { }

    canActivate() {
        if (this.loggedIn()) {
            // logged in so return true
            // console.log('loggedIn()' + this.loggedIn());
            return true;
        } else {
            // not logged in so redirect to login page
            this.router.navigate(['/auth']);
            return false;
        }
    }

    loggedIn() {
        return tokenNotExpired();
    }
}
