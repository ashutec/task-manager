import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

import { User } from '../user';
import { AuthenticationService } from '../authentication.service';
import { ToastrService } from '../../task/shared/toaster.service';


@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  user: User = new User();
  errMsg;
  isMailSent = false;

  constructor(
    private authService: AuthenticationService,
    private _dialogRef: MatDialogRef<ForgotPasswordComponent>,
    private toaster: ToastrService,
  ) { }

  ngOnInit() {
  }

  forgotPassword() {
    this.authService.forgotPassword(this.user).subscribe(result => {
      if (result === true) {
        this.isMailSent = true;
        // this._dialogRef.close();
        this.toaster.showInfo('Mail will be sent very soon, Check your inbox');
      }
    }, err => {
      const error = JSON.parse(err._body);
      this.errMsg = error.message;
    });
  }
  closeDialogue() {
    this._dialogRef.close();
  }
}
